
import Paper from '@mui/material/Paper';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getCostumerPage } from '../../../features/actions/costumer';
import { useCallback, useEffect, useState } from 'react';
import { Button, Fab, FormControl, Grid, InputLabel, MenuItem, Pagination, Select, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TablePagination, TableRow, TextField, Tooltip, Typography } from '@mui/material';
import { AddCircle, Refresh } from '@mui/icons-material';
export default function ProductTable() {

  //Initial state
  const initialState = {
    currentIndex: -1,
    searchTitle: "",
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const pageSizes = [3, 6, 9];

  const [state, setState] = useState(initialState);

  // Redirect link based on costumer id
  const navigate = useNavigate();

  const handleRowClick = useCallback(
    (row) => navigate('/costumer/' + row, { replace: true }, [navigate])
  );

  // Redux
  const dispatch = useDispatch();
  const costumerData = useSelector(state => state.costumer);

  // Handle table state
  const handleInputChange = event => {
    const { name, value } = event.target;
    setState({ ...state, [name]: value });
  };

  const getRequestParams = (searchTitle, page, pageSize, filter) => {
    let params = {};

    if (searchTitle) {
      params["name"] = searchTitle;
    }
    if (page) {
      params["page"] = page - 1;
    }
    if (pageSize) {
      params["size"] = pageSize;
    }
    if (filter) {
      params["filter"] = filter;
    }

    return params;
  }

  //get table data
  const retrieveData = () => {
    const { searchTitle, page, pageSize, filter } = state;
    const params = getRequestParams(searchTitle, page, pageSize, filter);

    dispatch(getCostumerPage(params));
  }

  useEffect(() => {
    retrieveData();
  }, [state, dispatch]);

  return (
    <>
      {/* Tombol refresh */}
      <Fab color="primary"
        aria-label="add"
        sx={{
          position: "fixed",
          top: (theme) => theme.spacing(10),
          right: (theme) => theme.spacing(5)
        }}
        onClick={retrieveData}>
        <Refresh />
      </Fab>

      <Paper sx={{ boxShadow: 5, p: 1 }}>

        {/* Tombol tambahkan pelanggan */}
        <Button sx={{ width: 1, mb: 1 }}
          variant="contained"
          endIcon={<AddCircle />}
          component={Link}
          to="/costumer">
          Pelanggan Baru
        </Button>

        {costumerData.items &&
          (<>

            {/* Filtering */}
            <Grid container
              spacing={1}
              sx={{ padding: 1 }}>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextField id="outlined-search"
                  label="Kata Kunci"
                  type="search"
                  fullWidth
                  onChange={handleInputChange}
                  value={state.searchTitle != null ? state.searchTitle : ""}
                  name="searchTitle" />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <FormControl fullWidth>
                  <InputLabel id="filter-label">Filter</InputLabel>
                  <Select
                    labelId="filter-label"
                    value={state.filter != null ? state.filter : ""}
                    onChange={handleInputChange}
                    name="filter"
                    id="filter"
                    label="Filter"
                  >
                    <MenuItem value={"name"}>Nama</MenuItem>
                    <MenuItem value={"address"}>Alamat</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>

            {/* Tabel Costumer */}
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow >
                    <StyledTableCell align="left">Nama</StyledTableCell>
                    <StyledTableCell align="left">Alamat</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {costumerData.items.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row.id) }}
                    >
                      <StyledTableCell align="left">{row.name}</StyledTableCell>
                      <StyledTableCell align="left"
                        title={row.address}>
                        {row.address.substring(0, 40) + "..."}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            {/* Pagination */}
            <TablePagination
              component="div"
              count={costumerData.totalItems && costumerData.totalItems}
              page={costumerData.currentPage && costumerData.currentPage}
              onPageChange={(event, page) => setState({ ...state, page: page + 1 })}
              rowsPerPage={state.pageSize}
              rowsPerPageOptions={pageSizes}
              onRowsPerPageChange={(event) => setState({ ...state, page: 1, pageSize: event.target.value })}
            />
            
          </>
          )}
      </Paper>
    </>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));