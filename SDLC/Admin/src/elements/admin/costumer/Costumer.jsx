import Box from '@mui/material/Box';
import { Avatar, Button, Card, CardActions, CardContent, FormGroup, Stack, styled, TextField, Typography, Grid } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { useDispatch, useSelector } from 'react-redux';
import { postImage } from '../../../features/actions/file';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { createCostumer, deleteCostumer, updateCostumer, getCostumer, getCostumerPage } from '../../../features/actions/costumer';
import { useConfirm } from 'material-ui-confirm';
import { Cancel, Edit, Save } from '@mui/icons-material';
import uploadFileService from '../../../features/services/uploadFileService';
import CostumerTable from './CostumerTable';

function Costumer() {
    //Get id from params
    const { costumerId } = useParams();

    //Initial state
    const initialCostumerState = {
        id: null,
        name: null,
        address: null,
        image: null,
    };

    const initialImageState = {
        currentFile: undefined,
        previewImage: undefined,
        message: "",

        imageInfos: [],
    };

    // use State
    const [costumer, setCostumer] = useState(initialCostumerState);
    const [image, setImage] = useState(initialImageState);
    const [button, setButton] = useState(null);
    const [editable, setEditable] = useState(false);

    // redux
    const dispatch = useDispatch();
    const costumerData = useSelector(state => state.costumer);

    // handle object costumer
    const handleInputChange = event => {
        const { name, value } = event.target;
        setCostumer({ ...costumer, [name]: value });
    };

    //handle submit event
    const handleSubmit = (e) => {
        e.preventDefault();

        if (button == "update") {
            confirm({ title: 'Apakah anda yakin ingin mengubah pelanggan id: ' + costumer.id + '?', description: 'Tindakan ini bersifat permanen.' })
                .then(() => {
                    dispatch(updateCostumer(costumer))
                        .then(data => {
                        })
                        .catch(e => {
                            alert("Maaf, terjadi kesalahan pada server!");
                        });
                })
                .catch(() => { /* ... */ });
        } else {
            dispatch(createCostumer(costumer))
                .then(data => {
                    dispatch(getCostumerPage({ page: 0 }));
                    refresh();
                })
                .catch(e => {
                    alert("Maaf, terjadi kesalahan pada server!");
                });
        }
    };

    // re initiate value
    const refresh = () => {
        setCostumer(initialCostumerState);
        setImage(initialImageState);
        setEditable(true);
    };

    const confirm = useConfirm();

    const Input = styled('input')({
        display: 'none',
    });

    // Select file
    const selectFile = (event) => {
        if (event.target.files[0].size >= 500e3) {
            window.alert("Please upload a file smaller than 500 KB");
        } else {
            setImage({
                ...image,
                currentFile: event.target.files[0],
                previewImage: URL.createObjectURL(event.target.files[0]),
                message: ""
            });
        }
    };

    //Upload
    const upload = () => {
        const imageId = Date.now();
        dispatch(postImage(image.currentFile, ("costumer-" + imageId)))
            .then((response) => {
                setCostumer({
                    ...costumer,
                    image: "http://localhost:8080/api/files/get/" + "costumer-" + imageId + "/" + image.currentFile.name,
                });
                return uploadFileService.getFiles(image.currentFile.name, ("costumer-" + imageId));
            })
            .catch((err) => {
                // console.log(err);
                setImage({
                    ...image,
                    message: "Could not upload the image!",
                    currentFile: undefined,
                });
            });
    }

    // Upload file
    useEffect(() => {
        if (image.currentFile != undefined) {
            upload();
        };
    }, [image.currentFile])

    //Update state
    useEffect(() => {
        if (costumerId > 0 && costumerData.length != 0) {
            const costumerFromDatabase = costumerData.items.find(costumer => costumer.id == costumerId);
            setCostumer(costumerFromDatabase);
            setImage({
                ...image,
                previewImage: costumerFromDatabase.image
            })
            setEditable(false);
        } else {
            refresh();
        }
    }, [costumerId, dispatch]);

    return (
        <>
            <Box>
                <Typography variant="h4" sx={{ mx: "auto" }}>
                    Pelanggan
                </Typography>
                <Stack sx={{ mx: "auto" }} >
                    <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                        <Grid container>

                            {/* Element Tabel Costumer */}
                            <Grid item xs={12} sm={12} md={6} lg={5} xl={4} sx={{ mb: 2 }}>
                                <CostumerTable />
                            </Grid>

                            {/* Element For Costumer */}
                            <Grid item xs={12} sm={12} md={6} lg={7} xl={8} component="form" onSubmit={handleSubmit}>
                                <FormGroup>

                                    {/* Bagian Foto */}
                                    <CardContent sx={{ py: 0, display: 'flex' }}>
                                        <Stack alignItems="center"
                                            direction="column"
                                            spacing={2}>
                                            {image.previewImage ? (
                                                <Avatar alt="Costumer Preview"
                                                    src={image.previewImage}
                                                    variant="square"
                                                    sx={{ width: 200, height: 200, border: 2 }} />
                                            ) : (<Avatar variant="square"
                                                sx={{ width: 200, height: 200, border: 2 }}>
                                                No Image
                                            </Avatar>)}
                                            <label htmlFor="contained-button-file">
                                                <Input accept="image/*"
                                                    id="contained-button-file"
                                                    type="file"
                                                    onChange={selectFile}
                                                    disabled={!editable} />
                                                <Button variant="contained"
                                                    component="span"
                                                    disabled={!editable} >
                                                    Unggah Gambar
                                                </Button>
                                            </label>
                                            <label htmlFor="icon-button-file">
                                                <Input accept="image/*"
                                                    id="icon-button-file"
                                                    type="file" />
                                            </label>
                                        </Stack>
                                    </CardContent>

                                    {/* Bagian Form Data */}
                                    <CardContent>
                                        <TextField required
                                            sx={{ my: 1 }}
                                            inputProps={{ pattern: "^[a-zA-Z ]+$", minLength: 3, maxLength: 50, title: "Nama hanya berupa huruf!" }}
                                            fullWidth
                                            id="outlined-basic"
                                            label="Nama"
                                            variant="outlined"
                                            value={costumer.name != null ? costumer.name : ""}
                                            onChange={handleInputChange}
                                            name="name"
                                            disabled={!editable} />
                                        <TextField required
                                            inputProps={{ maxLength: 255 }}
                                            fullWidth
                                            multiline
                                            minRows="5"
                                            id="outlined-basic"
                                            label="Alamat"
                                            variant="outlined"
                                            value={costumer.address != null ? costumer.address : ""}
                                            onChange={handleInputChange}
                                            name="address"
                                            disabled={!editable} />
                                    </CardContent>

                                    {/* Tombol Aksi */}
                                    <CardActions>
                                        <Stack
                                            width={1}
                                            direction="row"
                                            alignItems="center"
                                            spacing={2}
                                            justifyContent="left"
                                        >
                                            {costumer.id > 0 ? (
                                                <>
                                                    <Button variant="outlined"
                                                        startIcon={<Cancel />}
                                                        onClick={refresh}>
                                                        Batal
                                                    </Button>
                                                    {editable && (
                                                        <Button id="save"
                                                            name="action"
                                                            value="update"
                                                            variant="contained"
                                                            endIcon={<Save />}
                                                            type="submit"
                                                            onClick={() => (setButton("update"))}>
                                                            Simpan
                                                        </Button>
                                                    )}
                                                    {!editable && (
                                                        <Button id="edit"
                                                            color='success'
                                                            variant="contained"
                                                            endIcon={<Edit />}
                                                            onClick={() => (setEditable(true))}>
                                                            Edit
                                                        </Button>
                                                    )}
                                                </>
                                            ) : (
                                                <Button
                                                    name="action"
                                                    value="new"
                                                    variant="contained"
                                                    endIcon={<SendIcon />}
                                                    type="submit"
                                                    onClick={() => (setButton("new"))}>
                                                    Simpan Data Baru
                                                </Button>
                                            )
                                            }
                                        </Stack>
                                    </CardActions>
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </Card>
                </Stack>
            </Box>
        </>
    );
}

export default Costumer;

