import Box from '@mui/material/Box';
import { Button, Card, CardActions, CardContent, FormGroup, Stack, styled, TextField, Typography, Grid, Autocomplete } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { createOptions, deleteOptions, getOptionsPage, updateOptions } from '../../../features/actions/options';
import { getAllQuestion } from '../../../features/actions/question';
import { useConfirm } from 'material-ui-confirm';
import { Cancel, Edit, Save } from '@mui/icons-material';
import OptionsTable from './OptionsTable';

function Options() {

    // Get id from params
    const { optionsId } = useParams();

    // Initial state

    const initialOptionsState = {
        id: null,
        questionOption: null,
        questionId: null,
    };

    const initialQuestionState = null;

    const [options, setOptions] = useState(initialOptionsState);
    const [question, setQuestion] = useState(initialQuestionState);
    const [button, setButton] = useState(null);
    const [editable, setEditable] = useState(false);

    // Redux
    const dispatch = useDispatch();
    const optionsData = useSelector(state => state.options);
    const questionData = useSelector(state => state.question);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setOptions({ ...options, [name]: value });
    };

    // Redirect using navigate
    const navigate = useNavigate();

    const refresh = () => {
        setOptions(initialOptionsState);
        setQuestion(initialQuestionState);
        setEditable(true)
        navigate('/options/');
    };

    const confirm = useConfirm();

    const deleteConfirm = () => {
        confirm({ title: 'Apakah anda yakin ingin menghapus pilihan id: ' + options.id + '?' })
            .then(() => {
                dispatch(deleteOptions(options.id))
                    .then(data => {
                        dispatch(getOptionsPage({ page: 0 }));
                    })
                    .catch(e => {
                        alert("Maaf, terjadi kesalahan pada server!");
                    });
                refresh();
            })
            .catch(() => { /* ... */ });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (button == "update") {
            confirm({ title: 'Apakah anda yakin ingin mengubah pilihan id: ' + options.id + '?', description: 'Tindakan ini bersifat permanen.' })
                .then(() => {
                    dispatch(updateOptions({
                        id: options.id,
                        questionOption: options.questionOption,
                        questionId: { id: options.questionId },
                    }))
                        .then(data => {
                            dispatch(getOptionsPage({ page: 0 }));
                        })
                        .catch(e => {
                            alert("Maaf, terjadi kesalahan pada server!");
                        });
                })
                .catch(() => {
                    alert("Maaf, terjadi kesalahan pada server!");
                });
        } else {
            dispatch(createOptions({
                questionOption: options.questionOption,
                questionId: { id: options.questionId },
            }))
                .then(data => {
                    dispatch(getOptionsPage({ page: 0 }));
                    refresh();
                })
                .catch(e => {
                    alert("Maaf, terjadi kesalahan pada server!");
                });
        }
    };

    useEffect(() => {
        dispatch(getAllQuestion());
    }, []);

    // Update options data once the question change
    useEffect(() => {
        if (question) {
            setOptions({
                ...options,
                questionId: question.id,
            });
        }
    }, [question]);

    // Update question
    useEffect(() => {
        if (optionsId > 0 && optionsData.items) {
            const optionsFromDatabase = optionsData.items.find(options => options.id == optionsId);
            setOptions(optionsFromDatabase);
            setQuestion(questionData && questionData.find(question => question.id == optionsFromDatabase.questionId));
            setEditable(false);
        } else {
            refresh();
        }
    }, [optionsId, dispatch]);

    return (
        <>
            <Box>
                <Typography variant="h4" sx={{ mx: "auto" }}>
                    Pilihan
                </Typography>

                <Stack sx={{ mx: "auto" }} >
                    <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                        <Grid container>

                            {/* Elemen Tabel Pilihan */}
                            <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
                                <OptionsTable />
                            </Grid>

                            {/* Element Form */}
                            <Grid sx={{ p: 1 }} item xs={12} sm={12} md={6} lg={7} xl={8}
                                component="form"
                                onSubmit={handleSubmit}>
                                <FormGroup>

                                    {/* Form */}
                                    <CardContent>
                                        {questionData.length &&
                                            <Autocomplete
                                                fullWidth
                                                required
                                                value={question}
                                                onChange={(event, newQuestion) => {
                                                    setQuestion(newQuestion);
                                                }}
                                                id="user"
                                                options={questionData}
                                                getOptionLabel={(option) => option.question}
                                                sx={{ my: 1 }}
                                                renderInput={(params) => <TextField required {...params} label="Pertanyaan" />}
                                                disabled={!editable}
                                            />
                                        }

                                        <TextField required
                                            sx={{ my: 1 }}
                                            inputProps={{ minLength: 2, maxLength: 20 }}
                                            fullWidth
                                            id="outlined-basic"
                                            label="Pilihan Jawaban"
                                            variant="outlined"
                                            value={options.questionOption != null ? options.questionOption : ""}
                                            onChange={handleInputChange}
                                            name="questionOption"
                                            disabled={!editable} />
                                    </CardContent>

                                    {/* Tombol aksi form */}
                                    <CardActions >
                                        <Stack
                                            width={1}
                                            direction="row"
                                            alignItems="center"
                                            spacing={2}
                                            justifyContent="left"
                                        >
                                            {options.id > 0 ? (
                                                <>
                                                    {editable ? (<>
                                                        <Button variant="outlined"
                                                            startIcon={<Cancel />}
                                                            onClick={refresh}>
                                                            Batal
                                                        </Button>
                                                        <Button variant="outlined"
                                                            startIcon={<DeleteIcon />}
                                                            color="error"
                                                            onClick={deleteConfirm}>
                                                            Hapus
                                                        </Button>
                                                        <Button name="action"
                                                            value="update"
                                                            variant="contained"
                                                            endIcon={<Save />}
                                                            type="submit"
                                                            onClick={() => (setButton("update"))}>
                                                            Simpan
                                                        </Button>
                                                    </>) : <>
                                                        <Button variant="outlined"
                                                            startIcon={<Cancel />}
                                                            onClick={refresh}>
                                                            Batal
                                                        </Button>
                                                        <Button variant="contained"
                                                            color='success'
                                                            endIcon={<Edit />}
                                                            onClick={() => (setEditable(true))}>
                                                            Edit
                                                        </Button>
                                                    </>
                                                    }
                                                </>
                                            ) : (
                                                <Button name="action"
                                                    value="new"
                                                    variant="contained"
                                                    endIcon={<SendIcon />}
                                                    type="submit"
                                                    onClick={() => (setButton("new"))}>
                                                    Simpan Data Baru
                                                </Button>
                                            )
                                            }
                                        </Stack>
                                    </CardActions>
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </Card>
                </Stack>
            </Box>
        </>
    );
}

export default Options;

