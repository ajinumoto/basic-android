import Box from '@mui/material/Box';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';
import { useState } from 'react';


function AlertBar(props) {

    const [open, setOpen] = useState(props.open);
    const [content, setContent] = useState(props.content);

    return (
        <Box sx={{ width: '100%', zIndex: 'modal', position: 'absolute' }}>
            <Collapse in={open}>
                <Alert severity="error"
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                                setOpen(false);
                            }}
                        >
                            <CloseIcon fontSize="inherit" />
                        </IconButton>
                    }
                    sx={{ mb: 2 }}
                >
                    {content}
                </Alert>
            </Collapse>
        </Box>
    );
}

export default AlertBar;

