import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Avatar, ListItem, ListItemButton } from '@mui/material';
import { Badge, Dashboard, Flaky, HelpOutline, Logout, Map, People, Task } from '@mui/icons-material';
import { forwardRef, useState, useMemo } from 'react';
import eventBus from '../../common/EventBus';
import nexmileLogo from '../../asset/images/nexmile_logo.png';
import { createTheme, ThemeProvider } from '@mui/material/styles';

// Thenming

const drawerWidth = 240;

const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create(),
    overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create(),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(),
    }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

// List item for link
function ListItemLink(props) {
    const { icon, primary, to } = props;

    const renderLink = useMemo(
        () =>
            forwardRef(function Link(itemProps, ref) {
                return <RouterLink to={to} ref={ref} {...itemProps} role={undefined} />;
            }),
        [to],
    );

    return (
        <li>
            <ListItem button component={renderLink}>
                {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
                <ListItemText primary={primary} />
            </ListItem>
        </li>
    );
}

ListItemLink.propTypes = {
    icon: PropTypes.element,
    primary: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
};

export default function MiniDrawer() {
    const theme = createTheme({
        palette: {
            primary: {
                main: '#ffb300',
            },
            secondary: {
                main: '#e65100',
            },
        },
    });

    const [open, setOpen] = useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const logoutHendler = () => {
        eventBus.dispatch("logout");
        window.location.reload();
    }

    return (
        <ThemeProvider theme={theme}>
            <Box>
                <CssBaseline />
                <AppBar position="fixed" open={open}>
                    <Toolbar sx={{ height: 64 }}>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            sx={{
                                flexDirection: 'row-reverse',
                                // marginRight: 5,
                                ...(open && { display: 'none' }),
                            }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Avatar alt="Logo"
                            src={nexmileLogo}
                            sx={{ width: 40, height: 40, mx: 2 }} />
                        <Typography variant="h6"
                            component="div">
                            NEXMILE
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer variant="permanent"
                    open={open}>
                    <DrawerHeader>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                        </IconButton>
                    </DrawerHeader>
                    <Divider />
                    <List>
                        <ListItemLink to="/dashboard" primary="Halaman Depan" icon={<Dashboard />} />
                        <ListItemLink to="/user" primary="Seles" icon={<Badge />} />
                        <ListItemLink to="/costumer" primary="Pelanggan" icon={<People />} />
                        <ListItemLink to="/question" primary="Pertanyaan" icon={<HelpOutline />} />
                        <ListItemLink to="/options" primary="Pilihan Jawaban" icon={<Flaky />} />
                        <ListItemLink to="/route" primary="Rute Perjanan" icon={<Map />} />
                        <ListItemLink to="/report" primary="Laporan" icon={<Task />} />
                    </List>
                    <Divider />
                    <List>
                        <ListItem disablePadding>
                            <ListItemButton onClick={logoutHendler}>
                                <ListItemIcon>
                                    <Logout />
                                </ListItemIcon>
                                <ListItemText primary="Keluar" />
                            </ListItemButton>
                        </ListItem>
                    </List>
                </Drawer>
            </Box>
        </ThemeProvider>
    );
}
