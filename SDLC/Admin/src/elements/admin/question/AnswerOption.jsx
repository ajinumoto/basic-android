import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';
import { useDispatch, useSelector } from 'react-redux';
import { getOptionByQuestion, newOptions } from '../../../features/actions/options';
import { Fragment, useEffect, useState } from 'react';

const filter = createFilterOptions();

export default function FreeSoloCreateOptionDialog(props) {

    //Initial State

    const questionId = props.id;

    const [value, setValue] = useState("");
    const [status, setStatus] = useState(false);
    const [open, toggleOpen] = useState(false);

    const [dialogValue, setDialogValue] = useState("");

    // redux
    const dispatch = useDispatch();
    const optionsData = useSelector(state => state.options);

    // Handle Action
    const handleClose = () => {
        toggleOpen(false);
    };

    const handleCancel = () => {
        setValue(dialogValue);
        toggleOpen(false);
    };

    const handleSubmit = () => {
        dispatch(newOptions({
            questionOption: dialogValue,
            questionId: { id: questionId },
        }))
            .then(data => {
                setValue("");
                setDialogValue("");
                setStatus(!status);
            })
            .catch(e => {
                // console.log(e);
            });
        handleClose();
    };

    // update option data based on question id
    useEffect(() => {
        if (questionId > 0) {
            dispatch(getOptionByQuestion(questionId));
        }
    }, [questionId, status]);

    return (
        <Fragment>

            {/* Tampilkan Pilihan pada Autocomplete */}
            <Autocomplete
                disabled={!props.editable}
                value={value}
                onChange={(event, newValue) => {
                    if (typeof newValue === 'string') {
                        setTimeout(() => {
                            toggleOpen(true);
                            setDialogValue(newValue);
                        });
                    } else if (newValue && newValue.inputValue) {
                        toggleOpen(true);
                        setDialogValue(newValue.inputValue);
                    } else {
                        setValue(newValue);
                    }
                }}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);

                    if (params.inputValue !== '') {
                        filtered.push({
                            inputValue: params.inputValue,
                            questionOption: `Tambah "${params.inputValue}"`,
                        });
                    }

                    return filtered;
                }}
                id="free-solo-dialog-demo"
                options={optionsData.items ? [] : optionsData}
                getOptionLabel={(option) => {
                    if (typeof option === 'string') {
                        return option;
                    }
                    if (option.inputValue) {
                        return option.inputValue;
                    }
                    return option.questionOption;
                }}
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                renderOption={(props, option) => <li {...props}>{option.questionOption}</li>}
                sx={{ width: 300 }}
                freeSolo
                renderInput={(params) => <TextField {...params} label="Pilihan Jawaban" />}
            />

            {/* Konfirmasi Ketika data ditambahkan */}
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Konfirmasi</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Mohon periksa kembali jawaban Anda!
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        value={dialogValue}
                        onChange={(event) =>
                            setDialogValue(event.target.value)
                        }
                        label="Jawaban"
                        type="text"
                        variant="standard"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCancel}>Batalkan</Button>
                    <Button onClick={handleSubmit}>Tambahkan</Button>
                </DialogActions>
            </Dialog>
        </Fragment>
    );
}
