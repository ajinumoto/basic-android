import Box from '@mui/material/Box';
import { Button, Card, CardActions, CardContent, FormGroup, Stack, TextField, Typography, Grid, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { createQuestion, getQuestionPage, updateQuestion } from '../../../features/actions/question';
import { useConfirm } from 'material-ui-confirm';
import { Cancel, Edit, Save } from '@mui/icons-material';
import QuestionTable from './QuestionTable';
import AnswerOption from './AnswerOption';

function Question() {

    // Initial State

    const { questionId } = useParams();

    const initialQuestionState = {
        id: null,
        question: null,
        typeId: null,
    };

    const [question, setQuestion] = useState(initialQuestionState);
    const [button, setButton] = useState(null);
    const [editable, setEditable] = useState(false);

    // Redux
    const dispatch = useDispatch();
    const questionData = useSelector(state => state.question);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setQuestion({ ...question, [name]: value });
    };

    // Change page url using navigate
    const navigate = useNavigate();

    // get data from server
    const refresh = () => {
        setQuestion(initialQuestionState);
        setEditable(true);
        navigate('/question/');
    };

    const confirm = useConfirm();

    const handleSubmit = (e) => {
        e.preventDefault();

        if (button == "update") {
            confirm({ title: 'Apakah anda yakin ingin mengubah rute id: ' + question.id + '?', description: 'Tindakan ini bersifat permanen.' })
                .then(() => {
                    dispatch(updateQuestion(
                        {
                            id: question.id,
                            question: question.question,
                            typeId: { id: question.typeId },
                        }
                    ))
                        .then(data => {
                        })
                        .catch(e => {
                            alert("Maaf, terjadi kesalahan pada server!");
                        });
                })
                .catch(() => { /* ... */ });
        } else {
            dispatch(createQuestion({
                question: question.question,
                typeId: { id: question.typeId },
            }))
                .then(data => {
                    dispatch(getQuestionPage({ page: 0 }));
                    refresh();
                })
                .catch(e => {
                    alert("Maaf, terjadi kesalahan pada server!");
                });
        }
    };

    // update data once question ID change
    useEffect(() => {
        if (questionId > 0 && questionData.items) {
            const questionFromDatabase = questionData.items.find(question => question.id == questionId);
            setQuestion(questionFromDatabase);
            setEditable(false);
        } else {
            refresh();
        }
    }, [questionId, dispatch]);

    return (
        <>
            <Box>
                <Typography variant="h4" sx={{ mx: "auto" }}>
                    Pertanyaan
                </Typography>

                <Stack sx={{ mx: "auto" }} >
                    <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                        <Grid container>

                            {/* Element Tabel */}
                            <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
                                <QuestionTable />
                            </Grid>

                            {/* Element Form */}
                            <Grid sx={{ p: 1 }} item xs={12} sm={12} md={6} lg={7} xl={8}
                                component="form"
                                onSubmit={handleSubmit}>
                                <FormGroup>

                                    {/* Konten Form */}
                                    <CardContent>
                                        <FormControl required
                                            fullWidth
                                            sx={{ my: 1 }}>
                                            <InputLabel id="type">Jenis</InputLabel>
                                            <Select
                                                labelId="type"
                                                id="type"
                                                value={question.typeId != null ? question.typeId : ""}
                                                label="Jenis"
                                                onChange={handleInputChange}
                                                name="typeId"
                                                disabled={!editable}
                                            >
                                                <MenuItem value={1}>Text</MenuItem>
                                                <MenuItem value={2}>Radio</MenuItem>
                                                <MenuItem value={3}>Checkbox</MenuItem>
                                            </Select>
                                        </FormControl>

                                        <TextField required
                                            sx={{ my: 1 }}
                                            inputProps={{ maxLength: 1024 }}
                                            fullWidth
                                            id="outlined-basic"
                                            label="Pertanyaan"
                                            variant="outlined"
                                            value={question.question != null ? question.question : ""}
                                            onChange={handleInputChange}
                                            name="question"
                                            disabled={!editable} />
                                        {questionId && question.typeId == 2 &&
                                            <AnswerOption id={questionId} editable={editable} />}
                                        {questionId && question.typeId == 3 &&
                                            <AnswerOption id={questionId} editable={editable} />}
                                    </CardContent>

                                    {/* Tombol aksi pada form */}
                                    <CardActions>
                                        <Stack
                                            width={1}
                                            direction="row"
                                            alignItems="center"
                                            spacing={2}
                                            justifyContent="left"
                                        >
                                            {question.id > 0 ? (
                                                <>
                                                    <Button variant="outlined"
                                                        startIcon={<Cancel />}
                                                        onClick={refresh}>
                                                        Batal
                                                    </Button>
                                                    {editable && (
                                                        <Button id="save"
                                                            name="action"
                                                            value="update"
                                                            variant="contained"
                                                            endIcon={<Save />}
                                                            type="submit"
                                                            onClick={() => (setButton("update"))}>
                                                            Simpan
                                                        </Button>
                                                    )}
                                                    {!editable && (<Button color='success'
                                                        id="edit"
                                                        variant="contained"
                                                        endIcon={<Edit />}
                                                        onClick={() => (setEditable(true))}>
                                                        Edit
                                                    </Button>)}
                                                </>
                                            ) : (
                                                <Button name="action"
                                                    value="new"
                                                    variant="contained"
                                                    endIcon={<SendIcon />}
                                                    type="submit"
                                                    onClick={() => (setButton("new"))}>
                                                    Simpan Data Baru
                                                </Button>
                                            )
                                            }
                                        </Stack>
                                    </CardActions>
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </Card>
                </Stack>
            </Box>
        </>
    );
}

export default Question;

