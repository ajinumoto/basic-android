import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useNavigate } from 'react-router-dom';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getReportPage } from '../../../features/actions/report';
import Divider from '@mui/material/Divider';

export default function RecentReport() {

  const dispatch = useDispatch();
  const routeData = useSelector(state => state.report);

  const retrieveData = () => {
    dispatch(getReportPage({ name: 1 }));
  }

  useEffect(() => {
    retrieveData();
  }, [dispatch]);

  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>

        <Typography
          variant="h6"
          sx={{ mb: 1 }}
          color="text.secondary"
          gutterBottom>
          Laporan Terselesaikan Terakhir:
        </Typography>

        <Divider sx={{ my: 1 }} />

        {routeData && routeData.items && routeData.items.map((row, index) => (
          index == 0 ? <Box key={row.id}>
            <Typography variant="h6" component="div">
              {row.completedDate}
            </Typography>

            <Typography variant="h5" component="div">
              {row.costumerId.name}
            </Typography>
            <Typography variant="h6" color="text.secondary">
              {row.userId.roles[0].name == "ROLE_TAGGING" ? "Tagging" : "Survey"}
            </Typography>

          </Box> : <Box key={row.id}>

            <Divider light sx={{ my: 1 }} />

            <Typography variant="h5" component="div">
              {row.costumerId.name}
            </Typography>

            <Typography variant="h6" color="text.secondary">
              {row.userId.roles[0].name == "ROLE_TAGGING" ? ("Tagging (" + row.completedDate + ")") : ("Survey (" + row.completedDate + ")")}
            </Typography>
          </Box>
        ))
        }
      </CardContent>
    </Card>
  );
}
