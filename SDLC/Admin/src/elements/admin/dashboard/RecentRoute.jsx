import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useNavigate } from 'react-router-dom';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutePage } from '../../../features/actions/route';
import Divider from '@mui/material/Divider';
import { Avatar } from '@mui/material';

export default function BasicCard() {
  const initialState = {
    currentIndex: -1,
    searchTitle: "",
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const dispatch = useDispatch();
  const routeData = useSelector(state => state.route);

  const retrieveData = () => {
    dispatch(getRoutePage());
  }

  useEffect(() => {
    retrieveData();
  }, [dispatch]);
  return (
    <Card sx={{ minWidth: 275 }}>
      <CardContent>

        <Typography variant="h6" sx={{ mb: 1 }} color="text.secondary" gutterBottom>
          Rute Perjalanan Terakhir:
        </Typography>

        <Divider sx={{ my: 1 }} />

        {routeData && routeData.items && routeData.items.map((row, index) => (
          index == 0 ? <Box key={row.id}>
            <Avatar sx={{ width: 100, height: 100 }}
              src={row.costumerId.image} />
            <Typography variant="h5" component="div">
              {row.costumerId.name}
            </Typography>

            <Typography variant="h6" color="text.secondary">
              {row.userId.name} {row.userId.roles[0].name == "ROLE_TAGGING" ? (" (Tagging)") : (" (Survey)")}
            </Typography>
            <Typography sx={{ mb: 1 }} variant="h8" color="text.secondary">
              {row.costumerId.address}
            </Typography>
          </Box>
            :
            <Box key={row.id}>
              <Divider light sx={{ my: 1 }} />
              <Avatar sx={{ width: 100, height: 100 }}
                src={row.costumerId.image} />
                
              <Typography variant="h5" component="div">
                {row.costumerId.name}
              </Typography>
              <Typography variant="h6" color="text.secondary">
                {row.userId.name} {row.userId.roles[0].name == "ROLE_TAGGING" ? (" (Tagging)") : (" (Survey)")}
              </Typography>
              <Typography variant="h8" color="text.secondary">
                {row.costumerId.address}
              </Typography>
            </Box>
        ))
        }
      </CardContent>
    </Card>
  );
}
