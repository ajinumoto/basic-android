
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useEffect } from 'react';
import { Button, Chip, Fab, Grid, TextField, Typography } from '@mui/material';
import { AddCircle, Refresh } from '@mui/icons-material';
import RecentRoute from './RecentRoute';
import RecentReport from './RecentReport';
import { getReportPage } from '../../../features/actions/report';
import { getRoutePage } from '../../../features/actions/route';
export default function Dashboard() {
  const dispatch = useDispatch();

  const refresh = () => {
    dispatch(getReportPage({ name: 1 }));
    dispatch(getRoutePage());
  };

  // call refresh once page on loaded
  useEffect(() => {
    refresh();
  }, []);

  return (
    <Box>

      {/* Tombol refresh */}
      <Fab color="primary"
        aria-label="add"
        sx={{
          position: "fixed",
          top: (theme) => theme.spacing(10),
          right: (theme) => theme.spacing(5)
        }} onClick={refresh}>
        <Refresh />
      </Fab>

      <Typography variant="h4" sx={{ mx: "auto" }}>
        Dashboard
      </Typography>

      <Paper sx={{ width: '100%', my: 2, boxShadow: 5, p: 1 }}>
        <Grid container spacing={2}>
          {/* Card Route */}
          <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
            <RecentRoute />
          </Grid>

          {/* Card Report */}
          <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
            <RecentReport />
          </Grid>
        </Grid>
      </Paper>

    </Box>
  );
}
