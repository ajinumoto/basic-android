
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutePage } from '../../../features/actions/route';
import { useCallback, useEffect, useState } from 'react';
import { Fab, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TablePagination, TableRow, Typography } from '@mui/material';
import { CameraAlt, Quiz, Refresh } from '@mui/icons-material';
export default function RouteTable() {

  // Initial state

  const initialState = {
    currentIndex: -1,
    searchTitle: "",
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const pageSizes = [3, 6, 9];

  const [state, setState] = useState(initialState);

  const navigate = useNavigate();

  const handleRowClick = useCallback(
    (row) => navigate('/route/' + row, { replace: true }, [navigate])
  );

  // redux
  const dispatch = useDispatch();
  const routeData = useSelector(state => state.route);

  // Paginantion parameters
  const getRequestParams = (searchTitle, page, pageSize, filter) => {
    let params = {};

    if (searchTitle) {
      params["name"] = searchTitle;
    }
    if (page) {
      params["page"] = page - 1;
    }
    if (pageSize) {
      params["size"] = pageSize;
    }
    if (filter) {
      params["filter"] = filter;
    }

    return params;
  }

  // Update redux data using API
  const retrieveData = () => {
    const { searchTitle, page, pageSize, filter } = state;
    const params = getRequestParams(searchTitle, page, pageSize, filter);

    dispatch(getRoutePage(params));
  }

  // update data once state change
  useEffect(() => {
    retrieveData();
  }, [state, dispatch]);

  return (
    <Box>
      <Fab color="primary" aria-label="add" sx={{
        position: "fixed",
        top: (theme) => theme.spacing(10),
        right: (theme) => theme.spacing(5)
      }} onClick={retrieveData}>
        <Refresh />
      </Fab>

      <Paper sx={{ width: '100%', boxShadow: 5, p: 1 }}>

        {routeData.items &&
          (<>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow >
                    <StyledTableCell align="center">Jenis</StyledTableCell>
                    <StyledTableCell align="center">Tanggal</StyledTableCell>
                    <StyledTableCell>Seles</StyledTableCell>
                    <StyledTableCell align="left">Pelanggan</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {routeData.items.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row.id) }}
                    >
                      <StyledTableCell component="th" scope="row" align="center">
                        {row.userId.roles[0].name == "ROLE_TAGGING" ?
                          <CameraAlt sx={{ color: "orange" }} /> : <Quiz sx={{ color: "blue" }} />}
                      </StyledTableCell>
                      <StyledTableCell align="center">{row.date}</StyledTableCell>
                      <StyledTableCell align="left">{row.userId.name}</StyledTableCell>
                      <StyledTableCell align="left">{row.costumerId.name}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              component="div"
              count={routeData.totalItems && routeData.totalItems}
              page={routeData.currentPage && routeData.currentPage}
              onPageChange={(event, page) => setState({ ...state, page: page + 1 })}
              rowsPerPage={state.pageSize}
              rowsPerPageOptions={pageSizes}
              onRowsPerPageChange={(event) => setState({ ...state, page: 1, pageSize: event.target.value })}
            />
          </>
          )}
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));