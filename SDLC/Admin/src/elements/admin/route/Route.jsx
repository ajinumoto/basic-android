import Box from '@mui/material/Box';
import { Button, Card, CardActions, CardContent, FormGroup, Stack, TextField, Typography, Grid, Autocomplete, FormControlLabel, Checkbox, Divider } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { createRoute, deleteRoute, getRoutePage, updateRoute } from '../../../features/actions/route';
import { getAllCostumer } from '../../../features/actions/costumer';
import { getAllUser } from '../../../features/actions/user';
import { useConfirm } from 'material-ui-confirm';
import { Cancel, Edit, Save } from '@mui/icons-material';
import RouteTable from './RouteTable';
import { getAllQuestion } from '../../../features/actions/question';
import { editSurvey, getSurveyByRoute } from '../../../features/actions/survey';

function Route() {

    // Initial state
    const { routeId } = useParams();

    const initialRouteState = {
        id: null,
        userId: null,
        costumerId: null,
        date: null,
    };

    const [route, setRoute] = useState(initialRouteState);
    const [costumer, setCostumer] = useState(null);
    const [user, setUser] = useState(null);
    const [survey, setSurvey] = useState([]);
    const [button, setButton] = useState(null);
    const [filled, setFilled] = useState(false);

    // redux hooks
    const dispatch = useDispatch();
    const routeData = useSelector(state => state.route);
    const userData = useSelector(state => state.user);
    const surveyData = useSelector(state => state.survey);
    const costumerData = useSelector(state => state.costumer);
    const questionData = useSelector(state => state.question);
    const [editable, setEditable] = useState(false);

    // Check today date
    const today = (new Date()).toLocaleDateString('en-ca');

    // Handle route state
    const handleInputChange = event => {
        const { name, value } = event.target;
        setRoute({ ...route, [name]: value });
    };

    // Handle Survey state
    const handleSurveyChange = event => {
        let newSurvey = survey;
        if (event.target.checked) {
            newSurvey = newSurvey.filter(id => id !== parseInt(event.target.id))
            newSurvey.push(parseInt(event.target.id))
        } else {
            newSurvey = newSurvey.filter(id => id !== parseInt(event.target.id))
        }
        setSurvey(newSurvey);
    };

    const navigate = useNavigate();

    const refresh = () => {
        setRoute(initialRouteState);
        setCostumer(null);
        setUser(null);
        setSurvey([]);
        setEditable(true);
        navigate('/route/');
    };

    const confirm = useConfirm();


    // handle form submit action
    const handleSubmit = (e) => {
        e.preventDefault();

        if (button == "update") {
            confirm({ title: 'Apakah anda yakin ingin mengubah rute id: ' + route.id + '?', description: 'Tindakan ini bersifat permanen.' })
                .then(() => {
                    dispatch(updateRoute(route))
                        .then(data => {
                            if (user && user.roles[0].name == "ROLE_SURVEY") {
                                dispatch(editSurvey({ date: route.date, costumerId: costumer.id, questionIdList: survey }))
                            };
                            setEditable(false)
                        })
                        .catch(e => {
                            // alert("Maaf, terjadi kesalahan pada server!");
                        });
                })
                .catch(() => { /* ... */ });
        } else {
            dispatch(createRoute(route))
                .then(data => {
                    if (user && user.roles[0].name == "ROLE_SURVEY") {
                        dispatch(editSurvey({ date: route.date, costumerId: costumer.id, questionIdList: survey }))
                            .then(res => {
                                dispatch(getRoutePage({ page: 0 }));
                                refresh();
                            })
                            .catch(e => {
                                // alert("Maaf, terjadi kesalahan pada server!");
                            });
                    } else {
                        dispatch(getRoutePage({ page: 0 }));
                        refresh();
                    }
                })
                .catch(e => {
                    // alert("Maaf, terjadi kesalahan pada server!");
                });
        }
    };

    // load data once page on loaded
    useEffect(() => {
        if(user && user.roles[0].name == "ROLE_SURVEY" && costumer && route.date && costumer.id){
            dispatch(getSurveyByRoute(route.date, costumer.id))
        }
    }, [route]);

    // load data once page on loaded
    useEffect(() => {
        dispatch(getAllCostumer());
        dispatch(getAllUser());
        dispatch(getAllQuestion());
    }, []);

    // update question list once survey data change
    useEffect(() => {
        let questionList = [];
        surveyData.forEach(element => {
            questionList.push(element.questionId.id)
        });
        setSurvey(questionList);
    }, [surveyData]);

    // update user and costumer state
    useEffect(() => {
        if (user && costumer) {
            setRoute({
                ...route,
                userId: user,
                costumerId: costumer
            });
            if (user.roles[0].name == "ROLE_SURVEY" && route.date) {
                dispatch(getSurveyByRoute(route.date, costumer.id))
            };
            let questionList = [];
            surveyData.forEach(element => {
                questionList.push(element.questionId.id)
            });
            setSurvey(questionList);
        }

    }, [user, costumer]);

    // Update route data once route id change
    useEffect(() => {
        if (routeId > 0 && routeData.items) {
            const routeFromDatabase = routeData.items.find(route => route.id == routeId);
            setRoute(routeFromDatabase);
            setUser(userData && userData.find(user => user.id == routeFromDatabase.userId.id));
            setCostumer(costumerData && costumerData.find(costumer => costumer.id == routeFromDatabase.costumerId.id));
            setEditable(false);
        } else {
            refresh();
        }
    }, [routeId, dispatch]);

    return (
        <>
            <Box>
                <Typography variant="h4"
                    sx={{ mx: "auto" }}>
                    Perjalanan
                </Typography>
                <Stack sx={{ mx: "auto" }} >
                    <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
                                <RouteTable />
                            </Grid>
                            <Grid sx={{ p: 1 }}
                                item xs={12} sm={12} md={6} lg={7} xl={8}
                                component="form"
                                onSubmit={handleSubmit}>
                                <FormGroup>
                                    <CardContent>
                                        {userData && userData.length &&
                                            <Autocomplete
                                                required
                                                value={user}
                                                onChange={(event, newUser) => {
                                                    setUser(newUser);
                                                }}
                                                id="user"
                                                options={userData}
                                                getOptionLabel={(option) => option.username}
                                                sx={{ my: 1, width: 220 }}
                                                disabled={!editable}
                                                renderInput={(params) => <TextField required {...params} label="Sales" />}
                                            />
                                        }

                                        {costumerData.length &&
                                            <Autocomplete
                                                value={costumer}
                                                onChange={(event, newCostumer) => {
                                                    setCostumer(newCostumer);
                                                }}
                                                disabled={!editable}
                                                id="costumer"
                                                options={costumerData}
                                                getOptionLabel={(option) => option.name}
                                                sx={{ my: 1, width: 220 }}
                                                renderInput={(params) => <TextField required {...params} label="Costumer" />}
                                            />
                                        }

                                        <TextField
                                            required
                                            sx={{ my: 1, width: 220 }}
                                            id="date"
                                            label="Tanggal Perjalanan"
                                            type="date"
                                            value={route.date != null ? route.date : ""}
                                            onChange={handleInputChange}
                                            name="date"
                                            InputProps={{ inputProps: { min: today } }}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            disabled={!editable}
                                        />
                                        {user && user.roles[0].name == "ROLE_SURVEY" &&
                                            <>
                                                <Divider light sx={{ my: 1 }} />

                                                <Typography variant="h6" sx={{ mx: "auto" }}>
                                                    List Pertanyaan
                                                </Typography>

                                                <Divider light sx={{ my: 1 }} />

                                                <FormGroup>
                                                    {costumer && route.date && costumer.id && questionData && questionData.map((question) => (
                                                        <FormControlLabel key={question.id}
                                                            control={<Checkbox
                                                                id={question.id.toString()}
                                                                checked={survey.find(element => element === question.id) != undefined}
                                                                onChange={handleSurveyChange}
                                                            />}
                                                            label={question.question}
                                                            disabled={!editable} />
                                                    ))}
                                                </FormGroup>
                                            </>}
                                    </CardContent>

                                    <CardActions>
                                        <Stack
                                            width={1}
                                            direction="row"
                                            alignItems="center"
                                            spacing={2}
                                            justifyContent="left"
                                        >
                                            {route.id > 0 ? (
                                                <>
                                                    <Button variant="outlined"
                                                        startIcon={<Cancel />}
                                                        onClick={refresh}>
                                                        Batal
                                                    </Button>
                                                    {editable && (
                                                        <Button id="save"
                                                            name="action"
                                                            value="update"
                                                            variant="contained"
                                                            endIcon={<Save />}
                                                            type="submit"
                                                            onClick={() => (setButton("update"))}>
                                                            Simpan
                                                        </Button>
                                                    )}
                                                    {!editable && (<Button id="edit"
                                                        color='success'
                                                        variant="contained"
                                                        endIcon={<Edit />}
                                                        onClick={() => (setEditable(true))}>
                                                        Edit
                                                    </Button>)}
                                                </>
                                            ) : (
                                                <Button name="action"
                                                    value="new"
                                                    variant="contained"
                                                    endIcon={<SendIcon />}
                                                    type="submit"
                                                    onClick={() => (setButton("new"))}>
                                                    Simpan Data Baru
                                                </Button>
                                            )
                                            }
                                        </Stack>
                                    </CardActions>
                                </FormGroup>
                            </Grid>
                        </Grid>
                    </Card>
                </Stack>
            </Box>
        </>
    );
}

export default Route;

