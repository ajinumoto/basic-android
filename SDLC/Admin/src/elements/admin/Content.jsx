import Box from '@mui/material/Box';
import { Routes, Route } from "react-router-dom";
import { Stack } from '@mui/material';
import Report from './report/Report';
import Costumer from './costumer/Costumer';
import User from './user/User';
import RoutePlan from './route/Route';
import Question from './question/Question';
import Options from './options/Options';
import Dashboard from './dashboard/Dashboard';
import { createTheme, ThemeProvider } from '@mui/material/styles';

function Content() {

    //Routes for every content element

    const theme = createTheme({
        palette: {
            primary: {
                main: '#ffb300',
            },
            secondary: {
                main: '#e65100',
            },
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <Box component="main" sx={{ flexGrow: 1, p: 3, ml: 7, mt: 8 }}>
                <Stack sx={{ mx: "auto" }} width={{ xs: 1, md: 1 }}>
                    <Routes>
                        <Route path="/" element={<Dashboard />} />
                        <Route path="/dashboard" element={<Dashboard />} />
                        <Route path="/report" element={<Report />} />
                        <Route path="/report/:routeId" element={<Report />} />                             
                        <Route path="/tag" element={<Report />} />
                        <Route path="/tag/:tagId" element={<Report />} />                                
                        <Route path="/survey" element={<Report />} />
                        <Route path="/answer/:answerId" element={<Report />} />                        
                        <Route path="/costumer" element={<Costumer />} />
                        <Route path="/costumer/:costumerId" element={<Costumer />} />                           
                        <Route path="/user" element={<User />} />
                        <Route path="/user/:userId" element={<User />} />                      
                        <Route path="/route" element={<RoutePlan />} />
                        <Route path="/route/:routeId" element={<RoutePlan />} />                      
                        <Route path="/question" element={<Question />} />
                        <Route path="/question/:questionId" element={<Question />} />                      
                        <Route path="/options" element={<Options />} />
                        <Route path="/options/:optionsId" element={<Options />} />
                    </Routes>
                </Stack>
            </Box>
        </ThemeProvider>
    );
}

export default Content;

