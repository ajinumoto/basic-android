
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUserPage } from '../../../features/actions/user';
import { useCallback, useEffect, useState } from 'react';
import { Fab, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TablePagination, TableRow, Typography } from '@mui/material';
import { Refresh } from '@mui/icons-material';
export default function ProductTable() {

  // Initial state

  const initialState = {
    currentIndex: -1,
    searchTitle: "",
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const pageSizes = [3, 6, 9];

  const [state, setState] = useState(initialState);

  const navigate = useNavigate();

  const handleRowClick = useCallback(
    (row) => navigate('/user/' + row, { replace: true }, [navigate])
  );

  // redux hooks
  const dispatch = useDispatch();
  const userData = useSelector(state => state.user);

  // Pagination parameter
  const getRequestParams = (searchTitle, page, pageSize, filter) => {
    let params = {};

    if (searchTitle) {
      params["name"] = searchTitle;
    }
    if (page) {
      params["page"] = page - 1;
    }
    if (pageSize) {
      params["size"] = pageSize;
    }
    if (filter) {
      params["filter"] = filter;
    }

    return params;
  }

  // get data from API
  const retrieveData = () => {
    const { searchTitle, page, pageSize, filter } = state;
    const params = getRequestParams(searchTitle, page, pageSize, filter);

    dispatch(getUserPage(params));
  }

  // Update data once page/size changing
  useEffect(() => {
    retrieveData();
  }, [state, dispatch]);

  return (
    <Box>

      <Fab color="primary" aria-label="add" sx={{
        position: "fixed",
        top: (theme) => theme.spacing(10),
        right: (theme) => theme.spacing(5)
      }} onClick={retrieveData}>
        <Refresh />
      </Fab>

      <Paper sx={{ boxShadow: 5, p: 1 }}>
        {userData.items &&
          (<>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">

                <TableHead>
                  <TableRow >
                    <StyledTableCell align="center">Status</StyledTableCell>
                    <StyledTableCell>Username</StyledTableCell>
                    <StyledTableCell align="center">Role</StyledTableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {userData.items.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row.id) }}
                    >
                      <StyledTableCell align="center">{row.status == "active" ? "Aktif" : "Tidak Aktif"}</StyledTableCell>
                      <StyledTableCell align="left">{row.username}</StyledTableCell>
                      <StyledTableCell align="center">{row.roles[0].name == "ROLE_SURVEY" ? "Survey" : "Tagging"}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            <TablePagination
              component="div"
              count={userData.totalItems && userData.totalItems}
              page={userData.currentPage && userData.currentPage}
              onPageChange={(event, page) => setState({ ...state, page: page + 1 })}
              rowsPerPage={state.pageSize}
              rowsPerPageOptions={pageSizes}
              onRowsPerPageChange={(event) => setState({ ...state, page: 1, pageSize: event.target.value })}
            />
          </>
          )}
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));