import Box from '@mui/material/Box';
import { Avatar, Button, Card, CardActions, CardContent, FormGroup, Stack, styled, TextField, Typography, Grid, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { postImage } from '../../../features/actions/file';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { createUser, updateUser } from '../../../features/actions/user';
import { useConfirm } from 'material-ui-confirm';
import { Cancel, Edit, Save } from '@mui/icons-material';
import uploadFileService from '../../../features/services/uploadFileService';
import UserTable from './UserTable';

function User() {

    // get user id from url parameter
    const { userId } = useParams();

    // Initial state
    const initialUserState = {
        id: null,
        username: null,
        name: null,
        email: null,
        roles: null,
        status: "not active",
        image: null,
    };

    const initialImageState = {
        currentFile: undefined,
        previewImage: undefined,
        message: "",

        imageInfos: [],
    };

    const [user, setUser] = useState(initialUserState);
    const [image, setImage] = useState(initialImageState);
    const [button, setButton] = useState(null);
    const [editable, setEditable] = useState(false);

    // Redux
    const dispatch = useDispatch();
    const userData = useSelector(state => state.user);

    // Handle action
    const handleInputChange = event => {
        const { name, value } = event.target;
        setUser({ ...user, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (button == "update") {
            confirm({ title: 'Apakah anda yakin ingin mengubah User id: ' + user.id + '?', description: 'Tindakan ini bersifat permanen.' })
                .then(() => {
                    dispatch(updateUser(user))
                        .then(data => {
                        })
                        .catch(e => {
                            alert("Maaf, terjadi kesalahan pada server!");
                        });
                })
                .catch(() => { /* ... */ });
        } else {
            dispatch(createUser(user))
                .then(data => {
                    refresh();
                })
                .catch(e => {
                    alert("Maaf, terjadi kesalahan pada server!");
                });
        }
    };

    // re initiated data
    const refresh = () => {
        setUser(initialUserState);
        setImage(initialImageState);
    };

    const confirm = useConfirm();

    const Input = styled('input')({
        display: 'none',
    });

    // Select file from local disk
    const selectFile = (event) => {
        if (event.target.files[0].size >= 500e3) {
            window.alert("Please upload a file smaller than 500 KB");
        } else {
            setImage({
                ...image,
                currentFile: event.target.files[0],
                previewImage: URL.createObjectURL(event.target.files[0]),
                message: ""
            });
        }
    };

    // upload file to API server 
    const upload = () => {
        const imageId = Date.now();
        dispatch(postImage(image.currentFile, ("user-" + imageId)))
            .then((response) => {
                setUser({
                    ...user,
                    image: "http://localhost:8080/api/files/get/" + "user-" + imageId + "/" + image.currentFile.name,
                });
                return uploadFileService.getFiles(image.currentFile.name, ("user-" + imageId));
            })
            .catch((err) => {
                alert("Maaf, terjadi kesalahan pada server!");
                setImage({
                    ...image,
                    message: "Could not upload the image!",
                    currentFile: undefined,
                });
            });
    }

    // Call upload once current file changing
    useEffect(() => {
        if (image.currentFile != undefined) {
            upload();
        };
    }, [image.currentFile])

    // update user data if link/user id changing
    useEffect(() => {
        if (userId > 0 && userData.length != 0) {
            const userFromDatabase = userData.items.find(user => user.id == userId);
            setUser(userFromDatabase);
            setImage({
                ...image,
                previewImage: userFromDatabase.image
            })
            setEditable(false);
        } else {
            refresh();
        }
    }, [userId, dispatch]);

    return (
        <>
            <Box>
                <Typography variant="h4"
                    sx={{ mx: "auto" }}>
                    Aktivasi Akun
                </Typography>
                <Stack sx={{ mx: "auto" }} >
                    <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                        <Grid container>
                            <Grid item xs={12} sm={12} md={6} lg={5} xl={4}
                                sx={{ mb: 2 }}>
                                <UserTable />
                            </Grid>
                            {user.roles != null &&
                                <Grid item xs={12} sm={12} md={6} lg={7} xl={8}
                                    component="form"
                                    onSubmit={handleSubmit}>
                                    <FormGroup>
                                        <CardContent sx={{ py: 0, display: 'flex' }}>
                                            <Stack alignItems="center"
                                                direction="column"
                                                spacing={2}>

                                                {image.previewImage ? (
                                                    <Avatar alt="User Preview"
                                                        src={image.previewImage}
                                                        variant="square"
                                                        sx={{ width: 200, height: 200, border: 2 }} />
                                                ) : (<Avatar variant="square"
                                                    sx={{ width: 200, height: 200, border: 2 }}>
                                                    No Image
                                                </Avatar>)}
                                                <label htmlFor="contained-button-file">
                                                    <Input accept="image/*"
                                                        id="contained-button-file"
                                                        type="file"
                                                        onChange={selectFile}
                                                        disabled={!editable} />
                                                    <Button variant="contained"
                                                        component="span"
                                                        disabled={!editable} >
                                                        Unggah Gambar
                                                    </Button>
                                                </label>
                                                <label htmlFor="icon-button-file">
                                                    <Input accept="image/*"
                                                        id="icon-button-file"
                                                        type="file" />
                                                </label>
                                            </Stack>
                                        </CardContent>
                                        < CardContent >
                                            <TextField sx={{ my: 1 }}
                                                fullWidth
                                                id="outlined-basic"
                                                label="Username"
                                                variant="outlined"
                                                value={user.username != null ? user.username : ""}
                                                disabled />

                                            <TextField sx={{ my: 1 }}
                                                fullWidth id="outlined-basic"
                                                label="Email" variant="outlined"
                                                value={user.email != null ? user.email : ""}
                                                disabled />

                                            <TextField sx={{ my: 1 }}
                                                fullWidth
                                                id="outlined-basic"
                                                label="Role"
                                                variant="outlined"
                                                value={user.roles[0].name != null ? (user.roles[0].name == "ROLE_SURVEY" ? "Survey" : "Tagging") : ""}
                                                disabled />

                                            <TextField required
                                                sx={{ my: 1 }}
                                                inputProps={{
                                                    pattern: "^[a-zA-Z ]+$",
                                                    minLength: 3,
                                                    maxLength: 50,
                                                    title: "Nama hanya berupa huruf!"
                                                }}
                                                fullWidth
                                                id="outlined-basic"
                                                label="Nama"
                                                variant="outlined"
                                                value={user.name != null ? user.name : ""}
                                                onChange={handleInputChange}
                                                name="name"
                                                disabled={!editable} />

                                            <FormControl required
                                                fullWidth
                                                sx={{ my: 1 }}>
                                                <InputLabel id="status">Status</InputLabel>
                                                <Select
                                                    labelId="status"
                                                    id="status"
                                                    value={user.status}
                                                    label="Status"
                                                    onChange={handleInputChange}
                                                    name="status"
                                                    disabled={!editable}
                                                >
                                                    <MenuItem value={"not active"}>Tidak Aktif</MenuItem>
                                                    <MenuItem value={"active"}>Aktif</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </CardContent>

                                        <CardActions>
                                            <Stack
                                                width={1}
                                                direction="row"
                                                alignItems="center"
                                                spacing={2}
                                                justifyContent="left"
                                            >
                                                {user.id > 0 && (
                                                    <>
                                                        <Button variant="outlined"
                                                            startIcon={<Cancel />}
                                                            onClick={refresh}>
                                                            Batal
                                                        </Button>
                                                        {editable && (
                                                            <Button id="save"
                                                                name="action"
                                                                value="update"
                                                                variant="contained"
                                                                endIcon={<Save />}
                                                                type="submit"
                                                                onClick={() => (setButton("update"))}>
                                                                Simpan
                                                            </Button>
                                                        )}
                                                        {!editable && (<Button id="edit"
                                                            color='success'
                                                            variant="contained"
                                                            endIcon={<Edit />}
                                                            onClick={() => (setEditable(true))}>
                                                            Edit
                                                        </Button>)}
                                                    </>
                                                )}
                                            </Stack>
                                        </CardActions>
                                    </FormGroup>
                                </Grid>
                            }
                        </Grid>
                    </Card>
                </Stack>
            </Box>
        </>
    );
}

export default User;

