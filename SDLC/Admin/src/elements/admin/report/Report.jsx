import Box from '@mui/material/Box';
import { Card, Stack, Typography, Grid } from '@mui/material';
import { useSelector } from 'react-redux';
import ReportTable from './ReportTable';
import TaggingTable from './TaggingTable';
import AnswerTable from './AnswerTable';
import Tagging from './Tagging';

function Report() {

    // Redux hook
    const role = useSelector(state => state.report.role);

    return (
        <Box>
            <Typography variant="h4" sx={{ mx: "auto" }}>
                Laporan
            </Typography>

            <Stack sx={{ mx: "auto" }} >
                <Card sx={{ minWidth: 275, my: 2, p: 1, boxShadow: 5 }}>
                    <Grid container spacing={1}>

                        {/* Elemen Tabel */}
                        <Grid item xs={12} sm={12} md={6} lg={5} xl={4}>
                            <ReportTable />
                        </Grid>

                        {/* Element Detail */}
                        {role && (
                            <>
                                <Grid item xs={12} sm={12} md={6} lg={7} xl={8}>
                                    {role == "ROLE_TAGGING" ? (<TaggingTable />) : (<AnswerTable />)}
                                </Grid>
                                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                    {role == "ROLE_TAGGING" ? (<Tagging />) : (<></>)}
                                </Grid>
                            </>
                        )}

                    </Grid>
                </Card>
            </Stack>
        </Box>
    );
}

export default Report;

