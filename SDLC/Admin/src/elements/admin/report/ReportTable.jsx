
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutePage } from '../../../features/actions/route';
import { set } from '../../../features/actions/report';
import { useCallback, useEffect, useState } from 'react';
import { Fab, FormControl, InputLabel, MenuItem, Select, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TablePagination, TableRow, Typography } from '@mui/material';
import { Assignment, AssignmentTurnedIn, Refresh } from '@mui/icons-material';

export default function ReportTable() {

  // Initial state
  const initialState = {
    currentIndex: -1,
    searchTitle: 0,
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const pageSizes = [3, 6, 9];

  const [state, setState] = useState(initialState);

  // Redux
  const dispatch = useDispatch();
  const routeData = useSelector(state => state.route);

  const handleRowClick = useCallback(
    (row) => {
      const data = {
        role: row.userId.roles[0].name,
        route: row.id,
        id: null
      };
      dispatch(set(data));
    }
  );

  // Parameter for pagination
  const getRequestParams = (searchTitle, page, pageSize, filter) => {
    let params = {};

    if (searchTitle) {
      params["name"] = searchTitle;
    }
    if (page) {
      params["page"] = page - 1;
    }
    if (pageSize) {
      params["size"] = pageSize;
    }
    if (filter) {
      params["filter"] = filter;
    }

    return params;
  }

  //get data from API
  const retrieveData = () => {
    const { searchTitle, page, pageSize, filter } = state;
    const params = getRequestParams(searchTitle, page, pageSize, filter);

    dispatch(getRoutePage(params));
  }

  // Update data
  useEffect(() => {
    retrieveData();
  }, [state, dispatch]);

  return (
    <Box>

      {/* Tombol refresh data */}
      <Fab color="primary" aria-label="add" sx={{
        position: "fixed",
        top: (theme) => theme.spacing(10),
        right: (theme) => theme.spacing(5)
      }} onClick={retrieveData}>
        <Refresh />
      </Fab>

      <Paper sx={{ width: '100%', boxShadow: 5, p: 1 }}>

        {/* Filtering */}
        <FormControl required fullWidth sx={{ my: 1 }}>
          <InputLabel id="type">Status</InputLabel>
          <Select
            labelId="type"
            id="type"
            value={state.searchTitle}
            label="Jenis"
            onChange={(event) => setState({ ...state, page: 1, searchTitle: event.target.value })}
            name="typeId"
          >
            <MenuItem value={0}>Semua</MenuItem>
            <MenuItem value={1}>Selesai</MenuItem>
            <MenuItem value={2}>Belum Selesai</MenuItem>
          </Select>
        </FormControl>

        {/* Data tabel */}
        {routeData.items && routeData.items.length > 0 ?
          <>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">

                <TableHead>
                  <TableRow >
                    <StyledTableCell align="center">Jenis</StyledTableCell>
                    <StyledTableCell align="center">Tanggal</StyledTableCell>
                    <StyledTableCell>Seles</StyledTableCell>
                    <StyledTableCell>Pelanggan</StyledTableCell>
                    <StyledTableCell align="center">Status</StyledTableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {routeData.items.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row) }}
                    >
                      <StyledTableCell component="th" scope="row" align="center">
                        {row.userId.roles[0].name == "ROLE_TAGGING" ? "Tagging" : "Survey"}
                      </StyledTableCell>
                      <StyledTableCell align="center">{row.date}</StyledTableCell>
                      <StyledTableCell align="left">{row.userId.name}</StyledTableCell>
                      <StyledTableCell align="left">{row.costumerId.name}</StyledTableCell>
                      <StyledTableCell align="center">{row.completedDate ? <AssignmentTurnedIn sx={{ color: '#00a152' }} /> : <Assignment sx={{ color: 'orange' }} />}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>

              </Table>
            </TableContainer>

            {/* Sesuaikan pagination */}
            <TablePagination
              component="div"
              count={routeData.totalItems && routeData.totalItems}
              page={routeData.currentPage && routeData.currentPage}
              onPageChange={(event, page) => setState({ ...state, page: page + 1 })}
              rowsPerPage={state.pageSize}
              rowsPerPageOptions={pageSizes}
              onRowsPerPageChange={(event) => setState({ ...state, page: 1, pageSize: event.target.value })}
            />
          </> :
          <Typography variant="h8" sx={{ mx: "auto" }}>
            Tidak ada data.
          </Typography>
        }
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));