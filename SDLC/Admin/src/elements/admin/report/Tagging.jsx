import { Avatar, Box, CardActions, CardContent, FormGroup, Stack, TextField, Typography } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { createTagging, deleteTagging, updateTagging } from '../../../features/actions/tagging';
import { useConfirm } from 'material-ui-confirm';


function Report() {
    //Initial State
    const tagId = useSelector(state => state.report.id);

    const initialTaggingState = {
        id: null,
        imageUrl: null,
        serialNumber: null,
        contractNumber: null,
        assetDate: null,
        completedDate: null,
    };
    const [tagging, setTagging] = useState(initialTaggingState);

    // redux hook
    const dispatch = useDispatch();
    const tagData = useSelector(state => state.tagging);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setTagging({ ...tagging, [name]: value });
    };

    const updateTaggingData = (e) => {
        e.preventDefault();

        confirm({ title: 'Apakah anda yakin untuk memperbarui tag id: ' + tagging.id + '?', description: 'Tindakan ini bersifat permanen.' })
            .then(() => {
                dispatch(updateTagging(tagging))
                    .then(data => {
                        // console.log(data);
                    })
                    .catch(e => {
                        alert("Maaf, terjadi kesalahan pada server!");
                    });
            })
            .catch(() => {
                alert("Maaf, terjadi kesalahan pada server!");
            });
    };

    const refresh = () => {
        setTagging(initialTaggingState);
    };

    const confirm = useConfirm();

    const deleteConfirm = () => {
        confirm({ title: 'Apakah anda yakin untuk menghapus Tag id: ' + tagging.id + '?', description: 'Tindakan ini bersifat permanen.' })
            .then(() => {
                dispatch(deleteTagging(tagging.id))
                    .then(data => {
                    })
                    .catch(e => {
                        alert("Maaf, terjadi kesalahan pada server!");

                    });
                refresh();
            })
            .catch(() => {
                alert("Maaf, terjadi kesalahan pada server!");
            });
    };

    useEffect(() => {
        if (tagId > 0 && tagData.length != 0) {
            tagData.map((tag) => {
                if (tag.id == tagId) {
                    setTagging(tag);
                }
            });
        }
    }, [tagId]);

    return (
        <Box component="form" onSubmit={updateTaggingData} sx={{ width: '100%', boxShadow: 5, mt: 1, p: 1 }}>
            <FormGroup>
                <Typography variant="h6" sx={{ mx: "auto" }}>
                    Detail Tag
                </Typography>
                <CardContent>
                    {tagging.imageUrl ? (
                        <Avatar alt="User Preview"
                            src={"http://localhost:8080/api/files/get/tag/" + tagging.imageUrl.substring(70)} variant="square"
                            sx={{
                                width: 252, height: 189, border: 2, transform: "rotate(0.25turn)", my: 6
                            }} />
                    ) : (<Avatar variant="square"
                        sx={{ width: 200, height: 200, border: 2 }}>
                        No Image
                    </Avatar>)}
                    <TextField disabled
                        sx={{ my: 1 }}
                        fullWidth
                        id="outlined-basic"
                        label="Lokasi Gambar"
                        variant="outlined"
                        value={tagging.imageUrl != null ? tagging.imageUrl : ""}
                        onChange={handleInputChange}
                        name="imageUrl" />
                    <TextField disabled
                        sx={{ my: 1, width: 220 }}
                        id="date"
                        label="Tanggal Nonaktif Asset"
                        type="date"
                        value={tagging.assetDate != null ? tagging.assetDate : ""}
                        onChange={handleInputChange}
                        name="assetDate"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField disabled
                        sx={{ my: 1 }}
                        fullWidth
                        id="outlined-basic"
                        label="Nomor Serial"
                        variant="outlined"
                        value={tagging.serialNumber != null ? tagging.serialNumber : ""}
                        onChange={handleInputChange}
                        name="serialNumber" />
                    <TextField disabled
                        sx={{ my: 1 }}
                        fullWidth
                        id="outlined-basic"
                        label="Nomor Kontrak"
                        variant="outlined"
                        value={tagging.contractNumber != null ? tagging.contractNumber : ""}
                        onChange={handleInputChange}
                        name="contractNumber" />
                </CardContent>

                <CardActions>
                    <Stack
                        width={1}
                        direction="row"
                        alignItems="center"
                        spacing={2}
                        justifyContent="left"
                    >
                    </Stack>
                </CardActions>
            </FormGroup>
        </Box>
    );
}

export default Report;

