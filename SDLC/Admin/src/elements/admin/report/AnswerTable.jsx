
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getAnswerByRoute } from '../../../features/actions/answer';
import { useEffect } from 'react';
import { styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { change } from '../../../features/actions/report';

export default function SurveyTable() {

  // redux
  const routeId = useSelector(state => state.report.route);

  const dispatch = useDispatch();
  const answerData = useSelector(state => state.answer);

  const getAnswerRoute = () => {
    dispatch(getAnswerByRoute(routeId))
      .then(data => {
      })
      .catch(e => {
        // console.log(e);
      });
  };

  const handleRowClick = (row) => {
    dispatch(change({ name: "id", value: row }))
  }

  useEffect(() => {
    if (routeId > 0) {
      getAnswerRoute()
    };

  }, [routeId]);

  return (
    <Box>
      <Paper sx={{ width: '100%', boxShadow: 5, p: 1 }}>

        <Typography variant="h6">
          SURVEY
        </Typography>

        {answerData && answerData.length > 0 ?
          <>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                
                <TableHead>
                  <TableRow >
                    <StyledTableCell>Pertanyaan</StyledTableCell>
                    <StyledTableCell align="left">Jawaban</StyledTableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {answerData && answerData.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row.id) }}
                    >
                      <StyledTableCell align="left">{row.question.question}</StyledTableCell>
                      <StyledTableCell align="left">{row.answer}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>

              </Table>
            </TableContainer>
          </> :
          <Typography variant="h8" sx={{ mx: "auto" }}>
            Tidak ada data.
          </Typography>
        }
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));