
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getTaggingByRoute } from '../../../features/actions/tagging';
import { change } from '../../../features/actions/report';
import { useEffect } from 'react';
import { styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
export default function ReportTable() {

  // Redux hooks
  const routeId = useSelector(state => state.report.route);

  const dispatch = useDispatch();
  const tagData = useSelector(state => state.tagging);

  // update redux data from API
  const getTaggingRoute = () => {
    dispatch(getTaggingByRoute(routeId))
      .then(data => {
      })
      .catch(e => {
        // console.log(e);
      });
  };


  const handleRowClick = (row) => {
    dispatch(change({ name: "id", value: row }))
  }

  // Update taging route if route id changing
  useEffect(() => {
    if (routeId > 0) {
      getTaggingRoute()
    };

  }, [routeId]);

  return (
    <Box>
      <Paper sx={{ width: '100%', boxShadow: 5, p: 1 }}>
        <Typography variant="h6">
          TAG
        </Typography>
        {tagData && tagData.length > 0 ?
          <>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow >
                    <StyledTableCell>Kontrak</StyledTableCell>
                    <StyledTableCell align="left">Serial</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tagData && tagData.map((row) => (
                    <StyledTableRow key={row.id} onClick={() => { handleRowClick(row.id) }}
                    >
                      <StyledTableCell align="left">{row.contractNumber}</StyledTableCell>
                      <StyledTableCell align="left">{row.serialNumber}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </> :
          <Typography variant="h8" sx={{ mx: "auto" }}>
            Tidak ada data.
          </Typography>
        }
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 16,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));