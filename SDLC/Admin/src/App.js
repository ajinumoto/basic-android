import { Box } from '@mui/material';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import Content from './elements/admin/Content'
import LoginSide from './elements/signin/LoginSide'
import MiniDrawer from './elements/admin/MiniDrawer';
import { useCallback, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from './features/actions/auth';
import eventBus from './common/EventBus';
import userService from './features/services/userService';
import { ConfirmProvider } from "material-ui-confirm";
import { createTheme } from '@mui/material/styles';

function App() {

  const [showAdminBoard, setShowAdminBoard] = useState(false);
  const [content, setContent] = useState("");
  const dispatch = useDispatch();

  const user = useSelector(state => state.auth);

  const theme = createTheme({
    palette: {
      primary: {
        main: '#ffb300',
      },
      secondary: {
        main: '#e65100',
      },
    },
  });

  const logOut = useCallback(() => {
    dispatch(logout());
  }, [dispatch]);

  useEffect(() => {
    eventBus.on("logout", () => {
      logOut();
      // console.log("Logout...");
    });
    return () => {
      eventBus.remove("logout");
    };
  }, [logOut]);

  useEffect(() => {
    // console.log(user);
    userService.getAdminBoard().then(
      (response) => {
        setContent(response.data);
        setShowAdminBoard(true);
      },
      (error) => {
        const _content =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
        // console.log(error.toString());
        setContent(_content);
        setShowAdminBoard(false);

        if (error.response) {
          eventBus.dispatch("logout");
        }
      }
    );
  }, []);

  return (
    <BrowserRouter>
      {
        showAdminBoard ?
          (<Box>
            <ConfirmProvider>
              <MiniDrawer />
              <Content />
            </ConfirmProvider>
          </Box>) : (<LoginSide />)
      }
    </BrowserRouter>
  );
}

export default App;
