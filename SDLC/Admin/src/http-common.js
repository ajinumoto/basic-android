import axios from "axios";
import authHeader from "./features/services/authHeader";

export default axios.create({
  // baseURL: "http://3620-110-138-66-53.ngrok.io/api",
  baseURL: "http://localhost:8080/api",
  headers: authHeader(),
});
