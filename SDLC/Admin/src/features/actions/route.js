import {
  CREATE_ROUTE,
  GET_ALL_ROUTE,
  GET_ROUTE,
  UPDATE_ROUTE,
  DELETE_ROUTE,
  DELETE_ALL_ROUTE,
  GET_ROUTE_PAGE,
} from "./types";

import routeService from "../services/routeService";

export const createRoute = (data) => async (dispatch) => {
  try {
    const res = await routeService.create(data);

    dispatch({
      type: CREATE_ROUTE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllRoute = () => async (dispatch) => {
  try {
    const res = await routeService.getAll();

    dispatch({
      type: GET_ALL_ROUTE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getRoute = (id) => async (dispatch) => {
  try {
    const res = await routeService.get(id);

    dispatch({
      type: GET_ROUTE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateRoute = (data) => async (dispatch) => {
  try {
    const res = await routeService.update(data);

    dispatch({
      type: UPDATE_ROUTE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteRoute = (id) => async (dispatch) => {
  try {
    const res = await routeService.remove(id);

    dispatch({
      type: DELETE_ROUTE,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllRoute = () => async (dispatch) => {
  try {
    const res = await routeService.removeAll();

    dispatch({
      type: DELETE_ALL_ROUTE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getRoutePage = (params) => async (dispatch) => {
  try {
    const res = await routeService.getPage(params);
    dispatch({
      type: GET_ROUTE_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};
