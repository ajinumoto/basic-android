import {
  CREATE_OPTIONS,
  NEW_OPTIONS,
  GET_ALL_OPTIONS,
  GET_OPTIONS,
  UPDATE_OPTIONS,
  DELETE_OPTIONS,
  DELETE_ALL_OPTIONS,
  GET_OPTIONS_PAGE,
  GET_FROM_OPTIONS,
} from "./types";

import optionsService from "../services/optionsService";

export const createOptions = (data) => async (dispatch) => {
  try {
    const res = await optionsService.create(data);

    dispatch({
      type: CREATE_OPTIONS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const newOptions = (data) => async (dispatch) => {
  try {
    const res = await optionsService.create(data);

    dispatch({
      type: NEW_OPTIONS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllOptions = () => async (dispatch) => {
  try {
    const res = await optionsService.getAll();

    dispatch({
      type: GET_ALL_OPTIONS,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getOptionByQuestion = (id) => async (dispatch) => {
  try {
    const res = await optionsService.getFromQuestion(id);
    dispatch({
      type: GET_FROM_OPTIONS,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getOptions = (id) => async (dispatch) => {
  try {
    const res = await optionsService.get(id);

    dispatch({
      type: GET_OPTIONS,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateOptions = (data) => async (dispatch) => {
  try {
    const res = await optionsService.update(data);

    dispatch({
      type: UPDATE_OPTIONS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteOptions = (id) => async (dispatch) => {
  try {
    const res = await optionsService.remove(id);

    dispatch({
      type: DELETE_OPTIONS,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllOptions = () => async (dispatch) => {
  try {
    const res = await optionsService.removeAll();

    dispatch({
      type: DELETE_ALL_OPTIONS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getOptionsPage = (params) => async (dispatch) => {
  try {
    const res = await optionsService.getPage(params);
    dispatch({
      type: GET_OPTIONS_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};
