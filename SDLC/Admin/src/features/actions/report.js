import routeService from "../services/routeService";
import {
  SET_REPORT,
  HANDLE_CHANGE_REPORT,
  RESET_REPORT,
  GET_REPORT,
  GET_REPORT_PAGE,
} from "./types";

export const set = (params) => ({
      type: SET_REPORT,
      payload: params,
});

export const change = (params) => ({
      type: HANDLE_CHANGE_REPORT,
      payload: params,
});

export const get = () => ({
      type: GET_REPORT
});

export const getReportPage = (params) => async (dispatch) => {
      try {
        const res = await routeService.getPage(params);
        dispatch({
          type: GET_REPORT_PAGE,
          payload: res.data,
        });
      } catch (err) {
        // console.log(err);
      }
    };

export const reset = () => ({
      type: RESET_REPORT
});