import {
  POST_IMAGE,
  GET_IMAGE,
  GET_ALL_IMAGE,
} from "./types";


import uploadFilesService from "../services/uploadFileService";

export const postImage = (file, pathName) => async (dispatch) => {
  try {
    const res = await uploadFilesService.upload(file, pathName);
    dispatch({
      type: POST_IMAGE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    // console.log(err);
    return Promise.reject(err);
  }
};

export const getImage = (file, pathName) => async (dispatch) => {
  try {
    const res = await uploadFilesService.getFiles(file, pathName);

    dispatch({
      type: GET_IMAGE,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    // console.log(err);
    return Promise.reject(err);
  }
};