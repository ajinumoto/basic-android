import {
  CREATE_ANSWER,
  GET_ALL_ANSWER,
  GET_ANSWER,
  UPDATE_ANSWER,
  DELETE_ANSWER,
  DELETE_ALL_ANSWER,
  GET_ANSWER_PAGE,
  GET_ANSWER_BY_ROUTE,
} from "./types";

import answerService from "../services/answerService";

export const createAnswer = (data) => async (dispatch) => {
  try {
    const res = await answerService.create(data);

    dispatch({
      type: CREATE_ANSWER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllAnswer = () => async (dispatch) => {
  try {
    const res = await answerService.getAll();

    dispatch({
      type: GET_ALL_ANSWER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getAnswer = (id) => async (dispatch) => {
  try {
    const res = await answerService.get(id);

    dispatch({
      type: GET_ANSWER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateAnswer = (data) => async (dispatch) => {
  try {
    const res = await answerService.update(data);

    dispatch({
      type: UPDATE_ANSWER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAnswer = (id) => async (dispatch) => {
  try {
    const res = await answerService.remove(id);

    dispatch({
      type: DELETE_ANSWER,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllAnswer = () => async (dispatch) => {
  try {
    const res = await answerService.removeAll();

    dispatch({
      type: DELETE_ALL_ANSWER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAnswerPage = (params) => async (dispatch) => {
  try {
    const res = await answerService.getPage(params);
    dispatch({
      type: GET_ANSWER_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getAnswerByRoute = (id) => async (dispatch) => {
  try {
    const res = await answerService.getAnswerByRoute(id);
    dispatch({
      type: GET_ANSWER_BY_ROUTE,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: GET_ANSWER_BY_ROUTE,
      payload: [],
    });
  }
};
