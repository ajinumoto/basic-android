import {
  CREATE_SURVEY,
  GET_ALL_SURVEY,
  GET_SURVEY,
  UPDATE_SURVEY,
  EDIT_SURVEY,
  DELETE_SURVEY,
  DELETE_ALL_SURVEY,
  GET_SURVEY_PAGE,
  GET_SURVEY_BY_ROUTE,
} from "./types";

import surveyService from "../services/surveyService";

export const createSurvey = (data) => async (dispatch) => {
  try {
    const res = await surveyService.create(data);

    dispatch({
      type: CREATE_SURVEY,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllSurvey = () => async (dispatch) => {
  try {
    const res = await surveyService.getAll();

    dispatch({
      type: GET_ALL_SURVEY,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getSurvey = (id) => async (dispatch) => {
  try {
    const res = await surveyService.get(id);

    dispatch({
      type: GET_SURVEY,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateSurvey = (data) => async (dispatch) => {
  try {
    const res = await surveyService.update(data);

    dispatch({
      type: UPDATE_SURVEY,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const editSurvey = (data) => async (dispatch) => {
  try {
    const res = await surveyService.edit(data);

    dispatch({
      type: EDIT_SURVEY,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteSurvey = (id) => async (dispatch) => {
  try {
    const res = await surveyService.remove(id);

    dispatch({
      type: DELETE_SURVEY,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteByDateAndCostumerId = (date, id, questionList) => async (dispatch) => {
  try {
    const res = await surveyService.remove(id);

    dispatch({
      type: DELETE_SURVEY,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllSurvey = () => async (dispatch) => {
  try {
    const res = await surveyService.removeAll();

    dispatch({
      type: DELETE_ALL_SURVEY,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getSurveyPage = (params) => async (dispatch) => {
  try {
    const res = await surveyService.getPage(params);
    dispatch({
      type: GET_SURVEY_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getSurveyByRoute = (date, costumerId) => async (dispatch) => {
  try {
    const res = await surveyService.getSurveyByRoute(date, costumerId);
    dispatch({
      type: GET_SURVEY_BY_ROUTE,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: GET_SURVEY_BY_ROUTE,
      payload: [],
    });
  }
};
