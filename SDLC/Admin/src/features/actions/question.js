import {
  CREATE_QUESTION,
  GET_ALL_QUESTION,
  GET_QUESTION,
  UPDATE_QUESTION,
  DELETE_QUESTION,
  DELETE_ALL_QUESTION,
  GET_QUESTION_PAGE,
} from "./types";

import questionService from "../services/questionService";

export const createQuestion = (data) => async (dispatch) => {
  try {
    const res = await questionService.create(data);

    dispatch({
      type: CREATE_QUESTION,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllQuestion = () => async (dispatch) => {
  try {
    const res = await questionService.getAll();

    dispatch({
      type: GET_ALL_QUESTION,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getQuestion = (id) => async (dispatch) => {
  try {
    const res = await questionService.get(id);

    dispatch({
      type: GET_QUESTION,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateQuestion = (data) => async (dispatch) => {
  try {
    const res = await questionService.update(data);

    dispatch({
      type: UPDATE_QUESTION,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteQuestion = (id) => async (dispatch) => {
  try {
    const res = await questionService.remove(id);

    dispatch({
      type: DELETE_QUESTION,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllQuestion = () => async (dispatch) => {
  try {
    const res = await questionService.removeAll();

    dispatch({
      type: DELETE_ALL_QUESTION,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getQuestionPage = (params) => async (dispatch) => {
  try {
    const res = await questionService.getPage(params);
    dispatch({
      type: GET_QUESTION_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};
