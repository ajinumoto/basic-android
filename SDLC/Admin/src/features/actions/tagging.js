import {
  CREATE_TAGGING,
  GET_ALL_TAGGING,
  GET_TAGGING,
  UPDATE_TAGGING,
  DELETE_TAGGING,
  DELETE_ALL_TAGGING,
  GET_TAGGING_PAGE,
  GET_TAGGING_BY_ROUTE,
} from "./types";

import taggingService from "../services/taggingService";

export const createTagging = (data) => async (dispatch) => {
  try {
    const res = await taggingService.create(data);

    dispatch({
      type: CREATE_TAGGING,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllTagging = () => async (dispatch) => {
  try {
    const res = await taggingService.getAll();

    dispatch({
      type: GET_ALL_TAGGING,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getTagging = (id) => async (dispatch) => {
  try {
    const res = await taggingService.get(id);

    dispatch({
      type: GET_TAGGING,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateTagging = (data) => async (dispatch) => {
  try {
    const res = await taggingService.update(data);

    dispatch({
      type: UPDATE_TAGGING,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteTagging = (id) => async (dispatch) => {
  try {
    const res = await taggingService.remove(id);

    dispatch({
      type: DELETE_TAGGING,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllTagging = () => async (dispatch) => {
  try {
    const res = await taggingService.removeAll();

    dispatch({
      type: DELETE_ALL_TAGGING,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getTaggingPage = (params) => async (dispatch) => {
  try {
    const res = await taggingService.getPage(params);
    dispatch({
      type: GET_TAGGING_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getTaggingByRoute = (id) => async (dispatch) => {
  try {
    const res = await taggingService.getTaggingByRoute(id);
    dispatch({
      type: GET_TAGGING_BY_ROUTE,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: GET_TAGGING_BY_ROUTE,
      payload: [],
    });
  }
};
