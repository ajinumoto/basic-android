import {
  CREATE_USER,
  GET_ALL_USER,
  GET_USER,
  UPDATE_USER,
  DELETE_USER,
  DELETE_ALL_USER,
  GET_USER_PAGE,
} from "./types";

import userService from "../services/userService";

export const createUser = (data) => async (dispatch) => {
  try {
    const res = await userService.create(data);

    dispatch({
      type: CREATE_USER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllUser = () => async (dispatch) => {
  try {
    const res = await userService.getAllSeles();

    dispatch({
      type: GET_ALL_USER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getUser = (id) => async (dispatch) => {
  try {
    const res = await userService.get(id);

    dispatch({
      type: GET_USER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateUser = (data) => async (dispatch) => {
  try {
    const res = await userService.update(data);

    dispatch({
      type: UPDATE_USER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    const res = await userService.remove(id);

    dispatch({
      type: DELETE_USER,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllUser = () => async (dispatch) => {
  try {
    const res = await userService.removeAll();

    dispatch({
      type: DELETE_ALL_USER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getUserPage = (params) => async (dispatch) => {
  try {
    const res = await userService.getPage(params);
    dispatch({
      type: GET_USER_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};
