import {
  CREATE_COSTUMER,
  GET_ALL_COSTUMER,
  GET_COSTUMER,
  UPDATE_COSTUMER,
  DELETE_COSTUMER,
  DELETE_ALL_COSTUMER,
  GET_COSTUMER_PAGE,
} from "./types";

import costumerService from "../services/costumerService";

export const createCostumer = (data) => async (dispatch) => {
  try {
    const res = await costumerService.create(data);

    dispatch({
      type: CREATE_COSTUMER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getAllCostumer = () => async (dispatch) => {
  try {
    const res = await costumerService.getAll();

    dispatch({
      type: GET_ALL_COSTUMER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const getCostumer = (id) => async (dispatch) => {
  try {
    const res = await costumerService.get(id);

    dispatch({
      type: GET_COSTUMER,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};

export const updateCostumer = (data) => async (dispatch) => {
  try {
    const res = await costumerService.update(data);

    dispatch({
      type: UPDATE_COSTUMER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteCostumer = (id) => async (dispatch) => {
  try {
    const res = await costumerService.remove(id);

    dispatch({
      type: DELETE_COSTUMER,
      payload: { id },
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteAllCostumer = () => async (dispatch) => {
  try {
    const res = await costumerService.removeAll();

    dispatch({
      type: DELETE_ALL_COSTUMER,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const getCostumerPage = (params) => async (dispatch) => {
  try {
    const res = await costumerService.getPage(params);
    dispatch({
      type: GET_COSTUMER_PAGE,
      payload: res.data,
    });
  } catch (err) {
    // console.log(err);
  }
};
