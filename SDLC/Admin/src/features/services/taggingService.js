import http from "../../http-common";

const getAll = () => {
  return http.get("/tagging");
};

const get = id => {
  return http.get(`/tagging/${id}`);
};

const create = data => {
  return http.post("/tagging", data);
};

const update = (data) => {
  return http.put(`/tagging/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/tagging/${id}`);
};

const removeAll = () => {
  return http.delete(`/tagging`);
};

const getPage = (params) => {
  return http.get("/tagging/page", { params });
};

const getTaggingByRoute = (id) => {
  return http.get("/tagging/routeplan", { params: { routePlanId: id } });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
  getTaggingByRoute,
};
