import http from "../../http-common";

const getAdminBoard = () => {
  return http.get("/test/admin");
};

const getAll = () => {
  return http.get("/user");
};

const getAllSeles = () => {
  return http.get("/seles");
};

const get = id => {
  return http.get(`/user/${id}`);
};

const create = data => {
  return http.post("/user", data);
};

const update = (data) => {
  return http.put(`/user/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/user/${id}`);
};

const removeAll = () => {
  return http.delete(`/user`);
};

const getPage = (params) => {
  return http.get("/seles/page", { params });
};

export default {
  getAdminBoard,
  getAllSeles,
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
};