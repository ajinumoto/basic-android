import http from "../../http-common";

const getAll = () => {
  return http.get("/question");
};

const get = id => {
  return http.get(`/question/${id}`);
};

const create = data => {
  return http.post("/question", data);
};

const update = (data) => {
  return http.put(`/question/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/question/${id}`);
};

const removeAll = () => {
  return http.delete(`/question`);
};

const getPage = (params) => {
  return http.get("/question/page", { params });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
};
