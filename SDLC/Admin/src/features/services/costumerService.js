import http from "../../http-common";

const getAll = () => {
  return http.get("/costumer");
};

const get = id => {
  return http.get(`/costumer/${id}`);
};

const create = data => {
  return http.post("/costumer", data);
};

const update = (data) => {
  return http.put(`/costumer/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/costumer/${id}`);
};

const removeAll = () => {
  return http.delete(`/costumer`);
};

const getPage = (params) => {
  return http.get("/costumer/page", { params });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
};
