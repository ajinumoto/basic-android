import http from "../../http-common";

const getAll = () => {
  return http.get("/answer");
};

const get = id => {
  return http.get(`/answer/${id}`);
};

const create = data => {
  return http.post("/answer", data);
};

const update = (data) => {
  return http.put(`/answer/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/answer/${id}`);
};

const removeAll = () => {
  return http.delete(`/answer`);
};

const getPage = (params) => {
  return http.get("/answer/page", { params });
};

const getAnswerByRoute = (id) => {
  return http.get("/answer/routeplan", { params: { routePlanId: id } });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
  getAnswerByRoute,
};
