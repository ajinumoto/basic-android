import http from "../../http-common";

const getAll = () => {
  return http.get("/survey");
};

const get = id => {
  return http.get(`/survey/${id}`);
};

const create = data => {
  return http.post("/survey", data);
};

const update = (data) => {
  return http.put(`/survey/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/survey/${id}`);
};

const edit = data => {
  return http.post(`/survey/edit`,data);
};

const removeAll = () => {
  return http.delete(`/survey`);
};

const getPage = (params) => {
  return http.get("/survey/page", { params });
};

const getSurveyByRoute = (date,costumerId) => {
  return http.get("/survey/find", { params: { date: date,  costumerId: costumerId} });
};

export default {
  getAll,
  get,
  create,
  update,
  edit,
  remove,
  removeAll,
  getPage,
  getSurveyByRoute,
};
