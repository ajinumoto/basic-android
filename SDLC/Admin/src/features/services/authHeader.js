import eventBus from "../../common/EventBus";

// JWT = header.payload.signature

const parseJwt = (token) => {
  try {
    //ENCODE JWT payload
    return JSON.parse(atob(token.split(".")[1]));
  } catch (e) {
    return null;
  }
};

export default function authHeader() {
  const user = JSON.parse(localStorage.getItem("user"));

  if (user) {
    const decodedJwt = parseJwt(user.access_token);
    // Check JWT exp
    if (decodedJwt.exp * 1000 < Date.now()) {
      eventBus.dispatch("logout");
    }
  }

  if (user && user.access_token) {
    return { "Authorization": "Bearer " + user.access_token };
  } else {
    return {};
  }
}

//header for multipart
export function authHeaderType() {
  const user = JSON.parse(localStorage.getItem("user"));
  if (user && user.access_token) {
    return {
      Authorization: "Bearer " + user.access_token,
      "Content-Type": "multipart/form-data",
    };
  } else {
    return {};
  }
}
