import http from "../../http-common";
import {authHeaderType} from "./authHeader";

class uploadFilesService {
  upload(file, pathName) {
    let formData = new FormData();

    formData.append("file", file);
    formData.append("pathName", pathName);

    return http.post("/files/upload", formData, {
      headers: authHeaderType()
    });
  }

  getFiles(fileName, pathName) {
    return http.get("/files/get/"+ pathName +"/" + fileName);
  }
}

export default new uploadFilesService();
