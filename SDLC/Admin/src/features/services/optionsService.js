import http from "../../http-common";

const getAll = () => {
  return http.get("/option");
};
const getFromQuestion = (id) => {
  return http.get(`/option/byquestion`, { params: { questionId: id } });
};

const get = id => {
  return http.get(`/option/${id}`);
};

const create = data => {
  return http.post("/option", data);
};

const update = (data) => {
  return http.put(`/option/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/option/${id}`);
};

const removeAll = () => {
  return http.delete(`/option`);
};

const getPage = (params) => {
  return http.get("/option/page", { params });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
  getFromQuestion,
};
