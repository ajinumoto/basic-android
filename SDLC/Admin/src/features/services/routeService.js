import http from "../../http-common";

const getAll = () => {
  return http.get("/routeplan");
};

const get = id => {
  return http.get(`/routeplan/${id}`);
};

const create = data => {
  return http.post("/routeplan", data);
};

const update = (data) => {
  return http.put(`/routeplan/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/routeplan/${id}`);
};

const removeAll = () => {
  return http.delete(`/routeplan`);
};

const getPage = (params) => {
  return http.get("/routeplan/page", { params });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
};
