import http from "../../http-common";

const getAll = () => {
  return http.get("/report");
};

const get = id => {
  return http.get(`/report/${id}`);
};

const create = data => {
  return http.post("/report", data);
};

const update = (data) => {
  return http.put(`/report/${data.id}`, data);
};

const remove = id => {
  return http.delete(`/report/${id}`);
};

const removeAll = () => {
  return http.delete(`/report`);
};

const getPage = (params) => {
  return http.get("/report/page", { params });
};


export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  getPage,
};
