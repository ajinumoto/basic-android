import {
  CREATE_ANSWER,
  GET_ALL_ANSWER,
  GET_ANSWER,
  UPDATE_ANSWER,
  DELETE_ANSWER,
  DELETE_ALL_ANSWER,
  GET_ANSWER_PAGE,
  GET_ANSWER_BY_ROUTE,
} from "../actions/types";

const initialState = [];

const answerReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_ANSWER:
      return [...state, payload];

    case GET_ANSWER:
      return payload;

    case GET_ALL_ANSWER:
      return payload;

    case GET_ANSWER_PAGE:
      return payload;

    case GET_ANSWER_BY_ROUTE:
      return payload;

    case UPDATE_ANSWER:
      return state.map((answer) => {
        if (answer.id === payload.id) {
          return {
            ...answer,
            ...payload,
          };
        } else {
          return answer;
        }
      });

    case DELETE_ANSWER:
      return state.filter(({ id }) => id !== payload.id);

    case DELETE_ALL_ANSWER:
      return [];

    default:
      return state;
  }
};

export default answerReducer;