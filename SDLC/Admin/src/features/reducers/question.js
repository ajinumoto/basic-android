import {
  CREATE_QUESTION,
  GET_ALL_QUESTION,
  GET_QUESTION,
  UPDATE_QUESTION,
  DELETE_QUESTION,
  DELETE_ALL_QUESTION,
  GET_QUESTION_PAGE,
} from "../actions/types";

const initialState = [];

const questionReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_QUESTION:
      return {
        ...state,
        items : [payload,...state.items]
      }

    case GET_QUESTION:
      return payload;

    case GET_ALL_QUESTION:
      return payload;

    case GET_QUESTION_PAGE:
      return payload;

    case UPDATE_QUESTION:
      return {
        ...state,
        items: state.items.map((question) => {
          if (question.id === payload.id) {
            return {
              ...question,
              ...payload,
            };
          } else {
            return question;
          }
        })
      };

    case DELETE_QUESTION:
      return {
        ...state,
        items: state.items.filter(({ id }) => id !== payload.id)
      };

    case DELETE_ALL_QUESTION:
      return [];

    default:
      return state;
  }
};

export default questionReducer;