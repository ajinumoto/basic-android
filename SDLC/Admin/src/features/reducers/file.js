import {
  POST_IMAGE,
  GET_IMAGE,
  GET_ALL_IMAGE,
} from "../actions/types";

const initialState = [];

const fileReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case POST_IMAGE:
      return [...state, payload];

    case GET_IMAGE:
      return payload;

    default:
      return state;
  }
};

export default fileReducer;