import {
  CREATE_COSTUMER,
  GET_ALL_COSTUMER,
  GET_COSTUMER,
  UPDATE_COSTUMER,
  DELETE_COSTUMER,
  DELETE_ALL_COSTUMER,
  GET_COSTUMER_PAGE,
} from "../actions/types";

const initialState = [];

const costumerReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_COSTUMER:
      return {
        ...state,
        items : [payload,...state.items]
        // items : [payload,...state.items.slice(0, -1)]
      }

    case GET_COSTUMER:
      return payload;

    case GET_ALL_COSTUMER:
      return payload;

    case GET_COSTUMER_PAGE:
      return payload;

    case UPDATE_COSTUMER:
      return {
        ...state,
        items : state.items.map((costumer) => {
        if (costumer.id === payload.id) {
          return {
            ...costumer,
            ...payload,
          };
        } else {
          return costumer;
        }
      })};

    case DELETE_COSTUMER:
      return {
        ...state,
        items : state.items.filter(({ id }) => id !== payload.id)
      };

    case DELETE_ALL_COSTUMER:
      return [];

    default:
      return state;
  }
};

export default costumerReducer;