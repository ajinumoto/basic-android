import {
  CREATE_SURVEY,
  GET_ALL_SURVEY,
  GET_SURVEY,
  UPDATE_SURVEY,
  EDIT_SURVEY,
  DELETE_SURVEY,
  DELETE_ALL_SURVEY,
  GET_SURVEY_PAGE,
  GET_SURVEY_BY_ROUTE,
} from "../actions/types";

const initialState = [];

const surveyReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_SURVEY:
      return [...state, payload];

    case GET_SURVEY:
      return payload;

    case GET_ALL_SURVEY:
      return payload;

    case GET_SURVEY_PAGE:
      return payload;

    case GET_SURVEY_BY_ROUTE:
      return payload;

    case UPDATE_SURVEY:
      return state.map((survey) => {
        if (survey.id === payload.id) {
          return {
            ...survey,
            ...payload,
          };
        } else {
          return survey;
        }
      });

    case EDIT_SURVEY:
      return payload;

    case DELETE_SURVEY:
      return state.filter(({ id }) => id !== payload.id);

    case DELETE_ALL_SURVEY:
      return [];

    default:
      return state;
  }
};

export default surveyReducer;