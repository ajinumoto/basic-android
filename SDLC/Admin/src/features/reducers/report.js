import {
  SET_REPORT,
  HANDLE_CHANGE_REPORT,
  RESET_REPORT,
  GET_REPORT,
  GET_REPORT_PAGE,
} from "../actions/types";

const initialState = {
  role: null,
  route: null,
  id: null
};

const reportReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_REPORT:
      return {
        role: payload.role,
        route: payload.route,
        id: payload.id
      };

    case HANDLE_CHANGE_REPORT:
      return { ...state, [payload.name]: payload.value };

    case GET_REPORT:
      return payload;

    case RESET_REPORT:
      return initialState;

    case GET_REPORT_PAGE:
      return payload;

    default:
      return state;
  }
};

export default reportReducer;