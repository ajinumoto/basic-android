import {
  CREATE_TAGGING,
  GET_ALL_TAGGING,
  GET_TAGGING,
  UPDATE_TAGGING,
  DELETE_TAGGING,
  DELETE_ALL_TAGGING,
  GET_TAGGING_PAGE,
  GET_TAGGING_BY_ROUTE,
} from "../actions/types";

const initialState = [];

const taggingReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_TAGGING:
      return [...state, payload];

    case GET_TAGGING:
      return payload;

    case GET_ALL_TAGGING:
      return payload;

    case GET_TAGGING_PAGE:
      return payload;

    case GET_TAGGING_BY_ROUTE:
      return payload;

    case UPDATE_TAGGING:
      return state.map((tagging) => {
        if (tagging.id === payload.id) {
          return {
            ...tagging,
            ...payload,
          };
        } else {
          return tagging;
        }
      });

    case DELETE_TAGGING:
      return state.filter(({ id }) => id !== payload.id);

    case DELETE_ALL_TAGGING:
      return [];

    default:
      return state;
  }
};

export default taggingReducer;