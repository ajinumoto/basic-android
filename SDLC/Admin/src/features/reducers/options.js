import {
  CREATE_OPTIONS,
  NEW_OPTIONS,
  GET_ALL_OPTIONS,
  GET_OPTIONS,
  UPDATE_OPTIONS,
  DELETE_OPTIONS,
  DELETE_ALL_OPTIONS,
  GET_OPTIONS_PAGE,
  GET_FROM_OPTIONS,
} from "../actions/types";

const initialState = [];

const optionsReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_OPTIONS:
      return {
        ...state,
        items : [payload,...state.items]
      }

    case NEW_OPTIONS:
      return [payload,...state]

    case GET_OPTIONS:
      return payload;

    case GET_FROM_OPTIONS:
      return payload;

    case GET_ALL_OPTIONS:
      return payload;

    case GET_OPTIONS_PAGE:
      return payload;

    case UPDATE_OPTIONS:
      return {
        ...state,
        items: state.items.map((options) => {
          if (options.id === payload.id) {
            return {
              ...options,
              ...payload,
            };
          } else {
            return options;
          }
        })
      };

    case DELETE_OPTIONS:
      return {
        ...state,
        items: state.items.filter(({ id }) => id !== payload.id)
      };

    case DELETE_ALL_OPTIONS:
      return [];

    default:
      return state;
  }
};

export default optionsReducer;