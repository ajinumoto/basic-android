import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import file from "./file"
import costumer from "./costumer"
import user from "./user"
import route from "./route"
import question from "./question"
import options from "./options"
import report from "./report"
import tagging from "./tagging"
import answer from "./answer"
import survey from "./survey"

export default combineReducers({
  auth,
  message,
  file,
  route,
  question,
  options,
  report,
  costumer,
  user,
  tagging,
  answer,
  survey,
});
