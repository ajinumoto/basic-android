import {
  CREATE_ROUTE,
  GET_ALL_ROUTE,
  GET_ROUTE,
  UPDATE_ROUTE,
  DELETE_ROUTE,
  DELETE_ALL_ROUTE,
  GET_ROUTE_PAGE,
} from "../actions/types";

const initialState = [];

const routeReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_ROUTE:
      return {
        ...state,
        items : [payload,...state.items]
      }

    case GET_ROUTE:
      return payload;

    case GET_ALL_ROUTE:
      return payload;

    case GET_ROUTE_PAGE:
      return payload;

    case UPDATE_ROUTE:
      return {
        ...state,
        items: state.items.map((route) => {
          if (route.id === payload.id) {
            return {
              ...route,
              ...payload,
            };
          } else {
            return route;
          }
        })
      };

    case DELETE_ROUTE:
      return {
        ...state,
        items: state.items.filter(({ id }) => id !== payload.id)
      };

    case DELETE_ALL_ROUTE:
      return [];

    default:
      return state;
  }
};

export default routeReducer;