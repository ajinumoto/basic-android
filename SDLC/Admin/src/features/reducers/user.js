import {
  CREATE_USER,
  GET_ALL_USER,
  GET_USER,
  UPDATE_USER,
  DELETE_USER,
  DELETE_ALL_USER,
  GET_USER_PAGE,
} from "../actions/types";

const initialState = [];

const userReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_USER:
      return {
        ...state,
        items : [payload,...state.items]
      }

    case GET_USER:
      return payload;

    case GET_ALL_USER:
      return payload;

    case GET_USER_PAGE:
      return payload;

    case UPDATE_USER:
      return {
        ...state,
        items : state.items.map((user) => {
        if (user.id === payload.id) {
          return {
            ...user,
            ...payload,
          };
        } else {
          return user;
        }
      })};

    case DELETE_USER:
      return {
        ...state,
        items : state.items.filter(({ id }) => id !== payload.id)
      };

    case DELETE_ALL_USER:
      return [];

    default:
      return state;
  }
};

export default userReducer;