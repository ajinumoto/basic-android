
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { getRoutePage } from '../../../features/actions/route';
import { change, set } from '../../../features/actions/report';
import { useCallback, useEffect, useState } from 'react';
import { Collapse, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TablePagination, TableRow, TextField, Typography } from '@mui/material';
import { KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material';
import { getTaggingByRoute } from '../../../features/actions/tagging';

export default function ReportTable() {

  const initialState = {
    currentIndex: -1,
    searchTitle: "",
    filter: "",

    page: 1,
    count: 0,
    pageSize: 3,
  };

  const pageSizes = [3, 6, 9];

  const columns = [
    { field: 'date', headerName: 'Tanngal' },
    { field: 'username', headerName: 'Sales', valueGetter: (params) => params.row.userId.username },
    { field: 'name', headerName: 'Pelanggan', valueGetter: (params) => params.row.costumerId.name },
  ];

  const [state, setState] = useState(initialState);
  const [open, setOpen] = useState({});

  const dispatch = useDispatch();
  const reduxData = useSelector(state => state.route);
  const taggingData = useSelector(state => state.tagging);

  const handleRowClick = useCallback(
    (row) => {
      const data = {
        role: row.userId.roles[0].name,
        route: row.id,
        id: null
      };
      dispatch(set(data));
    }
  );

  const routeId = useSelector(state => state.report.route);

  const getTaggingRoute = () => {
    dispatch(getTaggingByRoute(routeId))
      .then(data => {
      })
      .catch(e => {
        // console.log(e);
      });
  };

  const handleDataClick = (row) => {
    dispatch(change({ name: "id", value: row }))
  }

  const handleInputChange = event => {
    const { name, value } = event.target;
    setState({ ...state, [name]: value });
  };

  const onChangeSearchTitle = (e) => {
    const searchTitle = e.target.value;

    setState({
      ...state,
      searchTitle: searchTitle,
    });
  }

  const getRequestParams = (searchTitle, page, pageSize, filter) => {
    let params = {};

    if (searchTitle) {
      params["name"] = searchTitle;
    }
    if (page) {
      params["page"] = page - 1;
    }
    if (pageSize) {
      params["size"] = pageSize;
    }
    if (filter) {
      params["filter"] = filter;
    }

    return params;
  }

  const retrieveData = () => {
    const { searchTitle, page, pageSize, filter } = state;
    const params = getRequestParams(searchTitle, page, pageSize, filter);

    dispatch(getRoutePage(params));
  }

  useEffect(() => {
    retrieveData();
  }, [state, dispatch]);

  return (
    <Box>
      <Typography variant="h6" sx={{ mx: "auto" }}>
        Daftar Laporan
      </Typography>
      <Paper sx={{ width: '100%', my: 2, boxShadow: 3, p: 1 }}>

        {reduxData.items && reduxData.items.length > 0 ?
          <>
            <Grid container spacing={1} sx={{ padding: 1 }}>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextField id="outlined-search" label="Kata Kunci" type="search" fullWidth />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>

                <FormControl fullWidth>
                  <InputLabel id="filter-label">Filter</InputLabel>
                  <Select
                    labelId="filter-label"
                    value={state.filter != null ? state.filter : ""}
                    onChange={handleInputChange}
                    name="filter"
                    id="filter"
                    label="Filter"
                  >
                    <MenuItem value={"name"}>Nama</MenuItem>
                    <MenuItem value={"address"}>Alamat</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                <TableHead>
                  <TableRow >
                    <StyledTableCell></StyledTableCell>
                    <StyledTableCell>Jenis</StyledTableCell>
                    <StyledTableCell>Tanggal</StyledTableCell>
                    <StyledTableCell>Seles</StyledTableCell>
                    <StyledTableCell align="left">Pelanggan</StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {reduxData.items.map((row) => (
                    <>
                      <StyledTableRow key={row.id} onClick={() => { handleRowClick(row) }}
                      >
                        <StyledTableCell>
                          <IconButton
                            aria-label="expand row"
                            size="small"
                            onClick={() => setOpen((prev) => ({ [row.id]: !prev[row.id] }))}
                          >
                            {open[row.id] ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                          </IconButton>
                        </StyledTableCell>
                        <StyledTableCell component="th" scope="row">
                          {row.userId.roles[0].name == "ROLE_TAGGING" ? "Tagging" : "Survey"}
                        </StyledTableCell>
                        <StyledTableCell align="left">{row.date}</StyledTableCell>
                        <StyledTableCell align="left">{row.userId.username}</StyledTableCell>
                        <StyledTableCell align="left">{row.costumerId.name}</StyledTableCell>
                      </StyledTableRow>
                      {row.userId.roles[0].name == "ROLE_TAGGING" ? (
                        <>
                          <TableRow>
                            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                              <Collapse in={open[row.id]} timeout="auto" unmountOnExit>
                                {open[row.id] && (
                                  <Box sx={{ margin: 1 }}>
                                  <Table size="small" aria-label="tagging">
                                    <TableHead>
                                      <TableRow >
                                        <TableCell>ID</TableCell>
                                        <TableCell>Kontrak</TableCell>
                                        <TableCell align="left">Serial</TableCell>
                                      </TableRow>
                                    </TableHead>
                                    <TableBody>
                                      {taggingData && taggingData.map((row) => (
                                        <TableRow key={row.id} onClick={() => { handleDataClick(row.id) }}
                                        >
                                          <TableCell component="th" scope="row">
                                            {row.id}
                                          </TableCell>
                                          <TableCell align="left">{row.contractNumber}</TableCell>
                                          <TableCell align="left">{row.serialNumber}</TableCell>
                                        </TableRow>
                                      ))}
                                    </TableBody>
                                  </Table>
                                </Box>
                                )}                                
                              </Collapse>
                            </TableCell>
                          </TableRow>
                        </>
                      ) : (
                        <></>
                      )}
                    </>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              component="div"
              count={reduxData.totalItems && reduxData.totalItems}
              page={reduxData.currentPage && reduxData.currentPage}
              onPageChange={(event, page) => setState({ ...state, page: page + 1 })}
              rowsPerPage={state.pageSize}
              rowsPerPageOptions={pageSizes}
              onRowsPerPageChange={(event) => setState({ ...state, page: 1, pageSize: event.target.value })}
            />
          </> :
          <Typography variant="h8" sx={{ mx: "auto" }}>
            Tidak ada data.
          </Typography>
        }
      </Paper>
    </Box>
  );
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));