import { Box, Button, CardActions, CardContent, FormGroup, Stack, TextField, Typography } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { deleteAnswer, updateAnswer } from '../src/features/actions/answer';
import { useConfirm } from 'material-ui-confirm';
import { Save } from '@mui/icons-material';

function Answer() {

    // Initial state
    const answerId = useSelector(state => state.report.id);

    const initialAnswerState = {
        id: null,
        question: null,
        answer: null,
        routePlanId: null,
        questionId: null,
    };
    const [answer, setAnswer] = useState(initialAnswerState);

    // redux
    const dispatch = useDispatch();
    const answerData = useSelector(state => state.answer);

    // handle change in answer state
    const handleInputChange = event => {
        const { name, value } = event.target;
        setAnswer({ ...answer, [name]: value });
    };

    const updateAnswerData = (e) => {
        e.preventDefault();

        confirm({ title: 'Apakah anda yakin untuk memperbarui jawaban id: ' + answer.id + '?', description: 'Tindakan ini bersifat permanen.' })
            .then(() => {
                dispatch(updateAnswer({
                    id: answer.id,
                    question: { id: answer.questionId },
                    answer: answer.answer,
                    routePlan: { id: answer.routePlanId },

                }))
                    .then(data => {
                    })
                    .catch(e => {
                        // console.log(e);
                    });
            })
            .catch(() => { /* ... */ });
    };

    const refresh = () => {
        setAnswer(initialAnswerState);
    };

    const confirm = useConfirm();

    const deleteConfirm = () => {
        confirm({ title: 'Apakah anda yakin untuk menghapus jawaban id: ' + answer.id + '?', description: 'Tindakan ini bersifat permanen.' })
            .then(() => {
                dispatch(deleteAnswer(answer.id))
                    .then(data => {
                    })
                    .catch(e => {
                        alert("Maaf, terjadi kesalahan pada server!");
                    });
                refresh();
            })
            .catch((error) => {
                alert("Maaf, terjadi kesalahan pada server!");
            });
    };

    useEffect(() => {
        if (answerId > 0 && answerData.length != 0) {
            answerData.map((ans) => {
                if (ans.id == answerId) {
                    setAnswer({
                        id: ans.id,
                        question: ans.question.question,
                        answer: ans.answer,
                        routePlanId: ans.routePlan.id,
                        questionId: ans.question.id
                    });
                }
            });
        }
    }, [answerId]);

    return (
        <Box component="form" onSubmit={updateAnswerData}>
            <FormGroup>
                <Typography variant="h6" sx={{ mx: "auto" }}>
                    Detail Jawaban
                </Typography>
                <CardContent>
                    <TextField required sx={{ my: 1 }} fullWidth id="outlined-basic" label="Pertanyaan" variant="outlined" value={answer.question != null ? answer.question : ""} onChange={handleInputChange} name="question" />
                    <TextField required sx={{ my: 1 }} fullWidth id="outlined-basic" label="Jawaban" variant="outlined" value={answer.answer != null ? answer.answer : ""} onChange={handleInputChange} name="answer" />
                </CardContent>

                <CardActions>
                    <Stack
                        width={1}
                        direction="row"
                        alignItems="center"
                        spacing={2}
                        justifyContent="left"
                    >
                        {answer.id > 0 &&
                            <>
                                <Button variant="outlined" startIcon={<DeleteIcon />} color="error" onClick={deleteConfirm}>
                                    Hapus
                                </Button>
                                <Button variant="contained" endIcon={<Save />} type="submit">
                                    Simpan
                                </Button>
                            </>
                        }
                    </Stack>
                </CardActions>
            </FormGroup>
        </Box>

    );
}

export default Answer;

