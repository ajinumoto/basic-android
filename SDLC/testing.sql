create database nexsoftdbv5;
use nexsoftdbv5;
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_TAGGING');
INSERT INTO roles(name) VALUES('ROLE_SURVEY');

INSERT INTO `users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (1,'admin@gmail.com','http://localhost:8080/api/files/get/user-1652425666927/28908511.jpg',NULL,'$2a$10$beUccdDA74FZKNSJYQ0DIOiDw4oQsCaESLT0DWGGNH0n/x/Qp1jlq','active','admin');
INSERT INTO `users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (2,'tagging@gmail.com','http://localhost:8080/api/files/get/user-1652425666927/28908511.jpg',NULL,'$2a$10$Q0jeZyFH1Bix2JqZNFh55eRuPPeac7msw8WGFcitEenp1cA5VIYpC','active','tagging');
INSERT INTO `users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (3,'survey@gmail.com','http://localhost:8080/api/files/get/user-1652425666927/28908511.jpg',NULL,'$2a$10$jsM5sLz96eU0nWbVnLMarO/YWs.gcl1t/GXVjgdtCRlG1SAN.JJ.S','active','survey');

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES ('2', '2');
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES ('3', '3');

INSERT INTO `question_type` (`name`) VALUES ('TEXT');
INSERT INTO `question_type` (`name`) VALUES ('RADIO');
INSERT INTO `question_type` (`name`) VALUES ('CHECK_BOX');

INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (1,'Jalan Patimura No. 52, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926985365/20200719_194935~2.jpg','Yolanda Agus Septiana');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (2,'Jalan Ngembul Raya 22, Kalipare, Kabupaten Malang, Jawa Timur, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926861499/20211016_152544.jpg','Lalu Alwinda Firdaus');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (3,'Jalan Sukarno No. 33, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926854470/20210927_165619.jpg','Maulana Akbar Rifaid');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (4,'Jalan Ayar Indah No. 22, Cilinaya Indah, Kota Bandung, Jawa Barat, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926848223/20210914_182413.jpg','Ajeng Risa Sulistia');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (5,'Jalan Kartini No. 42, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926770017/PXL_20210327_013136721.MP.jpg','Bayu Idiajir');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (6,'Jalan Adem sfer 44, Kalipare, Kabupaten Malang, Jawa Timur, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926739110/20210513_095048.jpg','Arya Zulfikar Akbar');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (8,'Jalan Pahlawan No. 22, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926679169/20200808_073350.jpg','Rusdi Zulkaidi');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (11,'Gang Masjid No. 32, Desa Selat, Kecamatan Narmada, Kabupaten Lombok  Barat',NULL,'http://localhost:8080/api/files/get/costumer-1651926890384/IMG_20160514_153712_1.jpg','Fairuz Abadi');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (12,'Jalan Maju No. 120, Bertais, Lombok Barat',NULL,'http://localhost:8080/api/files/get/costumer-1651926633972/20210905_165838.jpg','Hasby Arrahman');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (13,'Jalan Musium 32, Ampenan, Kota Mataram, Nusa Tenggara Barat',NULL,'http://localhost:8080/api/files/get/costumer-1651926597857/COLOR_POP.jpg','Zalizah Isnaini');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (14,'Jalan Babakan Indah 21, Sweta, Lombok Barat',NULL,'http://localhost:8080/api/files/get/costumer-1651926532741/P_20160825_142642.jpg','Mozaik Al Qharomi');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (15,'Gomong Raya No. 15, Mataram Barat, Nusa Tenggara Barat - Indonesia',NULL,'http://localhost:8080/api/files/get/costumer-1651926525540/P_20160312_113445.jpg','Adam Danis Maulana');
INSERT INTO `costumer` (`id`,`address`,`delete_date`,`image`,`name`) VALUES (19,'Jalan Pemuda No. 62, Cakranegara, Mataram, Nusa Tenggara Barat',NULL,'http://localhost:8080/api/files/get/costumer-1651822072953/fotopribadi.jpg','Adjie Satryo P');

INSERT INTO `question` (`question`, `type_id`) VALUES ('Siapa nama lengkap pemilik Toko ini?', '1');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Apakah Toko ini menerima pembayaran menggunakan eWallet?', '2');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Produk apa saja yang dijual pada toko ini?', '3');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Seberapa mahal harga produk yang dijual di Toko ini jika dibandingkan dengan harga pada mini Market?', '2');

INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Ya',2);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Tidak',2);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Makanan',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Minuman',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Makanan Ringan',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Mahal',4);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Murah',4);