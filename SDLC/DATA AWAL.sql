create database nexsoftdbv1;
use nexsoftdbv1;
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO roles(name) VALUES('ROLE_TAGGING');
INSERT INTO roles(name) VALUES('ROLE_SURVEY');

INSERT INTO `nexsoftdb`.`users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (1,'admin@gmail.com',NULL,NULL,'$2a$10$beUccdDA74FZKNSJYQ0DIOiDw4oQsCaESLT0DWGGNH0n/x/Qp1jlq',NULL,'admin');
INSERT INTO `nexsoftdb`.`users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (2,'tagging@gmail.com',NULL,NULL,'$2a$10$Q0jeZyFH1Bix2JqZNFh55eRuPPeac7msw8WGFcitEenp1cA5VIYpC',NULL,'tagging');
INSERT INTO `nexsoftdb`.`users` (`id`,`email`,`image`,`name`,`password`,`status`,`username`) VALUES (3,'survey@gmail.com',NULL,NULL,'$2a$10$jsM5sLz96eU0nWbVnLMarO/YWs.gcl1t/GXVjgdtCRlG1SAN.JJ.S',NULL,'survey');

INSERT INTO `nexsoftdbv1`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `nexsoftdbv1`.`user_roles` (`user_id`, `role_id`) VALUES ('2', '2');
INSERT INTO `nexsoftdbv1`.`user_roles` (`user_id`, `role_id`) VALUES ('3', '3');

INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('Bambang Susanto', 'Jalan Patimura No. 52, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia');
INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('Gus Dur', 'Jalan Ngembul Raya 22, Kalipare, Kabupaten Malang, Jawa Timur, Indonesia');
INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('BJ Habiebie', 'Jalan Sukarno No. 33, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia');
INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('Megawati Soekarnoputri', 'Jalan Ayar Indah No. 22, Cilinaya Indah, Kota Bandung, Jawa Barat, Indonesia');
INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('Ahmad Dermawan', 'Jalan Kartini No. 42, Cilinaya Indah, Kota Mataram, Nusa Tenggara Barat, Indonesia');
INSERT INTO `nexsoftdbv1`.`costumer` (`name`, `address`) VALUES ('Rizal Hasan', 'Jalan Adem sfer 44, Kalipare, Kabupaten Malang, Jawa Timur, Indonesia');

INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Mataram', 'Indonesia', 'Ampenan', 'Nusa Tenggara Barat', 'Jalan Gotong Royong No.13', '83113');
INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Tanggerang', 'Indonesia', 'Gading Serpong', 'Banten', 'Jalan Wangi  No.33', '12387');
INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Bandung', 'Indonesia', 'Ubud', 'Jawa Barat', 'Jalan Melati  No.12', '45675');
INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Malang', 'Indonesia', 'Kepanjen', 'Jawa Timur', 'Jalan Durian No.432', '55645');
INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Denpasar', 'Indonesia', 'Kuta', 'Bali', 'Jalan Duku  No.23', '64756');
INSERT INTO `nexsoftdb`.`address` (`city`, `country`, `district`, `province`, `street`, `zip`) VALUES ('Surabaya', 'Indonesia', 'Pasar Wangi', 'Jawa Timur', 'Jalan Pasar Minggu  No.42', '57757');

INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('Bambang Susanto', '1');
INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('Gus Dur', '2');
INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('BJ Habiebie', '3');
INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('Megawati Soekarnoputri', '4');
INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('Ahmad Dermawan', '5');
INSERT INTO `nexsoftdb`.`costumer` (`name`, `address_id`) VALUES ('Rizal Hasan', '6');

INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-26', '1', '3');
INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-26', '2', '3');
INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-27', '3', '3');
INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-26', '4', '2');
INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-26', '5', '2');
INSERT INTO `route_plan` (`date`, `costumer_id`, `user_id`) VALUES ('2022-04-27', '6', '2');

INSERT INTO `question_type` (`name`) VALUES ('TEXT');
INSERT INTO `question_type` (`name`) VALUES ('RADIO');
INSERT INTO `question_type` (`name`) VALUES ('CHECK_BOX');

INSERT INTO `question` (`question`, `type_id`) VALUES ('Siapa nama lengkap pemilik Toko ini?', '1');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Apakah Toko ini menerima pembayaran menggunakan eWallet?', '2');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Produk apa saja yang dijual pada toko ini?', '3');
INSERT INTO `question` (`question`, `type_id`) VALUES ('Seberapa mahal harga produk yang dijual di Toko ini jika dibandingkan dengan harga pada mini Market?', '2');

INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Ya',2);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Tidak',2);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Makanan',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Minuman',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Makanan Ringan',3);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Mahal',4);
INSERT INTO `question_option` (`question_option`, `question_id`) VALUES ('Murah',4);

-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Other');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Ya');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Tidak');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Makanan');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Minuman');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Makanan Ringan');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Mahal');
-- INSERT INTO `nexsoftdb`.`question_option` (`name`) VALUES ('Murah');

-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('1', '1');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('2', '2');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('2', '3');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('3', '4');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('3', '5');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('3', '6');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('4', '7');
-- INSERT INTO `nexsoftdb`.`mapping_option` (`question_id`, `question_option_id`) VALUES ('4', '8');

INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('1', '1');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('1', '2');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('1', '3');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('1', '4');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('2', '1');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('2', '3');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('2', '4');
INSERT INTO`survey` (`costumer_id`, `question_id`) VALUES ('3', '1');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('3', '2');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('4', '3');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('4', '2');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('4', '1');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('5', '1');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('5', '2');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('6', '3');
INSERT INTO `survey` (`costumer_id`, `question_id`) VALUES ('6', '4');

