package id.co.nexsoft.nexmile.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;

import id.co.nexsoft.nexmile.R;


public class SessionManager {
    private SharedPreferences prefs;
    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ROLES = "user_roles";
    public static final String USER_USERNAME = "user_username";
    public static final String USER_EMAIL = "user_email";

    public SessionManager(@NotNull Context context){
        this.prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public void saveAccessToken(@NotNull String token) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACCESS_TOKEN, token)
                .apply();
    }

    public String fetchAccessToken() {
        return  prefs.getString(ACCESS_TOKEN, null);
    }

    public void saveAccessUsername(@NotNull String username) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_USERNAME, username)
                .apply();
    }

    public String fetchAccessUsername() {
        return  prefs.getString(USER_USERNAME, null);
    }

    public void saveAccessEmail(@NotNull String email) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_EMAIL, email)
                .apply();
    }

    public String fetchAccessEmail() {
        return  prefs.getString(USER_EMAIL, null);
    }

    public void saveAccessRoles(@NotNull String roles) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_ROLES, roles)
                .apply();
    }

    public String fetchAccessRoles() {
        return  prefs.getString(USER_ROLES, USER_ROLES);
    }

    public void deleteAccessToken() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear().apply();
    }



}
