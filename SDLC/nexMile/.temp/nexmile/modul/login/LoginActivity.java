package id.co.nexsoft.nexmile.modul.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.modul.home.MainActivity;
import id.co.nexsoft.nexmile.modul.login.presenter.LoginPresenterContract;
import id.co.nexsoft.nexmile.modul.login.presenter.LoginPresenterImpl;
import id.co.nexsoft.nexmile.modul.login.view.LoginViewContract;


public class LoginActivity extends AppCompatActivity implements LoginViewContract {

    private LoginPresenterContract loginPresenterContract;

    Button btnLogin;
    LinearLayout llProgressBar;
    EditText edtUsername;
    EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenterContract = new LoginPresenterImpl(this, this);

        btnLogin = findViewById(R.id.btnLogin);
        llProgressBar = findViewById(R.id.llProgressBar);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);

        btnLogin.setOnClickListener(view -> {
            llProgressBar.setVisibility(View.VISIBLE);

            String username = edtUsername.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();

            if (username.isEmpty() || password.isEmpty()) {
                llProgressBar.setVisibility(View.GONE);
                Toast.makeText(this, "Username dan password wajib diisi!", Toast.LENGTH_LONG).show();
            } else {
                login(username, password, this);
            }
        });
    }

    public void toMain() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    @Override
    public void login(String username, String password, Context context) {
        llProgressBar.setVisibility(View.VISIBLE);
        loginPresenterContract.login(username,password,context);
    }

    @Override
    public void onSucceed() {
        llProgressBar.setVisibility(View.GONE);
        toMain();
    }

    @Override
    public void onNotMatch() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Username dan password tidak sesuai", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Gagal kontak server", Toast.LENGTH_LONG).show();
    }
}