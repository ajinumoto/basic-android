package id.co.nexsoft.nexmile.modul.login.view;

import android.content.Context;

public interface LoginViewContract {

    void login(String username, String password, Context context);

    void onSucceed();

    void onNotMatch();

    void onFailed();


}
