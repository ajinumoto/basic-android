package id.co.nexsoft.nexmile.modul.login.presenter;

import android.content.Context;

import androidx.annotation.NonNull;

import id.co.nexsoft.nexmile.core.model.User;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.core.repo.LoginRequest;
import id.co.nexsoft.nexmile.modul.login.view.LoginViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenterImpl implements LoginPresenterContract{

    private LoginViewContract loginViewContract;

    private APIClient apiClient;

    private SessionManager sessionManager;

    public LoginPresenterImpl(LoginViewContract loginViewContract,Context context){
        apiClient = new APIClient();
        sessionManager = new SessionManager(context);
        this.loginViewContract = loginViewContract;
    };

    @Override
    public void login(String username, String password, Context context) {

        apiClient.getApiService(context).login(new LoginRequest(username, password))
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {

                        User user = response.body();

                        if (user != null) {
                            if (!user.getAccessToken().isEmpty()) {
                                sessionManager.saveAccessToken(user.getAccessToken());
                                sessionManager.saveAccessRoles(user.getRoles().get(0));
                                sessionManager.saveAccessUsername(user.getUsername());
                                sessionManager.saveAccessEmail(user.getEmail());
                                loginViewContract.onSucceed();
                            } else {
                                loginViewContract.onNotMatch();
                            }
                        } else {
                            loginViewContract.onNotMatch();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                        loginViewContract.onFailed();
                    }
                });

    }
}
