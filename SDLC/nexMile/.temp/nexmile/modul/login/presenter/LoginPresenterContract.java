package id.co.nexsoft.nexmile.modul.login.presenter;

import android.content.Context;

public interface LoginPresenterContract {

    void login(String username, String password, Context context);

}
