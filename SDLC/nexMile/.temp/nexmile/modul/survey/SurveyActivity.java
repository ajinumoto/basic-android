package id.co.nexsoft.nexmile.modul.survey;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.login.LoginActivity;
import id.co.nexsoft.nexmile.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    TextView tvToken;
    TextView tvRole;
    Button btnLogout;
    private APIClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        tvToken = findViewById(R.id.tvToken);
        tvRole = findViewById(R.id.tvRole);
        btnLogout = findViewById(R.id.btnLogout);
        apiClient = new APIClient();

        sessionManager = new SessionManager(this);
        String token = sessionManager.fetchAccessToken();
        String role = sessionManager.fetchAccessRoles();

        if (token != null) {
            tvToken.setText(token);
            tvRole.setText("Coba yaaa");
        } else {
            toLogin();
        }

        btnLogout.setOnClickListener(view -> toLogin());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("lifecycle","onResume invoked");

        apiClient.getApiService(getApplicationContext()).user()
                .enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        Toast.makeText(getApplicationContext(), "Data : " + response, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Failed to get data", Toast.LENGTH_LONG).show();
                    }
                });


    }

    public void toLogin() {
        sessionManager.deleteAccessToken();

        Intent loginIntent = new Intent(this, LoginActivity.class);

        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }
}