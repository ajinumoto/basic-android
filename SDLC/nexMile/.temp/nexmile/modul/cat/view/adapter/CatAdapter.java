package id.co.nexsoft.nexmile.modul.cat.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.CatImg;


public class CatAdapter extends RecyclerView.Adapter<CatAdapter.CatViewHolder>{

    private Context context;
    private List<CatImg> cats;

    public CatAdapter(Context context, List<CatImg> cats) {
        this.context = context;
        this.cats = cats;
    }

    @NonNull
    @Override
    public CatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.itempic,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CatViewHolder holder, int position) {
        CatImg cat=cats.get(position);
        Picasso.get().load(cat.getUrl()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return cats.size();
    }

    public void getAllData(List<CatImg> cats)
    {
        this.cats=cats;
    }

    public static class CatViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView image;

        public CatViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.itempic);
        }
    }
}