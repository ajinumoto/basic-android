package id.co.nexsoft.nexmile.modul.cat.view;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.CatImg;
import id.co.nexsoft.nexmile.core.repo.Repository;

public class CatViewModel extends AndroidViewModel {
    private Repository repository;
    public LiveData<List<CatImg>> getAllCats;

    public CatViewModel(@NonNull Application application) {
        super(application);
        repository=new Repository(application);
        getAllCats=repository.getAllCats();
    }

    public void insert(List<CatImg> cats){
        repository.insert(cats);
    }

    public LiveData<List<CatImg>> getAllCats()
    {
        return getAllCats;
    }
}
