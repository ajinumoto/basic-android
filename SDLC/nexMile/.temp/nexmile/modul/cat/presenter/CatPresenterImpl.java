package id.co.nexsoft.nexmile.modul.cat.presenter;

import android.util.Log;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.CatImg;
import id.co.nexsoft.nexmile.core.repo.CATapi;
import id.co.nexsoft.nexmile.core.repo.Repository;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CatPresenterImpl implements CatPresenterContract{

    private Repository repository;


    @Override
    public void makeRequest() {

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("https://api.thecatapi.com/v1/images/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CATapi api=retrofit.create(CATapi.class);
        Call<List<CatImg>> call=api.getImgs(10);
        call.enqueue(new retrofit2.Callback<List<CatImg>>() {
            @Override
            public void onResponse(Call<List<CatImg>> call, Response<List<CatImg>> response) {
                if(response.isSuccessful()) {
                    repository.insert(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CatImg>> call, Throwable t) {
                Log.d("main", "onFailure: "+t.getMessage());
            }
        });

    }

}
