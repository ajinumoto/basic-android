package id.co.nexsoft.nexmile.modul.cat.presenter;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.CatImg;

public interface CatPresenterContract {
    void makeRequest();
}
