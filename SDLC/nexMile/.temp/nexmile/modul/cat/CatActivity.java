package id.co.nexsoft.nexmile.modul.cat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.CatImg;
import id.co.nexsoft.nexmile.core.repo.CATapi;
import id.co.nexsoft.nexmile.core.repo.Repository;
import id.co.nexsoft.nexmile.modul.cat.view.CatViewModel;
import id.co.nexsoft.nexmile.modul.cat.view.adapter.CatAdapter;
import id.co.nexsoft.nexmile.modul.home.MainActivity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CatActivity extends AppCompatActivity {
    private CatViewModel catViewModel;
    private List<CatImg> catList;
    private CatAdapter catAdapter;
    private RecyclerView recyclerView;
    private Repository repository;

    FloatingActionButton fabCat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat);

        repository=new Repository(getApplication());
        catList=new ArrayList<>();
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);
        fabCat = findViewById(R.id.fabCat);

        catViewModel=new ViewModelProvider(this).get(CatViewModel.class);

        catAdapter=new CatAdapter(this, catList);
        makeRequest();
        catViewModel.getAllCats().observe(this, new Observer<List<CatImg>>() {
            @Override
            public void onChanged(List<CatImg> cats) {
                recyclerView.setAdapter(catAdapter);
                catAdapter.getAllData(cats);
                Log.d("cat", "onChanged: "+cats);
            }
        });

        fabCat.setOnClickListener(view -> toMain());
    }

    public void toMain() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    private void makeRequest() {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("https://api.thecatapi.com/v1/images/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CATapi api=retrofit.create(CATapi.class);
        Call<List<CatImg>> call=api.getImgs(10);
        call.enqueue(new retrofit2.Callback<List<CatImg>>() {
            @Override
            public void onResponse(Call<List<CatImg>> call, Response<List<CatImg>> response) {
                if(response.isSuccessful()) {
                    repository.insert(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CatImg>> call, Throwable t) {
                Log.d("cat", "onFailure: "+t.getMessage());
            }
        });
    }
}