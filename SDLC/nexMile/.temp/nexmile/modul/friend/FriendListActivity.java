// https://axella-gerald.medium.com/android-mvp-architecture-without-di-490890578973

package id.co.nexsoft.nexmile.modul.tagging;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import android.view.WindowManager;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendListPresenterContract;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendListPresenterImpl;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendListViewContract;
import id.co.nexsoft.nexmile.modul.tagging.view.RecyclerViewFriendAdapter;
import id.co.nexsoft.nexmile.utils.AppExecutors;

public class FriendListActivity extends AppCompatActivity implements FriendListViewContract {

    private FriendListPresenterContract friendListPresenterContract;
    FloatingActionButton fabAddFriend;

    private RecyclerView recyclerViewFriend;
    private RecyclerViewFriendAdapter recyclerViewFriendAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        friendListPresenterContract = new FriendListPresenterImpl(this,this);
        initViews();
        friendListPresenterContract.getAllFriends();
        fabAddFriend.setOnClickListener(view -> addFriend());

    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
        AppExecutors.getInstance().diskIO().execute(() -> runOnUiThread(() -> friendListPresenterContract.getAllFriends()));
    }

    private void initViews(){
        recyclerViewFriend = findViewById(R.id.recyclerViewFriend);
        fabAddFriend = findViewById(R.id.fabAddFriend);
    }

    public void addFriend(){
        Intent intent = new Intent(this, FriendEditActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void getAllFriends(List<Friend> friendList) {
        recyclerViewFriendAdapter = new RecyclerViewFriendAdapter(this, friendList);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewFriend.setAdapter(recyclerViewFriendAdapter);
        recyclerViewFriendAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}