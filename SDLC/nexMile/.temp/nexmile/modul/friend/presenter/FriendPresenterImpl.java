//Presenter, Menghubungkan semua logic
package id.co.nexsoft.nexmile.modul.tagging.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendViewContract;

public class FriendPresenterImpl implements FriendPresenterContract {

    private FriendViewContract friendView;

    private NexDataBase nexDataBase;

    public FriendPresenterImpl(FriendViewContract friendView, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.friendView = friendView;
    }

    @Override
    public void saveFriend(Friend friend) {
        nexDataBase.friendDAO().insertFriend(friend);//retrofit
        friendView.saveFriend(friend);
    }

    @Override
    public void getAllFriends() {
        friendView.getAllFriends(nexDataBase.friendDAO().getAllFriends());
    }

}