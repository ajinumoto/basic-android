//Contractor

package id.co.nexsoft.nexmile.modul.tagging.presenter;

import id.co.nexsoft.nexmile.core.model.Friend;

public interface FriendPresenterContract {
    //Presenter
        void saveFriend(Friend friend);
        void getAllFriends();
}
