//Presenter, Menghubungkan semua logic
package id.co.nexsoft.nexmile.modul.tagging.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendListViewContract;

public class FriendListPresenterImpl implements FriendListPresenterContract {

    private FriendListViewContract friendListViewContract;

    private NexDataBase nexDataBase;

    public FriendListPresenterImpl(FriendListViewContract friendListViewContract, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.friendListViewContract = friendListViewContract;
    }

    @Override
    public void getAllFriends() {
        friendListViewContract.getAllFriends(nexDataBase.friendDAO().getAllFriends());
    }

}