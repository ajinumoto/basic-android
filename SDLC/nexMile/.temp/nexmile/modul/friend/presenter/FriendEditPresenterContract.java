//Contractor

package id.co.nexsoft.nexmile.modul.tagging.presenter;

import id.co.nexsoft.nexmile.core.model.Friend;

public interface FriendEditPresenterContract {
    //Presenter
        void saveFriend(String name, String phone);
        void updateFriend(int id, String name, String phone);
        Friend loadFriendById(int friendId);
}
