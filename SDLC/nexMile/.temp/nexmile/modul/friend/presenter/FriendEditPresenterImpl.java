//Presenter, Menghubungkan semua logic
package id.co.nexsoft.nexmile.modul.tagging.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendEditViewContract;

public class FriendEditPresenterImpl implements FriendEditPresenterContract {

    private FriendEditViewContract friendView;

    private NexDataBase nexDataBase;

    public FriendEditPresenterImpl(FriendEditViewContract friendView, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.friendView = friendView;
    }

    @Override
    public void saveFriend(String name, String phone) {
        Friend friend = new Friend(name,phone);
        nexDataBase.friendDAO().insertFriend(friend);//retrofit
    }

    @Override
    public void updateFriend(int id, String name, String phone) {
        Friend friend = new Friend(name,phone);
        friend.setId(id);
        nexDataBase.friendDAO().updateFriend(friend);//retrofit
    }

    @Override
    public Friend loadFriendById(int friendId) {
        return nexDataBase.friendDAO().loadFriendById(friendId);
    }

}