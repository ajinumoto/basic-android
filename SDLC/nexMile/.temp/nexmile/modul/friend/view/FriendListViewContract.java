//Contractor

package id.co.nexsoft.nexmile.modul.tagging.view;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Friend;

public interface FriendListViewContract {
        void getAllFriends(List<Friend> friendList);
}
