package id.co.nexsoft.nexmile.modul.tagging.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.FriendEditActivity;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendRecyclerPresenterImpl;
import id.co.nexsoft.nexmile.utils.Constants;


public class RecyclerViewFriendAdapter extends RecyclerView.Adapter<RecyclerViewFriendAdapter.ViewHolder> {

    private Context context;
    private List<Friend> friendList;

//    private final FriendRecyclerPresenterImpl presenter;

    public RecyclerViewFriendAdapter(Context context, List<Friend> friendList) {
        this.context = context;
        this.friendList = friendList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_friend_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Friend friend = friendList.get(position);
        holder.txtName.setText(friend.getName());
        holder.txtPhone.setText(friend.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtPhone;
        private ImageView editImage;
        public ViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtPhone = itemView.findViewById(R.id.txtPhone);
            editImage = itemView.findViewById(R.id.edit_image);
            editImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int elementId = friendList.get(getAdapterPosition()).getId();
                    Intent i = new Intent(context, FriendEditActivity.class);
                    i.putExtra(Constants.UPDATE_Person_Id, elementId);
                    context.startActivity(i);
                }
            });
        }
    }
}
