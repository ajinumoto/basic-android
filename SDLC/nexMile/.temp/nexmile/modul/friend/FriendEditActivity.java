// https://axella-gerald.medium.com/android-mvp-architecture-without-di-490890578973

package id.co.nexsoft.nexmile.modul.tagging;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendEditPresenterContract;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendEditPresenterImpl;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendEditViewContract;
import id.co.nexsoft.nexmile.utils.AppExecutors;
import id.co.nexsoft.nexmile.utils.Constants;


public class FriendEditActivity extends AppCompatActivity implements FriendEditViewContract {

    private FriendEditPresenterContract presenter;

    private EditText txtInputName, txtInputPhone;
    private Button btnSaveFriend;

    int personId;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_friend);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        presenter = new FriendEditPresenterImpl(this, this);
        initViews();
        intent = getIntent();
        if (intent != null && intent.hasExtra(Constants.UPDATE_Person_Id)){
            btnSaveFriend.setText("Update");
            personId = intent.getIntExtra(Constants.UPDATE_Person_Id, -1);

            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    populateUI(presenter.loadFriendById(personId));
                }
            });
        }

    }

    private void populateUI(Friend friend) {

        if (friend == null) {
            return;
        }

        txtInputName.setText(friend.getName());
        txtInputPhone.setText(friend.getPhoneNumber());
    }

    private void initViews() {
        txtInputName = findViewById(R.id.txtInputName);
        txtInputPhone = findViewById(R.id.txtInputPhone);
        btnSaveFriend = findViewById(R.id.btnSaveFriend);
        btnSaveFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveButtonClicked();
            }
        });
    }

    private void onSaveButtonClicked(){

        final String name = txtInputName.getText().toString();
        final String phone = txtInputPhone.getText().toString();

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if (!intent.hasExtra(Constants.UPDATE_Person_Id)){
                    presenter.saveFriend(name, phone);
                } else {
                    presenter.updateFriend(personId, name, phone);
                }
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}