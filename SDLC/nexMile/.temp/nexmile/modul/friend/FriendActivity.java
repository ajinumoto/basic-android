// https://axella-gerald.medium.com/android-mvp-architecture-without-di-490890578973

package id.co.nexsoft.nexmile.modul.tagging;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Friend;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendPresenterContract;
import id.co.nexsoft.nexmile.modul.tagging.presenter.FriendPresenterImpl;
import id.co.nexsoft.nexmile.modul.tagging.view.FriendViewContract;
import id.co.nexsoft.nexmile.modul.tagging.view.RecyclerViewFriendAdapter;
import id.co.nexsoft.nexmile.modul.home.MainActivity;
import id.co.nexsoft.nexmile.utils.AppExecutors;

public class FriendActivity extends AppCompatActivity implements FriendViewContract, Button.OnClickListener {

    private FriendPresenterContract friendPresenterContract;

    private RecyclerView recyclerViewFriend;
    private EditText txtInputName, txtInputPhone;
    private Button btnSaveFriend;
    private RecyclerViewFriendAdapter recyclerViewFriendAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        friendPresenterContract = new FriendPresenterImpl(this,this);
        initViews();
        friendPresenterContract.getAllFriends();
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
        AppExecutors.getInstance().diskIO().execute(() -> runOnUiThread(() -> friendPresenterContract.getAllFriends()));
    }

    private void initViews(){
        txtInputName = findViewById(R.id.txtInputName);
        txtInputPhone = findViewById(R.id.txtInputPhone);
        btnSaveFriend = findViewById(R.id.btnSaveFriend);
        btnSaveFriend.setOnClickListener(this);
        recyclerViewFriend = findViewById(R.id.recyclerViewFriend);
    }

    @Override
    public void getAllFriends(List<Friend> friendList) {
        recyclerViewFriendAdapter = new RecyclerViewFriendAdapter(this, friendList);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewFriend.setAdapter(recyclerViewFriendAdapter);
        recyclerViewFriendAdapter.notifyDataSetChanged();
    }

    @Override
    public void saveFriend(Friend friend) {
        friendPresenterContract.getAllFriends();
        clearInput();
    }

    public void clearInput() {
        txtInputName.setText("");
        txtInputPhone.setText("");
        txtInputName.requestFocus();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSaveFriend:
                friendPresenterContract.saveFriend(new Friend(txtInputName.getText().toString(), txtInputPhone.getText().toString()));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}