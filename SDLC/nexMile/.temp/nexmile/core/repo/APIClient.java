package id.co.nexsoft.nexmile.core.repo;

import android.content.Context;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private APIService apiService;
    private static Retrofit retrofit;
    private static final String BASE_URL = "https://d9b0-118-136-209-207.ngrok.io";

    public final APIService getApiService(Context context) {
        if (apiService == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient(context))
                    .build();

            apiService = retrofit.create(APIService.class);
        }
        return apiService;
    }

    private OkHttpClient okHttpClient(Context context) {
        return new OkHttpClient.Builder()
                .addInterceptor(new RequestInterceptor(context))
                .build();
    }
}