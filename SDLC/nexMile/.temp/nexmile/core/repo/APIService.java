package id.co.nexsoft.nexmile.core.repo;

import id.co.nexsoft.nexmile.core.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {

    @POST("/api/auth/signin")
    Call<User> login(@Body LoginRequest request);

    @GET("/api/test/user")
    Call<Object> user();
}