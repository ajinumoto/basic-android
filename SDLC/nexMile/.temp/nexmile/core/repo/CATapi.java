package id.co.nexsoft.nexmile.core.repo;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.CatImg;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CATapi {
    @GET("search")
    Call<List<CatImg>> getImgs(@Query("limit") int limit);
}
