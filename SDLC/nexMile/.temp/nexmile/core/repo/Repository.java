package id.co.nexsoft.nexmile.core.repo;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.co.nexsoft.nexmile.core.database.CatDatabase;
import id.co.nexsoft.nexmile.core.database.dao.catimgDao;
import id.co.nexsoft.nexmile.core.model.CatImg;

public class Repository {
    public catimgDao catimgDao;
    public LiveData<List<CatImg>> getAllCats;
    private CatDatabase database;

    public Repository(Application application){
        database=CatDatabase.getInstance(application);
        catimgDao=database.catimgDao();
        getAllCats=catimgDao.getcats();

    }

    public void insert(List<CatImg> cats){
        new InsertAsyncTask(catimgDao).execute(cats);

    }

    public LiveData<List<CatImg>> getAllCats(){
        return getAllCats;
    }
    private static class InsertAsyncTask extends AsyncTask<List<CatImg>,Void,Void> {
        private catimgDao catimgDao;

        public InsertAsyncTask(catimgDao catDao)
        {
            this.catimgDao=catDao;
        }
        @Override
        protected Void doInBackground(List<CatImg>... lists) {
            catimgDao.insert(lists[0]);
            return null;
        }
    }

}