package id.co.nexsoft.nexmile.core.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class User {
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("roles")
    private List<String> roles;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public List<String> getRoles() {
        return roles;
    }
}
