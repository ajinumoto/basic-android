package id.co.nexsoft.nexmile.core.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.co.nexsoft.nexmile.core.database.dao.FriendDAO;
import id.co.nexsoft.nexmile.core.model.Friend;


@Database(entities = {Friend.class}
        , version = 1
        , exportSchema = false)
public abstract class NexDataBase extends RoomDatabase {
    private static NexDataBase nexDataBase;

    private static final String DATABASE_NAME = "nexDB";

    public static synchronized NexDataBase getInstance(Context context) {
        if (nexDataBase == null) {
            nexDataBase = Room.databaseBuilder(context.getApplicationContext()
                    , NexDataBase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return nexDataBase;
    }

    public abstract FriendDAO friendDAO();
}