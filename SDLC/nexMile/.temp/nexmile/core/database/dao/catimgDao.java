package id.co.nexsoft.nexmile.core.database.dao;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.CatImg;

@Dao
public interface catimgDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<CatImg> cats);

    @Query("SELECT DISTINCT * FROM CatImg")
    LiveData<List<CatImg>>  getcats();

    @Query("DELETE FROM CatImg")
    void deleteAll();
}
