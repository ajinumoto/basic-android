package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Friend;


@Dao
public interface FriendDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFriend(Friend friend);

    @Delete()
    void deleteFriend(Friend friend);

    @Query("Select count(name) From Friend")
    int getTotalFriend();

    @Query("Select * From Friend")
    List<Friend> getAllFriends();

    @Query("SELECT * FROM Friend WHERE id = :id")
    Friend loadFriendById(int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateFriend(Friend friend);

}
