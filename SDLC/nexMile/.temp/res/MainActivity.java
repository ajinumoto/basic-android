
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    TextView tvToken;
    TextView tvRole;
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvToken = findViewById(R.id.tvToken);
        tvRole = findViewById(R.id.tvRole);
        btnLogout = findViewById(R.id.btnLogout);

        sessionManager = new SessionManager(this);
        String token = sessionManager.fetchAccessToken();
        String role = sessionManager.fetchAccessRoles();

        if (token != null) {
            tvToken.setText(token);
            tvRole.setText(role);
            if (role.equals("ROLE_USER")){toSurvey();}
        } else {
            toLogin();
        }

        btnLogout.setOnClickListener(view -> toLogin());
    }

    public void toLogin() {
        sessionManager.deleteAccessToken();

        Intent loginIntent = new Intent(this, LoginActivity.class);

        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void toSurvey() {
        Intent intent = new Intent(this, SurveyActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}