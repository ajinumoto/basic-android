package id.co.nexsoft.nexmile.modul.tagging;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.tagging.presenter.TaggingListPresenterContract;
import id.co.nexsoft.nexmile.modul.tagging.presenter.TaggingListPresenterImpl;
import id.co.nexsoft.nexmile.modul.tagging.view.TaggingListViewContract;
import id.co.nexsoft.nexmile.utils.Constants;

public class TaggingListActivity extends AppCompatActivity implements TaggingListViewContract {

    private TaggingListPresenterContract taggingListPresenterContract;
    FloatingActionButton fabAdd;

    private RecyclerView recyclerViewFriend;
    private TaggingAdapter taggingAdapter;
    private int routePlanId;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        taggingListPresenterContract = new TaggingListPresenterImpl(this, this);
        initViews();

        if (intent != null && intent.hasExtra(Constants.UPDATE_ROUTE_ID)) {
            routePlanId = intent.getIntExtra(Constants.UPDATE_ROUTE_ID, 0);
            taggingListPresenterContract.getAllTags(routePlanId);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
        taggingListPresenterContract.getAllTags(routePlanId);
    }

    private void initViews() {
        recyclerViewFriend = findViewById(R.id.recyclerViewTag);
        fabAdd = findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(view -> addTag());
        intent = getIntent();

    }

    public void addTag() {
        Intent intentTag = new Intent(this, TaggingEditActivity.class);
        intentTag.putExtra(Constants.UPDATE_ROUTE_ID, routePlanId);
        this.startActivity(intentTag);
    }

    @Override
    public void getAllTags(List<Tagging> taggingList) {
        taggingAdapter = new TaggingAdapter(this, taggingList);
        recyclerViewFriend.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewFriend.setAdapter(taggingAdapter);
        taggingAdapter.notifyDataSetChanged();
    }

    @Override
    public void getRoutePlan(List<RoutePlan> routePlanList) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}