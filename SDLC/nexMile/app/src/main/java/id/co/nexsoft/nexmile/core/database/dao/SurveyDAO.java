package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Survey;


@Dao
public interface SurveyDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Survey survey);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Survey survey);

    @Delete()
    void deleteAll(List<Survey> surveys);

    // get all data
    @Query("Select * From survey")
    List<Survey> getAll();

    // get data by id
    @Query("SELECT * FROM survey WHERE id = :id")
    Survey loadById(int id);

}
