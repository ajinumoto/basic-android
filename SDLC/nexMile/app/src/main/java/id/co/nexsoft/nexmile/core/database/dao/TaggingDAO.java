package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Tagging;


@Dao
public interface TaggingDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTag(Tagging tagging);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateTag(Tagging tagging);

    @Delete()
    void deleteAll(List<Tagging> answers);

    // remove tag by id
    @Query("DELETE FROM Tagging WHERE id = :id")
    void deleteTagById(int id);

    // get all uploaded tag
    @Query("Select * From Tagging Where status=201")
    List<Tagging> getAllUploaded();

    // get all tag
    @Query("Select * From Tagging")
    List<Tagging> getAllTag();

    // load tag data by id
    @Query("SELECT * FROM Tagging WHERE id = :id")
    Tagging loadTagById(int id);

    // Get list of tag with the name of costumer
    //@Query("SELECT t.*, c.name as costumerName FROM tagging as t left join route_plan as rp on t.route_plan_id = rp.id left join costumer as c on rp.costumer_id = c.id")
    @Query("SELECT t.id, t.api_id, t.image_url,t.serial_number,t.contract_number,t.asset_date,t.route_plan_id,t.status, rp.costumer_name as costumer_name FROM tagging as t left join route_plan as rp on t.route_plan_id = rp.api_id")
    List<Tagging> getAllTagAndCostumer();

    // get tag data from route id
    @Query("SELECT * FROM tagging WHERE route_plan_id = :routePlanId")
    List<Tagging> loadByRouteId(int routePlanId);

}
