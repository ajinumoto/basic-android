package id.co.nexsoft.nexmile.modul.routeplan;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.modul.routeplan.presenter.RouteAdapterPresenterContract;
import id.co.nexsoft.nexmile.modul.routeplan.presenter.RouteAdapterPresenterImpl;
import id.co.nexsoft.nexmile.modul.routeplan.view.RouteAdapterViewContract;
import id.co.nexsoft.nexmile.modul.survey.SurveyActivity;
import id.co.nexsoft.nexmile.modul.tagging.TaggingListActivity;
import id.co.nexsoft.nexmile.utils.Constants;
import id.co.nexsoft.nexmile.utils.SessionManager;

public class RouteAdapterView extends RecyclerView.Adapter<RouteAdapterView.ViewHolder> implements RouteAdapterViewContract {

    private Context context;
    private List<RoutePlan> routePlanList;
    private RouteAdapterPresenterContract presenterContract;
    private SessionManager sessionManager;

    public RouteAdapterView(Context context, List<RoutePlan> routePlanList) {
        this.context = context;
        this.routePlanList = routePlanList;
        sessionManager = new SessionManager(context);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        presenterContract = new RouteAdapterPresenterImpl(this, recyclerView.getContext());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_route, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RoutePlan routePlan = routePlanList.get(position);
        holder.tvCostumerName.setText(routePlan.getCostumerName());
        holder.tvCostumerAddress.setText(routePlan.getCostumerAddress());
        int routeId = routePlan.getApiId();

        if (!presenterContract.checkFilled(routeId)) {
            holder.ivTagFromRoute.setVisibility(View.GONE);
        }

        //Set green cloud if data has been uploaded
        if (presenterContract.checkCloud(routePlan.getApiId())) {
            holder.ivTagFromRoute.setImageResource(R.drawable.ic_baseline_cloud_done_green_24);
        }

        if (sessionManager.fetchAccessRoles().equals("Tagging")) {
            holder.cvRoute.setOnClickListener(view -> {
                Intent intent = new Intent(context, TaggingListActivity.class);
                //Send constant to tag intent
                intent.putExtra(Constants.UPDATE_ROUTE_ID, routePlan.getApiId());
                context.startActivity(intent);
            });
        } else {
            holder.cvRoute.setOnClickListener(view -> {
                if (!presenterContract.checkFilled(routeId)) {
                    Log.d("TAG", "onBindViewHolder: "+routePlan.toString());
                    Intent intent = new Intent(context, SurveyActivity.class);
                    //Send constant to survey intent
                    intent.putExtra(Constants.UPDATE_COSTUMER_ID, routePlan.getCostumerId());
                    intent.putExtra(Constants.COSTUMER_NAME, routePlan.getCostumerName());
                    intent.putExtra(Constants.COSTUMER_ADDRESS, routePlan.getCostumerAddress());
                    intent.putExtra(Constants.COSTUMER_IMAGE, routePlan.getCostumerImage());
                    intent.putExtra(Constants.UPDATE_ROUTE_ID, routePlan.getApiId());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Anda telah mengisi survey ini!", Toast.LENGTH_SHORT).show();
                }
            });
        }
        holder.ivTagFromRoute.setOnClickListener(view -> {
            presenterContract.saveRoutePlan(routeId, position);
        });
    }


    @Override
    public int getItemCount() {
        return routePlanList.size();
    }

    @Override
    public void changeImage(int position) {
        notifyItemChanged(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCostumerName, tvCostumerAddress;
        private ImageView ivTagFromRoute;
        private CardView cvRoute;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCostumerName = itemView.findViewById(R.id.tvCostumerName);
            tvCostumerAddress = itemView.findViewById(R.id.tvCostumerAddress);
            ivTagFromRoute = itemView.findViewById(R.id.ivTagFromRoute);
            cvRoute = itemView.findViewById(R.id.cvRoute);
        }
    }
}
