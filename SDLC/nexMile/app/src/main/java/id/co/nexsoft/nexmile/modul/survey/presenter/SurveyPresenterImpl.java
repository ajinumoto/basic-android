package id.co.nexsoft.nexmile.modul.survey.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.survey.view.SurveyViewContract;

public class SurveyPresenterImpl implements SurveyPresenterContract {

    private SurveyViewContract view;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;


    public SurveyPresenterImpl(SurveyViewContract view, Context context) {
        apiClient = new APIClient();
        this.nexDataBase = NexDataBase.getInstance(context);
        this.view = view;
        this.context = context;
    }

    @Override
    public void getAllQuestionByCostumer(int id) {
        view.getAllQuestion(nexDataBase.questionDao().loadByCostumerId(id));
    }

}