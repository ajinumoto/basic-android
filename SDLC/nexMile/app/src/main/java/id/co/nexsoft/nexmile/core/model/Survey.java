package id.co.nexsoft.nexmile.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "survey")
public class Survey {
    @SerializedName("retrofitId")
    @Expose(serialize = false)
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("id")
    @ColumnInfo(name = "api_id")
    private int apiId;

    @SerializedName("costumerId")
    @Expose(serialize = false)
    @Ignore
    private Costumer costumer;

    @SerializedName("retrofitCostumerId")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_id")
    private int costumerId;

    @SerializedName("questionId")
    @Ignore
    private Question question;

    @SerializedName("question")
    @Expose(serialize = false)
    @ColumnInfo(name = "question_id")
    private int questionId;

    @SerializedName("status")
    @Expose(serialize = false)
    @ColumnInfo(name = "status")
    private int status;

    public Survey() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public int getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(int costumerId) {
        this.costumerId = costumerId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }

}
