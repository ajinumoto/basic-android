package id.co.nexsoft.nexmile.core.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.co.nexsoft.nexmile.core.database.dao.AnswerDAO;
import id.co.nexsoft.nexmile.core.database.dao.CostumerDAO;
import id.co.nexsoft.nexmile.core.database.dao.OptionDAO;
import id.co.nexsoft.nexmile.core.database.dao.QuestionDAO;
import id.co.nexsoft.nexmile.core.database.dao.RoutePlanDAO;
import id.co.nexsoft.nexmile.core.database.dao.SurveyDAO;
import id.co.nexsoft.nexmile.core.database.dao.TaggingDAO;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Costumer;
import id.co.nexsoft.nexmile.core.model.Option;
import id.co.nexsoft.nexmile.core.model.Question;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;

@Database(entities = {Answer.class, Costumer.class, Option.class, Question.class, RoutePlan.class, Survey.class,Tagging.class}
        , version = 20
        , exportSchema = false)
public abstract class NexDataBase extends RoomDatabase {
    private static NexDataBase nexDataBase;

    private static final String DATABASE_NAME = "nexDB";

    public static synchronized NexDataBase getInstance(Context context) {
        if (nexDataBase == null) {
            nexDataBase = Room.databaseBuilder(context.getApplicationContext()
                    , NexDataBase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return nexDataBase;
    }
    public abstract AnswerDAO answerDao();
    public abstract CostumerDAO costumerDAO();
    public abstract OptionDAO optionDAO();
    public abstract QuestionDAO questionDao();
    public abstract RoutePlanDAO routePlanDao();
    public abstract SurveyDAO surveyDAO();
    public abstract TaggingDAO taggingDAO();
}