package id.co.nexsoft.nexmile.modul.download;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.modul.download.presenter.DownloadPresenterContract;
import id.co.nexsoft.nexmile.modul.download.presenter.DownloadPresenterImpl;
import id.co.nexsoft.nexmile.modul.download.view.DownloadViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;

public class DownloadActivity extends AppCompatActivity implements DownloadViewContract {

    private DownloadPresenterContract downloadPresenterContract;
    private SessionManager sessionManager;
    Button btnDownload;
    TextView tvRoleTitle, tvUpdateDate;
    private int userId;
    private Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        // Go back to home action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        initViews();
        updateDate();
        downloadPresenterContract.checkData(getDate(), userId);
    }

    private void initViews() {
        tvRoleTitle = findViewById(R.id.tvRoleTitle);
        tvUpdateDate = findViewById(R.id.updateDate);
        btnDownload = findViewById(R.id.btnDownload);
        sessionManager = new SessionManager(this);
        String role = sessionManager.fetchAccessRoles();
        userId = sessionManager.fetchAccessUserId();
        tvRoleTitle.setText(role);
        downloadPresenterContract = new DownloadPresenterImpl(this, this);
        btnDownload.setOnClickListener(view -> {
            downloadPresenterContract.downloadData(getDate(), userId);
        });

    }

    //Get today date
    private String getDate() {
        date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    @SuppressLint("SetTextI18n")
    public void updateDate() {
        try {
            Long updateDateInMillis = Long.parseLong(sessionManager.fetchAccessUpdateDate());
            tvUpdateDate.setText("Last update: " + DateUtils.getRelativeTimeSpanString(updateDateInMillis, Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS) + ".");
        } catch (Exception e) {
            tvUpdateDate.setText("Last update: Never.");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}