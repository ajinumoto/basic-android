package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Question;


@Dao
public interface QuestionDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Question question);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Question question);

    @Delete()
    void deleteAll(List<Question> questions);

    // get all data
    @Query("Select * From Question")
    List<Question> getAll();

    // get data by id
    @Query("SELECT * FROM Question WHERE id = :id")
    Question loadById(int id);

    // get list of data from costumer id
    @Query("SELECT q.* FROM survey as s left join question as q on s.question_id = q.api_id where costumer_id = :costumerId")
    List<Question> loadByCostumerId(int costumerId);

}
