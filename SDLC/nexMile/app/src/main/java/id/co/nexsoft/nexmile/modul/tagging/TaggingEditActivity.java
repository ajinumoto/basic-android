package id.co.nexsoft.nexmile.modul.tagging;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.tagging.presenter.TaggingEditPresenterContract;
import id.co.nexsoft.nexmile.modul.tagging.presenter.TaggingEditPresenterImpl;
import id.co.nexsoft.nexmile.modul.tagging.view.TaggingEditViewContract;
import id.co.nexsoft.nexmile.utils.Constants;


public class TaggingEditActivity extends AppCompatActivity implements TaggingEditViewContract {

    private TaggingEditPresenterContract presenter;

    private EditText txtSerialNumber, txtContract;
    private TextView txtAssetDate;
    private Button btnSaveTagging, btnCamera, btnDeleteTagging;
    private ImageView ivTagging;
    private String imageUrl;

    static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100;
    final Calendar calendar = Calendar.getInstance();
    private String currentPhotoPath;
    private int tagId, routePlanId;
    private long apiId;
    private Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_edit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        presenter = new TaggingEditPresenterImpl(this, this);
        initViews();
        intent = getIntent();

        routePlanId = intent.getIntExtra(Constants.UPDATE_ROUTE_ID, 0);

        if (intent.hasExtra(Constants.UPDATE_TAG_ID)) {
            btnSaveTagging.setText("Update");
            btnDeleteTagging.setVisibility(View.VISIBLE);
            tagId = intent.getIntExtra(Constants.UPDATE_TAG_ID, 0);
            populateUI(presenter.loadTagById(tagId));
        }

        btnDeleteTagging.setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(TaggingEditActivity.this);
            builder.setCancelable(true);
            builder.setTitle("Apakah anda yakin ingin menghapus pada database lokal?");
            builder.setMessage("File yang telah dihapus tidak dapat dikembalikan. Perhatikan juga bahwa opsi ini tidak dapat menghapus data yang telah terunggah pada server.");
            builder.setPositiveButton("Hapus", (dialog, which) -> {
                presenter.deleteTag(tagId);
                finish();
            });
            builder.setNegativeButton("Batal", (dialog, which) -> {
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void initViews() {
        txtSerialNumber = findViewById(R.id.txtSerialNumber);
        txtContract = findViewById(R.id.txtContract);
        ivTagging = findViewById(R.id.ivTagging);
        btnSaveTagging = findViewById(R.id.btnSaveTagging);
        btnSaveTagging.setOnClickListener(view -> onSaveButtonClicked());
        btnCamera = findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(view -> {
            // Check permission everytime want to upload image
            if (checkAndRequestPermissions(TaggingEditActivity.this)) {
                chooseImage(TaggingEditActivity.this);
            }
        });

        btnDeleteTagging = findViewById(R.id.btnDeleteTagging);

        txtAssetDate = findViewById(R.id.txtAssetDate);
        initCalender();
    }

    // Chose contract date using date picker
    private void initCalender() {
        DatePickerDialog.OnDateSetListener date = (view, year, month, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String Format = "yyyy-MM-dd";
            SimpleDateFormat sdf = new SimpleDateFormat(Format, Locale.US);
            txtAssetDate.setText(sdf.format(calendar.getTime()));
            txtAssetDate.setError(null);
        };
        txtAssetDate.setOnClickListener(v -> {
            new DatePickerDialog(this, date,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
                    .show();
        });
    }

    private void populateUI(Tagging tagging) {
        if (tagging == null) {
            return;
        }
        imageUrl = tagging.getImageUrl();
        ivTagging.setImageBitmap(BitmapFactory.decodeFile(imageUrl));
        txtSerialNumber.setText(tagging.getSerialNumber());
        txtContract.setText(tagging.getContractNumber());
        txtAssetDate.setText(tagging.getAssetDate());
        routePlanId = tagging.getRoutePlanId();
        if (tagging.getApiId() != null){
            apiId = tagging.getApiId();
        }
    }

    // function to check permission
    public static boolean checkAndRequestPermissions(final Activity context) {
        int WExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (WExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(TaggingEditActivity.this,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "FlagUp Requires Access to Camara.", Toast.LENGTH_SHORT)
                        .show();
            } else if (ContextCompat.checkSelfPermission(TaggingEditActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "FlagUp Requires Access to Your Storage.",
                        Toast.LENGTH_SHORT).show();
            } else {
                chooseImage(TaggingEditActivity.this);
            }
        }
    }

    // function to let's the user to choose image from camera or gallery
    private void chooseImage(Context context) {
        final CharSequence[] optionsMenu = {"Take Photo", "Choose from Gallery", "Exit"}; // create a menuOption Array
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setItems(optionsMenu, (dialogInterface, i) -> {
            if (optionsMenu[i].equals("Take Photo")) {
                // Open the camera and get the photo
                dispatchTakePictureIntent();
            } else if (optionsMenu[i].equals("Choose from Gallery")) {
                // choose from  external storage
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            } else if (optionsMenu[i].equals("Exit")) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TAG", "onActivityResult: " + currentPhotoPath);

        //update image view from camera/file based on result code
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 100:
                    if (resultCode == RESULT_OK && currentPhotoPath != null) {
                        imageUrl = currentPhotoPath;
                        File file = new File(currentPhotoPath);
                        presenter.saveBitmapToFile(file);
                        Bitmap myBitmap = BitmapFactory.decodeFile(currentPhotoPath);
                        ivTagging.setImageBitmap(myBitmap);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                imageUrl = picturePath;
                                ivTagging.setImageBitmap(BitmapFactory.decodeFile(imageUrl));
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
            Log.d("Image", "createImageFile: " + currentPhotoPath);
        } catch (IOException ignored) {
            Log.e("TAG", "dispatchTakePictureIntent: ", ignored);
        }
        if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(this,
                    "id.co.nexsoft.nexmile",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            Log.d("intent", "createImageFile: " + takePictureIntent);
            galleryAddPic();
            startActivityForResult(takePictureIntent, 100);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);//media store
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void onSaveButtonClicked() {

        final String serialNumber = txtSerialNumber.getText().toString();
        final String contractNumber = txtContract.getText().toString();
        final String assetDate = txtAssetDate.getText().toString();
        if (serialNumber.isEmpty() || contractNumber.isEmpty() || assetDate.isEmpty() || imageUrl == null) {
            Toast.makeText(this, "Data belum lengkap!", Toast.LENGTH_LONG).show();
        } else {
            if (serialNumber.length()<3 || serialNumber.length()>20 || contractNumber.length()<3 || contractNumber.length()>20){
                Toast.makeText(this, "Format serial/kontrak tidak sesuai!", Toast.LENGTH_LONG).show();
            } else {
                if (!intent.hasExtra(Constants.UPDATE_TAG_ID)) {
                    presenter.saveTag(imageUrl, serialNumber, contractNumber, assetDate, routePlanId, apiId);
                } else {
                    presenter.updateTag(tagId, imageUrl, serialNumber, contractNumber, assetDate, routePlanId, apiId);
                }
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}