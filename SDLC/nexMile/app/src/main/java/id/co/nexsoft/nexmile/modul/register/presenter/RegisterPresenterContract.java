package id.co.nexsoft.nexmile.modul.register.presenter;

import android.content.Context;

public interface RegisterPresenterContract {

    void register(String username, String email, String password, String role, Context context);

}
