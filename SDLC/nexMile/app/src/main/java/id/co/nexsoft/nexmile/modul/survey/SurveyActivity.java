package id.co.nexsoft.nexmile.modul.survey;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Question;
import id.co.nexsoft.nexmile.modul.survey.presenter.SurveyPresenterContract;
import id.co.nexsoft.nexmile.modul.survey.presenter.SurveyPresenterImpl;
import id.co.nexsoft.nexmile.modul.survey.view.SurveyViewContract;
import id.co.nexsoft.nexmile.utils.Constants;

public class SurveyActivity extends AppCompatActivity implements SurveyViewContract {

    private SurveyPresenterContract presenter;
    private RecyclerView rvQuestion;
    private SurveyAdapter surveyAdapter;
    private Button btnSaveSurvey;
    private TextView tvCostumerName,tvCostumerAddress;
    private ImageView ivCostumer;
    private int costumerId, routeId;
    private String costumerName, costumerAddress, costumerImage;
    private Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        presenter = new SurveyPresenterImpl(this, this);
        initViews();

        //get constant from previous intent
        costumerId = intent.getIntExtra(Constants.UPDATE_COSTUMER_ID, 0);
        routeId = intent.getIntExtra(Constants.UPDATE_ROUTE_ID, 0);

        costumerName =intent.getStringExtra(Constants.COSTUMER_NAME);
        costumerAddress =intent.getStringExtra(Constants.COSTUMER_ADDRESS);
        costumerImage =intent.getStringExtra(Constants.COSTUMER_IMAGE);

        tvCostumerName.setText(costumerName);
        tvCostumerAddress.setText(costumerAddress);
        try {
            Picasso.get().load(costumerImage).into(ivCostumer);
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
        presenter.getAllQuestionByCostumer(costumerId);
    }

    private void initViews() {
        rvQuestion = findViewById(R.id.rvQuestion);
        btnSaveSurvey = findViewById(R.id.btnSaveSurvey);
        tvCostumerName = findViewById(R.id.tvCostumerName);
        tvCostumerAddress = findViewById(R.id.tvCostumerAddress);
        ivCostumer = findViewById(R.id.ivCostumer);
        btnSaveSurvey.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Apakah anda yakin ingin menyimpan survey ini?");
            builder.setMessage("Survey yanng telah disimpan tidak dapat dirubah kembali.");
            builder.setPositiveButton("Simpan", (dialog, which) -> {
                saveSurvey(routeId);
                finish();
            });
            builder.setNegativeButton("Batal", (dialog, which) -> {
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        intent = getIntent();
    }

    @Override
    public void getAllQuestion(List<Question> questionList) {
        surveyAdapter = new SurveyAdapter(this, questionList);
        rvQuestion.setLayoutManager(new LinearLayoutManager(this));
        rvQuestion.setAdapter(surveyAdapter);
        surveyAdapter.notifyDataSetChanged();
    }

    private void saveSurvey(int surveyId) {
        surveyAdapter.saveTag(surveyId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}