package id.co.nexsoft.nexmile.modul.routeplan.presenter;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.FileUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.File;
import java.util.List;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.routeplan.view.RouteAdapterViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteAdapterPresenterImpl implements RouteAdapterPresenterContract {

    private RouteAdapterViewContract routeAdapterViewContract;
    private APIClient apiClient;
    private Context context;
    private NexDataBase nexDataBase;
    private SessionManager sessionManager;

    public RouteAdapterPresenterImpl(RouteAdapterViewContract routeAdapterViewContract, Context context) {
        apiClient = new APIClient();
        sessionManager = new SessionManager(context);
        this.nexDataBase = NexDataBase.getInstance(context);
        this.routeAdapterViewContract = routeAdapterViewContract;
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void saveRoutePlan(int routeId, int position) {
        if (sessionManager.fetchAccessRoles().equals("Tagging")) {
            List<Tagging> taggingList = nexDataBase.taggingDAO().loadByRouteId(routeId);
            apiClient.getApiService(context).saveTaggingList(taggingList)
                    .enqueue(new Callback<List<Tagging>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Tagging>> call, @NonNull Response<List<Tagging>> response) {
                            if (response.isSuccessful()){
                                nexDataBase.taggingDAO().deleteAll(taggingList);
                                response.body().forEach(tag -> {
                                    //save tagging list to API server and save API response code to local database status
                                    tag.setStatus(response.code());
                                    tag.setRoutePlanId(tag.getRoutePlanId());
                                    tag.setCostumerName(nexDataBase.routePlanDao().loadByApiId(tag.getRoutePlanId()).getCostumerName());
                                    nexDataBase.taggingDAO().insertTag(tag);
                                });
                                routeAdapterViewContract.changeImage(position);
                                taggingList.forEach(tag -> {
                                    //upload image to API
                                    uploadFile(tag.getImageUrl());
                                });
                            } else  {
                                Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<List<Tagging>> call, @NonNull Throwable t) {
                            Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            List<Answer> answerList = nexDataBase.answerDao().loadByRouteId(routeId);
            apiClient.getApiService(context).saveAnswerList(answerList)
                    .enqueue(new Callback<List<Answer>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Answer>> call, @NonNull Response<List<Answer>> response) {
                            if (response.isSuccessful()){
                                nexDataBase.answerDao().deleteAll(answerList);
                                response.body().forEach(answer -> {
                                    //save answer list to API server and save API response code to local database status
                                    answer.setStatus(response.code());
                                    answer.setRoutePlanId(answer.getRoutePlanId());
                                    answer.setCostumerName(nexDataBase.routePlanDao().loadByApiId(answer.getRoutePlanId()).getCostumerName());
                                    nexDataBase.answerDao().insert(answer);
                                });
                                routeAdapterViewContract.changeImage(position);
                            } else  {
                                Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(@NonNull Call<List<Answer>> call, @NonNull Throwable t) {
                            Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    //check whether the list of answer/tag has been uploaded
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean checkCloud(int routeId) {
        int uploadedCount;
        if (sessionManager.fetchAccessRoles().equals("Tagging")) {
            List<Tagging> taggingList = nexDataBase.taggingDAO().loadByRouteId(routeId);
            uploadedCount = (int) taggingList.stream().filter(tagging -> tagging.getStatus() == 201).count();
            if (uploadedCount == taggingList.size()) {
                return true;
            }
        } else {
            List<Answer> answerList = nexDataBase.answerDao().loadByRouteId(routeId);
            uploadedCount = (int) answerList.stream().filter(tagging -> tagging.getStatus() == 201).count();
            if (uploadedCount == answerList.size()) {
                return true;
            }
        }
        return false;
    }

    //check whether tag/survey has been filled
    @Override
    public boolean checkFilled(int routeId) {
        if (sessionManager.fetchAccessRoles().equals("Tagging")) {
            return (nexDataBase.taggingDAO().loadByRouteId(routeId).size() > 0);
        } else {
            return (nexDataBase.answerDao().loadByRouteId(routeId).size() > 0);
        }
    }

    //upload file to server API using multipart
    private void uploadFile(String filePath) {
        // create upload service client
        File file = new File(filePath);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/jpeg"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        // add another part within the multipart request
        String pathName = "tag";
        RequestBody path =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, pathName);

        // finally, execute the request
        Call<ResponseBody> call = apiClient.getApiService(context).upload(path, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success " +response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Upload foto gagal!", Toast.LENGTH_SHORT).show();
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}