package id.co.nexsoft.nexmile.modul.home.presenter;

import android.content.Context;
import android.util.Log;

import java.util.Date;

import id.co.nexsoft.nexmile.core.database.NexDataBase;

public class MainPresenterImpl implements MainPresenterContract {

    private NexDataBase nexDataBase;
    private Context context;

    public MainPresenterImpl(Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.context = context;
    }

    @Override
    public void clearDatabase() {
        nexDataBase.clearAllTables();
    }

    @Override
    public void checkDate(Long updateDateInMillis) {

        Date lastUpdateDate = new Date(updateDateInMillis);
        Date now = new Date();

        Log.d("TAG", "checkDate lasUpdaeDate: " + lastUpdateDate.getDate());
        Log.d("TAG", "checkDate now: " + now.getDate());

        //Clear database if date change
        if (lastUpdateDate.getDate()<now.getDate()){
            nexDataBase.clearAllTables();
        }

    }
}
