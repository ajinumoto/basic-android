package id.co.nexsoft.nexmile.modul.tagging;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.utils.Constants;


public class TaggingAdapter extends RecyclerView.Adapter<TaggingAdapter.ViewHolder> {

    private Context context;
    private List<Tagging> taggingList;

    public TaggingAdapter(Context context, List<Tagging> taggingList) {
        this.context = context;
        this.taggingList = taggingList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_tag, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tagging tagging = taggingList.get(position);
        holder.txtSerialNumber.setText(tagging.getSerialNumber());
        holder.txtContract.setText(tagging.getContractNumber());
    }

    @Override
    public int getItemCount() {
        return taggingList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSerialNumber, txtContract;
        private ImageView editImage;

        public ViewHolder(View itemView) {
            super(itemView);
            txtSerialNumber = itemView.findViewById(R.id.txtSerialNumber);
            txtContract = itemView.findViewById(R.id.txtContract);
            editImage = itemView.findViewById(R.id.edit_image);
            editImage.setOnClickListener(view -> {
                // Open edit activity with tag id constant
                int elementId = taggingList.get(getAdapterPosition()).getId();
                Intent i = new Intent(context, TaggingEditActivity.class);
                i.putExtra(Constants.UPDATE_TAG_ID, elementId);
                context.startActivity(i);
            });
        }
    }
}
