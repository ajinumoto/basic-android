package id.co.nexsoft.nexmile.modul.report.view;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;

public interface ReportListViewContract {
    void getAllTags(List<Tagging> taggingList);
    void getAllAnswer(List<Answer> answerList);
}
