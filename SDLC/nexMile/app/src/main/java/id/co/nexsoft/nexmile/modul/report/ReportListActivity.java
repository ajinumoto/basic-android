package id.co.nexsoft.nexmile.modul.report;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.report.presenter.ReportListPresenterContract;
import id.co.nexsoft.nexmile.modul.report.presenter.ReportListPresenterImpl;
import id.co.nexsoft.nexmile.modul.report.view.ReportListViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;


public class ReportListActivity extends AppCompatActivity implements ReportListViewContract {

    private ReportListPresenterContract reportListPresenterContract;
    FloatingActionButton fabUpload;

    private RecyclerView recyclerView;
    private TaggingAdapter taggingAdapter;
    private SurveyAdapter surveyAdapter;
    private Intent intent;
    private SessionManager sessionManager;
    private TextView tvNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list);
        sessionManager = new SessionManager(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        reportListPresenterContract = new ReportListPresenterImpl(this, this);
        initViews();
    }

    //update local database when activity resume
    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
                reportListPresenterContract.getAllData();
    }

    private void initViews() {
        tvNoData = findViewById(R.id.tvNoData);
        recyclerView = findViewById(R.id.recyclerViewReport);
        fabUpload = findViewById(R.id.fabUpload);
        fabUpload.setOnClickListener(view -> uploadTag());
        intent = getIntent();
    }

    public void uploadTag() {
        reportListPresenterContract.saveAllData();
    }

    @Override
    public void getAllTags(List<Tagging> taggingList) {
        if (taggingList.size()==0){
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.GONE);
        }
        taggingAdapter = new TaggingAdapter(this, taggingList);
        recyclerView.setAdapter(taggingAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        taggingAdapter.notifyDataSetChanged();
    }

    @Override
    public void getAllAnswer(List<Answer> answerList) {
        if (answerList.size()==0){
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.GONE);
        }
        surveyAdapter = new SurveyAdapter(this, answerList);
        recyclerView.setAdapter(surveyAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        surveyAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}