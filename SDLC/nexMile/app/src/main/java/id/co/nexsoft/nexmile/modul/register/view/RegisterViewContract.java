package id.co.nexsoft.nexmile.modul.register.view;

import android.content.Context;

public interface RegisterViewContract {

    void register(String username, String email, String password, String role, Context context);

    void onSucceed();

    void onUsernameInvalid();

    void onEmailInvalid();

    void onFailed();

}
