package id.co.nexsoft.nexmile.modul.tagging.presenter;

public interface TaggingListPresenterContract {
    void getAllTags(int routePlanId);
}
