package id.co.nexsoft.nexmile.modul.register.presenter;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import id.co.nexsoft.nexmile.core.model.User;
import id.co.nexsoft.nexmile.core.model.requests.LoginRequest;
import id.co.nexsoft.nexmile.core.model.requests.MessageResponse;
import id.co.nexsoft.nexmile.core.model.requests.SignUpRequest;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.register.view.RegisterViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenterImpl implements RegisterPresenterContract {

    private RegisterViewContract registerViewContract;

    private APIClient apiClient;

    public RegisterPresenterImpl(RegisterViewContract registerViewContract, Context context) {
        apiClient = new APIClient();
        this.registerViewContract = registerViewContract;
    }

    @Override
    public void register(String username, String email, String password, String role, Context context) {

        List<String> roles = new ArrayList<>();

        if (role.equals("Survey")) {
            roles.add("survey");
        } else {
            roles.add("tagging");
        }

        // save
        apiClient.getApiService(context).signup(new SignUpRequest(username, email, password, roles))
                .enqueue(new Callback<MessageResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MessageResponse> call, @NonNull Response<MessageResponse> response) {

                        if (response.isSuccessful()) {
                            registerViewContract.onSucceed();
                        } else {
                            Gson gson = new Gson();
                            assert response.errorBody() != null;
                            try {
                                MessageResponse message = gson.fromJson(response.errorBody().charStream(), MessageResponse.class);

                                // Show server response error
                                switch (message.getMessage()) {
                                    case "Error: Username is already taken!":
                                        registerViewContract.onUsernameInvalid();
                                        break;
                                    case "Error: Email is already in use!":
                                        registerViewContract.onEmailInvalid();
                                        break;
                                    default:
                                        registerViewContract.onFailed();
                                }
                            } catch (IllegalStateException | JsonSyntaxException exception) {
                                registerViewContract.onFailed();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<MessageResponse> call, @NonNull Throwable t) {
                        registerViewContract.onFailed();
                    }
                });
    }

}


