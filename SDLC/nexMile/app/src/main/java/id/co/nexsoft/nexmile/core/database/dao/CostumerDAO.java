package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Costumer;


@Dao
public interface CostumerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Costumer costumer);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Costumer costumer);

    @Delete()
    void deleteAll(List<Costumer> costumers);

    // get All data
    @Query("Select * From Costumer")
    List<Costumer> getAll();

    // get data by id
    @Query("SELECT * FROM Costumer WHERE id = :id")
    Costumer loadById(int id);
}
