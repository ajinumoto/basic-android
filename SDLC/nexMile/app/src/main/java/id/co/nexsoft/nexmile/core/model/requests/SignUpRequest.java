package id.co.nexsoft.nexmile.core.model.requests;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignUpRequest {

    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("role")
    private List<String> role;

    public SignUpRequest(String username, String email, String password, List<String> role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
