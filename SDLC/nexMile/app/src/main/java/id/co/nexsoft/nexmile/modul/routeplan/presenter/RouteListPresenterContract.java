package id.co.nexsoft.nexmile.modul.routeplan.presenter;

public interface RouteListPresenterContract {
    void getRoutePlan();
}
