package id.co.nexsoft.nexmile.modul.report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.report.presenter.TaggingAdapterPresenterContract;
import id.co.nexsoft.nexmile.modul.report.presenter.TaggingAdapterPresenterImpl;
import id.co.nexsoft.nexmile.modul.report.view.ReportAdapterViewContract;

public class TaggingAdapter extends RecyclerView.Adapter<TaggingAdapter.ViewHolder> implements ReportAdapterViewContract {

    private Context context;
    private List<Tagging> taggingList;
    private TaggingAdapterPresenterContract presenterContract;

    public TaggingAdapter(Context context, List<Tagging> taggingList) {
        this.context = context;
        this.taggingList = taggingList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        presenterContract = new TaggingAdapterPresenterImpl(this, recyclerView.getContext());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_report, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tagging tagging = taggingList.get(position);
        //show green cloud status if data has been uploaded
        if (presenterContract.checkCloud(tagging.getId())) {
            holder.ivReportStatus.setImageResource(R.drawable.ic_baseline_cloud_done_green_24);
        }
        holder.tvCostumer.setText(tagging.getCostumerName());
        holder.tvSerialNumber.setText(tagging.getSerialNumber());
    }

    @Override
    public int getItemCount() {
        return taggingList.size();
    }

    @Override
    public void changeImage(int position) {
        notifyItemChanged(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCostumer, tvSerialNumber;
        private ImageView ivReportStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCostumer = itemView.findViewById(R.id.tvCostumer);
            tvSerialNumber = itemView.findViewById(R.id.tvSerialNumber);
            ivReportStatus = itemView.findViewById(R.id.ivReportStatus);
            ivReportStatus.setOnClickListener(view -> {

            });
        }
    }
}
