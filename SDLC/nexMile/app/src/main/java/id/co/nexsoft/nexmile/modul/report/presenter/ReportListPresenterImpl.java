package id.co.nexsoft.nexmile.modul.report.presenter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.File;
import java.util.List;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.report.view.ReportListViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportListPresenterImpl implements ReportListPresenterContract {

    private ReportListViewContract reportListViewContract;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;
    private SessionManager sessionManager;

    public ReportListPresenterImpl(ReportListViewContract reportListViewContract, Context context) {
        apiClient = new APIClient();
        sessionManager = new SessionManager(context);
        this.nexDataBase = NexDataBase.getInstance(context);
        this.reportListViewContract = reportListViewContract;
        this.context = context;
    }

    @Override
    public void getAllData() {
        //Get data based on user role
        if(sessionManager.fetchAccessRoles().equals("Tagging")) {
            reportListViewContract.getAllTags(nexDataBase.taggingDAO().getAllTagAndCostumer());
        } else {
            reportListViewContract.getAllAnswer(nexDataBase.answerDao().getAll());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void saveAllData() {
        if(sessionManager.fetchAccessRoles().equals("Tagging")) {
            List<Tagging> taggingList = nexDataBase.taggingDAO().getAllTag();
            apiClient.getApiService(context).saveTaggingList(taggingList)
                    .enqueue(new Callback<List<Tagging>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Tagging>> call, @NonNull Response<List<Tagging>> response) {
                            if (response.isSuccessful()){
                                nexDataBase.taggingDAO().deleteAll(taggingList);
                                response.body().forEach(tag -> {
                                    //save data to server and replace status on local database with server response code
                                    tag.setStatus(response.code());
                                    tag.setRoutePlanId(tag.getRoutePlanId());
                                    tag.setCostumerName(nexDataBase.routePlanDao().loadByApiId(tag.getRoutePlanId()).getCostumerName());
                                    nexDataBase.taggingDAO().insertTag(tag);
                                });
                                reportListViewContract.getAllTags(nexDataBase.taggingDAO().getAllTagAndCostumer());
                                taggingList.forEach(tag -> {
                                    // save image to server
                                    uploadFile(tag.getImageUrl());
                                });
                            } else  {
                                Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Tagging>> call, @NonNull Throwable t) {
                            Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_LONG).show();
                        }
                    });
        } else {
            List<Answer> answerList = nexDataBase.answerDao().getAll();
            apiClient.getApiService(context).saveAnswerList(answerList)
                    .enqueue(new Callback<List<Answer>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Answer>> call, @NonNull Response<List<Answer>> response) {
                            if (response.isSuccessful()){
                                nexDataBase.answerDao().deleteAll(answerList);
                                response.body().forEach(answer -> {
                                    //save data to server and replace status on local database with server response code
                                    answer.setStatus(response.code());
                                    answer.setRoutePlanId(answer.getRoutePlanId());
                                    answer.setCostumerName(nexDataBase.routePlanDao().loadByApiId(answer.getRoutePlanId()).getCostumerName());
                                    nexDataBase.answerDao().insert(answer);
                                });
                                reportListViewContract.getAllAnswer(nexDataBase.answerDao().getAll());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Answer>> call, @NonNull Throwable t) {
                            Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void uploadFile(String filePath) {
        // create upload service client
        File file = new File(filePath);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/jpeg"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        // add another part within the multipart request
        String pathName = "tag";
        RequestBody path =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, pathName);

        // finally, execute the request
        Call<ResponseBody> call = apiClient.getApiService(context).upload(path, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success " +response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Upload foto gagal!", Toast.LENGTH_SHORT).show();
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}