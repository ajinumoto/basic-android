package id.co.nexsoft.nexmile.modul.download.presenter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.util.Calendar;
import java.util.List;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Costumer;
import id.co.nexsoft.nexmile.core.model.Option;
import id.co.nexsoft.nexmile.core.model.Question;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.download.view.DownloadViewContract;
import id.co.nexsoft.nexmile.utils.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadPresenterImpl implements DownloadPresenterContract {

    private DownloadViewContract view;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;
    private SessionManager sessionManager;

    public DownloadPresenterImpl(DownloadViewContract view, Context context) {
        apiClient = new APIClient();
        sessionManager = new SessionManager(context);
        this.nexDataBase = NexDataBase.getInstance(context);
        this.view = view;
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void downloadData(String date, int id) {
        apiClient.getApiService(context).getRoutePlanDate(date, id)
                .enqueue(new Callback<List<RoutePlan>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<RoutePlan>> call, @NonNull Response<List<RoutePlan>> response) {
                        if (response.isSuccessful()) {
                            // update database time
                            Long now = Calendar.getInstance().getTimeInMillis();
                            sessionManager.saveAccessUpdateDate(now.toString());
                            //clear database
                            nexDataBase.routePlanDao().deleteAll(nexDataBase.routePlanDao().getAll());
                            nexDataBase.costumerDAO().deleteAll(nexDataBase.costumerDAO().getAll());
                            nexDataBase.answerDao().deleteAll(nexDataBase.answerDao().getAllUploaded());
                            nexDataBase.taggingDAO().deleteAll(nexDataBase.taggingDAO().getAllUploaded());
                            List<RoutePlan> routePlanList = response.body();

                            //Save response to database
                            if (!routePlanList.isEmpty()) {
                                routePlanList.forEach(routePlan -> {
                                    Costumer costumer = routePlan.getCostumer();
                                    String address = routePlan.getCostumer().getAddress();
                                    Log.d("TAG", "onResponse: "+routePlan);

                                    routePlan.setCostumerId(routePlan.getCostumer().getApiId());
                                    routePlan.setCostumerName(routePlan.getCostumer().getName());
                                    routePlan.setCostumerImage(routePlan.getCostumer().getImage());
                                    routePlan.setCostumerAddress(address);
                                    routePlan.setUserId(routePlan.getUser().getId());

                                    //saving costumer and route plan
                                    nexDataBase.costumerDAO().insert(costumer);
                                    nexDataBase.routePlanDao().insert(routePlan);
                                });
                                Toast.makeText(context, "Database rute berhasil diperbarui", Toast.LENGTH_SHORT).show();
                                if (sessionManager.fetchAccessRoles().equals("Survey")) {
                                    //If roles is survey, send survey, question, option, and answer request to API
                                    downloadSurveyData(date);
                                    downloadQuestionData();
                                    downloadOptionData();
                                    downloadAnswer();
                                } else  {
                                    // Tag only download tag data
                                    downloadTagging();
                                }
                            }
                        } else {
                            if (response.code()==401){
                                Toast.makeText(context, "Akun tidak valid, Mohon login ulang!", Toast.LENGTH_SHORT).show();
                            } else  {
                                Toast.makeText(context, "Tidak ada data rute pada server", Toast.LENGTH_SHORT).show();
                            }
                        }
                        view.updateDate();
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<RoutePlan>> call, @NonNull Throwable t) {
                        Toast.makeText(context, "Gagal kontak server, database rute gagal diperbarui!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void checkData(String date, int id) {
        apiClient.getApiService(context).getRoutePlanDate(date, id)
                .enqueue(new Callback<List<RoutePlan>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(@NonNull Call<List<RoutePlan>> call, @NonNull Response<List<RoutePlan>> response) {
                        if (response.isSuccessful()) {
                            List<RoutePlan> routePlanListDatabase = nexDataBase.routePlanDao().getAll();
                            List<RoutePlan> routePlanListApi = response.body();
                            //Check whether there are new update based on data size
                            if (routePlanListDatabase.size() == routePlanListApi.size()) {
                                Toast.makeText(context, "Tidak ada pembaruan", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Pembaruan tersedia! silahkan download data", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Tidak ada data pada server", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(@NonNull Call<List<RoutePlan>> call, @NonNull Throwable t) {
                        Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void downloadTagging() {
        List<RoutePlan> routePlanList = nexDataBase.routePlanDao().getAll();
        if (!routePlanList.isEmpty()) {
            routePlanList.forEach(routePlan -> {
                apiClient.getApiService(context).getTagging(routePlan.getApiId())
                        .enqueue(new Callback<List<Tagging>>() {
                            @Override
                            public void onResponse(@NonNull Call<List<Tagging>> call, @NonNull Response<List<Tagging>> response) {
                                if (response.isSuccessful()) {
                                    List<Tagging> taggingList = response.body();
                                    taggingList.forEach(tagging ->
                                            {
                                                //Saving api resp to database
                                                tagging.setRoutePlanId(tagging.getRoutePlan().getApiId());
                                                tagging.setStatus(201);
                                                tagging.setCostumerName(tagging.getRoutePlan().getCostumer().getName());
                                                nexDataBase.taggingDAO().insertTag(tagging);
                                            }
                                    );
                                }
                            }
                            @Override
                            public void onFailure(@NonNull Call<List<Tagging>> call, @NonNull Throwable t) {
                                Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                            }
                        });
            });
        };
    }

    private void downloadSurveyData(String date) {
        apiClient.getApiService(context).getSurveyList(date)
                .enqueue(new Callback<List<Survey>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(@NonNull Call<List<Survey>> call, @NonNull Response<List<Survey>> response) {
                        if (response.isSuccessful()) {
                            Long now = Calendar.getInstance().getTimeInMillis();
                            sessionManager.saveAccessUpdateDate(now.toString());
                            nexDataBase.surveyDAO().deleteAll(nexDataBase.surveyDAO().getAll());
                            List<Survey> surveyList = response.body();
                            if (!surveyList.isEmpty()) {
                                surveyList.forEach(survey -> {
                                    //Saving api resp to database
                                    Question question = survey.getQuestion();

                                    survey.setQuestionId(question.getApiId());
                                    survey.setCostumerId(survey.getCostumer().getApiId());

                                    nexDataBase.surveyDAO().insert(survey);
                                });
                                Toast.makeText(context, "Database survey berhasil diperbarui", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "Tidak ada data survey pada server", Toast.LENGTH_SHORT).show();
                        }
                        view.updateDate();
                    }
                    @Override
                    public void onFailure(@NonNull Call<List<Survey>> call, @NonNull Throwable t) {
                        Toast.makeText(context, "Gagal kontak server, database survey gagal diperbarui!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void downloadQuestionData() {
        apiClient.getApiService(context).getQuestionList()
                .enqueue(new Callback<List<Question>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(@NonNull Call<List<Question>> call, @NonNull Response<List<Question>> response) {
                        if (response.isSuccessful()) {
                            Long now = Calendar.getInstance().getTimeInMillis();
                            sessionManager.saveAccessUpdateDate(now.toString());
                            nexDataBase.questionDao().deleteAll(nexDataBase.questionDao().getAll());
                            List<Question> questionList = response.body();
                            if (!questionList.isEmpty()) {
                                //Saving api resp to database
                                questionList.forEach(question -> { nexDataBase.questionDao().insert(question); });
                            }
                            Toast.makeText(context, "Database pertanyaan berhasil diperbarui", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Tidak ada data pertanyaan pada server", Toast.LENGTH_SHORT).show();
                        }
                        view.updateDate();
                    }
                    @Override
                    public void onFailure(@NonNull Call<List<Question>> call, @NonNull Throwable t) {
                        Toast.makeText(context, "Gagal kontak server, pertanyaan gagal diperbarui!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void downloadOptionData() {
        apiClient.getApiService(context).getOptionList()
                .enqueue(new Callback<List<Option>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(@NonNull Call<List<Option>> call, @NonNull Response<List<Option>> response) {
                        if (response.isSuccessful()) {
                            Long now = Calendar.getInstance().getTimeInMillis();
                            sessionManager.saveAccessUpdateDate(now.toString());
                            nexDataBase.optionDAO().deleteAll(nexDataBase.optionDAO().getAll());
                            List<Option> optionList = response.body();
                            if (!optionList.isEmpty()) {
                                //Saving api resp to database
                                optionList.forEach(option -> { nexDataBase.optionDAO().insert(option); });
                            }
                            Toast.makeText(context, "Database pilihan jawaban berhasil diperbarui", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Tidak ada data pada server", Toast.LENGTH_SHORT).show();
                        }
                        view.updateDate();
                    }
                    @Override
                    public void onFailure(@NonNull Call<List<Option>> call, @NonNull Throwable t) {
                        Toast.makeText(context, "Gagal kontak server, pilihan jawaban gagal diperbarui!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void downloadAnswer() {
        List<RoutePlan> routePlanList = nexDataBase.routePlanDao().getAll();
        if (!routePlanList.isEmpty()) {
            routePlanList.forEach(routePlan -> {
                apiClient.getApiService(context).getAnswer(routePlan.getApiId())
                        .enqueue(new Callback<List<Answer>>() {
                            @Override
                            public void onResponse(@NonNull Call<List<Answer>> call, @NonNull Response<List<Answer>> response) {
                                if (response.isSuccessful()) {
                                    List<Answer> answerList = response.body();
                                    answerList.forEach(answer ->
                                            {
                                                //Saving api resp to database
                                                answer.setRoutePlanId(answer.getRoutePlan().getApiId());
                                                answer.setStatus(201);
                                                answer.setCostumerName(answer.getRoutePlan().getCostumer().getName());
                                                nexDataBase.answerDao().insert(answer);
                                            }
                                    );
                                }
                            }
                            @Override
                            public void onFailure(@NonNull Call<List<Answer>> call, @NonNull Throwable t) {
                                Toast.makeText(context, "Gagal kontak server", Toast.LENGTH_SHORT).show();
                            }
                        });
            });
        };
    }
}