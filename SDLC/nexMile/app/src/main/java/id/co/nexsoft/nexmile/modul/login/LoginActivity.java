package id.co.nexsoft.nexmile.modul.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.modul.home.MainActivity;
import id.co.nexsoft.nexmile.modul.login.presenter.LoginPresenterContract;
import id.co.nexsoft.nexmile.modul.login.presenter.LoginPresenterImpl;
import id.co.nexsoft.nexmile.modul.login.view.LoginViewContract;
import id.co.nexsoft.nexmile.modul.register.RegisterActivity;


public class LoginActivity extends AppCompatActivity implements LoginViewContract {

    private LoginPresenterContract loginPresenterContract;

    Button btnLogin;
    LinearLayout llProgressBar;
    EditText edtUsername;
    EditText edtPassword;
    TextView tvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenterContract = new LoginPresenterImpl(this, this);

        btnLogin = findViewById(R.id.btnLogin);
        llProgressBar = findViewById(R.id.llProgressBar);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        tvRegister = findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(view -> {
            toRegister();
        });

        btnLogin.setOnClickListener(view -> {

            String username = edtUsername.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();

            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Username dan password wajib diisi!", Toast.LENGTH_LONG).show();
            } else {
                onClickVerification(username, password, this);
            }
        });
    }

    public void toMain() {
        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    public void toRegister() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        registerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(registerIntent);
    }

    //reCAPTCHA
    private void onClickVerification(String username, String password, Context context) {
        SafetyNet.getClient(LoginActivity.this).verifyWithRecaptcha("6Lf-g2gfAAAAANThjdhJ3KASVFA-85h0be9jWy_A")
                .addOnSuccessListener(response -> {
                    // Indicates communication with reCAPTCHA service was
                    // successful.
                    String userResponseToken = response.getTokenResult();
                    if (!userResponseToken.isEmpty()) {
                        // Validate the user response token using the
                        // reCAPTCHA siteverify API.
                        login(username, password, context);
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(LoginActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                    if (e instanceof ApiException) {
                        // An error occurred when communicating with the
                        // reCAPTCHA service. Refer to the status code to
                        // handle the error appropriately.
                        ApiException apiException = (ApiException) e;
                        int statusCode = apiException.getStatusCode();
                        Toast.makeText(LoginActivity.this,"API Exception: " + statusCode, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void login(String username, String password, Context context) {
        llProgressBar.setVisibility(View.VISIBLE);
        loginPresenterContract.login(username, password, context);
    }

    @Override
    public void onSucceed() {
        llProgressBar.setVisibility(View.GONE);
        toMain();
    }


    // validation
    @Override
    public void onNotMatch() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Username dan password tidak sesuai", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNotActive() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Akun anda belum aktif, silahkan hubungi Admin!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Gagal kontak server", Toast.LENGTH_LONG).show();
    }
}