package id.co.nexsoft.nexmile.modul.tagging.presenter;

import java.io.File;

import id.co.nexsoft.nexmile.core.model.Tagging;

public interface TaggingEditPresenterContract {
    void saveTag(String imageUrl, String serialNumber, String contractNumber, String assetDate, int routePlanId, long apiId);

    void deleteTag(int id);

    void updateTag(int id, String imageUrl, String serialNumber, String contractNumber, String assetDate, int routePlanId, long apiId);

    Tagging loadTagById(int tagId);

    void saveBitmapToFile(File file);
}
