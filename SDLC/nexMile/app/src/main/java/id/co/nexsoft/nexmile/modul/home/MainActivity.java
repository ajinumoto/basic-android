package id.co.nexsoft.nexmile.modul.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import java.util.Date;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.modul.download.DownloadActivity;
import id.co.nexsoft.nexmile.modul.home.presenter.MainPresenterContract;
import id.co.nexsoft.nexmile.modul.home.presenter.MainPresenterImpl;
import id.co.nexsoft.nexmile.modul.login.LoginActivity;
import id.co.nexsoft.nexmile.modul.report.ReportListActivity;
import id.co.nexsoft.nexmile.modul.routeplan.RouteListActivity;
import id.co.nexsoft.nexmile.utils.SessionManager;

public class MainActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private MainPresenterContract presenter;
    Button btnLogout, btnTagging, btnDownload, btnReport;
    ImageView ivUser;
    TextView tvUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenterImpl(this);
        btnLogout = findViewById(R.id.btnLogout);
        btnTagging = findViewById(R.id.btnTagging);
        btnDownload = findViewById(R.id.btnDownload);
        btnReport = findViewById(R.id.btnReport);
        tvUsername = findViewById(R.id.tvUsername);
        ivUser = findViewById(R.id.ivUser);

        sessionManager = new SessionManager(this);
        String token = sessionManager.fetchAccessToken();

        //Check user have token?
        if (token == null) {
            toLogin();
            presenter.clearDatabase();
        }

        if (sessionManager.fetchAccessRoles().equals("Survey")) {
            btnTagging.setText("Survey");
        }
        tvUsername.setText(sessionManager.fetchAccessUsername());

        // Load user image using picasso
        try {
            Picasso.get().load(sessionManager.fetchAccessImage()).into(ivUser);
        } catch (Exception ignored) {
        }

        btnTagging.setOnClickListener(view -> toRoutePlan());
        btnLogout.setOnClickListener(view -> toLogin());
        Log.d("Home", "onCreate: " + sessionManager.fetchAccessRoles());
        btnDownload.setOnClickListener(view -> toDownload());
        btnReport.setOnClickListener(view -> toReport());


        Log.d("TAG", "onCreate: "+sessionManager.fetchAccessUpdateDate());
        if (sessionManager.fetchAccessUpdateDate()!=null){
            presenter.checkDate(Long.parseLong(sessionManager.fetchAccessUpdateDate()));
        }
    }

    public void toRoutePlan() {
        Intent intent = new Intent(this, RouteListActivity.class);
        startActivity(intent);
    }

    public void toReport() {
        Intent intent = new Intent(this, ReportListActivity.class);
        startActivity(intent);
    }

    public void toDownload() {
        Intent intent = new Intent(this, DownloadActivity.class);
        startActivity(intent);
    }

    public void toLogin() {
        sessionManager.deleteAccessToken();
        presenter.clearDatabase();
        Intent loginIntent = new Intent(this, LoginActivity.class);

        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

}