package id.co.nexsoft.nexmile.modul.download.presenter;

public interface DownloadPresenterContract {
    void downloadData(String date, int user);

    void checkData(String date, int user);
}
