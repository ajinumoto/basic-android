package id.co.nexsoft.nexmile.modul.home.presenter;

public interface MainPresenterContract {
    void clearDatabase();
    void checkDate(Long updateDateInMillis);
}
