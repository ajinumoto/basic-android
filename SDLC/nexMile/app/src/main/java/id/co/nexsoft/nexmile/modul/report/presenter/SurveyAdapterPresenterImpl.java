package id.co.nexsoft.nexmile.modul.report.presenter;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.modul.report.SurveyAdapter;

public class SurveyAdapterPresenterImpl implements SurveyAdapterPresenterContract {

    private SurveyAdapter surveyAdapter;

    private NexDataBase nexDataBase;

    public SurveyAdapterPresenterImpl(SurveyAdapter surveyAdapter, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.surveyAdapter = surveyAdapter;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean checkCloud(int id) {
        Answer answer = nexDataBase.answerDao().loadById(id);
        //check whether data has been successfully uploaded or not
        if (answer.getStatus() == 201) {
            return true;
        }
        return false;
    }

}