package id.co.nexsoft.nexmile.modul.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.modul.home.MainActivity;
import id.co.nexsoft.nexmile.modul.login.LoginActivity;
import id.co.nexsoft.nexmile.modul.register.presenter.RegisterPresenterContract;
import id.co.nexsoft.nexmile.modul.register.presenter.RegisterPresenterImpl;
import id.co.nexsoft.nexmile.modul.register.view.RegisterViewContract;


public class RegisterActivity extends AppCompatActivity implements RegisterViewContract, AdapterView.OnItemSelectedListener {

    private RegisterPresenterContract registerPresenterContract;

    Button btnRegister;
    LinearLayout llProgressBar;
    EditText edtUsername;
    EditText edtEmail;
    EditText edtPassword, edtRetypePassword;
    ImageView ivClose;
    Spinner spinnerRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerPresenterContract = new RegisterPresenterImpl(this, this);

        btnRegister = findViewById(R.id.btnRegister);
        llProgressBar = findViewById(R.id.llProgressBar);
        edtUsername = findViewById(R.id.edtUsername);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtRetypePassword = findViewById(R.id.edtRetypePassword);
        ivClose = findViewById(R.id.ivClose);
        spinnerRole = findViewById(R.id.spinnerRole);

        ivClose.setOnClickListener(view -> {
            toLogin();
        });

        btnRegister.setOnClickListener(view -> {
            llProgressBar.setVisibility(View.VISIBLE);

            String username = edtUsername.getText().toString().trim();
            String email = edtEmail.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            String retypePassword = edtRetypePassword.getText().toString().trim();
            String role = String.valueOf(spinnerRole.getSelectedItem());

            if (username.isEmpty() || password.isEmpty() || email.isEmpty()) {
                llProgressBar.setVisibility(View.GONE);
                Toast.makeText(this, "Data belum lengkap!", Toast.LENGTH_LONG).show();
            } else {
                //Show validation
                if(isValidUsername(username)){
                    if (isValidEmail(email)){
                        if(isValidPassword(password)){
                            if (password.equals(retypePassword)){
                                register(username,email,password,role,this);
                            }else {
                                llProgressBar.setVisibility(View.GONE);
                                Toast.makeText(this, "Cek kembali kedua password anda!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            llProgressBar.setVisibility(View.GONE);
                            Toast.makeText(this, "Minimal 8 karakter yang mengandung huruf kecil, huruf kapita, dan angka!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        llProgressBar.setVisibility(View.GONE);
                        Toast.makeText(this, "Email tidak valid!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    llProgressBar.setVisibility(View.GONE);
                    Toast.makeText(this, "Hanya terdiri dari huruf dan angka dengan panjang 5-16 karakter!", Toast.LENGTH_LONG).show();
                }
            }
        });

        List<String> roles = new ArrayList<String>();
        roles.add("Tagging");
        roles.add("Survey");

        spinnerRole.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, roles);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRole.setAdapter(dataAdapter);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        // Password should contain number,lower case, uppercase with minimum length 8
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public boolean isValidUsername(final String username) {

        Pattern pattern;
        Matcher matcher;

        // username should only contain uppercase/lowercase, or numbers with length 5-16 char
        final String USERNAME_PATTERN = "^[a-zA-Z0-9]{5,16}$";

        pattern = Pattern.compile(USERNAME_PATTERN);
        matcher = pattern.matcher(username);

        return matcher.matches();

    }

    public void toLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    @Override
    public void register(String username, String email, String password, String role, Context context) {
        llProgressBar.setVisibility(View.VISIBLE);
        registerPresenterContract.register(username, email, password, role, context);
    }

    @Override
    public void onSucceed() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Pendaftaran berhasil, hubungi admin untuk aktivasi akun!", Toast.LENGTH_LONG).show();
        toLogin();
    }

    @Override
    public void onUsernameInvalid() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Username telah terdaftar!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onEmailInvalid() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Email telah terdaftar!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailed() {
        llProgressBar.setVisibility(View.GONE);
        Toast.makeText(getApplicationContext(), "Gagal kontak server", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}