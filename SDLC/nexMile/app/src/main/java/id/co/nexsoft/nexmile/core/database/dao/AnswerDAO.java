package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Tagging;


@Dao
public interface AnswerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Answer answer);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Answer answer);

    @Delete()
    void deleteAll(List<Answer> answers);

    // get all uploaded data
    @Query("Select * From Answer Where status=201")
    List<Answer> getAllUploaded();

    // get all data
    @Query("Select * From Answer")
    List<Answer> getAll();

    // get data by id
    @Query("SELECT * FROM Answer WHERE id = :id")
    Answer loadById(int id);

    // get list of data from route plan data
    @Query("SELECT * FROM Answer WHERE route_plan_id = :routePlanId")
    List<Answer> loadByRouteId(int routePlanId);

}
