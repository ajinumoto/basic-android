package id.co.nexsoft.nexmile.modul.routeplan.view;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.RoutePlan;

public interface RouteListViewContract {
    void getRoutePlan(List<RoutePlan> routePlanList);
}
