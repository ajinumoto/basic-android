package id.co.nexsoft.nexmile.modul.routeplan.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.routeplan.view.RouteListViewContract;

public class RouteListPresenterImpl implements RouteListPresenterContract {

    private RouteListViewContract routeListViewContract;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;

    public RouteListPresenterImpl(RouteListViewContract routeListViewContract, Context context) {
        apiClient = new APIClient();
        this.nexDataBase = NexDataBase.getInstance(context);
        this.routeListViewContract = routeListViewContract;
        this.context = context;
    }

    @Override
    public void getRoutePlan() {
        //letak executor
        routeListViewContract.getRoutePlan(nexDataBase.routePlanDao().getAll());
    }


}