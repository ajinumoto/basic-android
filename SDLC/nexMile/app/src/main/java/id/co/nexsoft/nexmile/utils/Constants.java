package id.co.nexsoft.nexmile.utils;

public class Constants {
    public static final String UPDATE_TAG_ID = "tag_id";
    public static final String UPDATE_ROUTE_ID = "route_id";
    public static final String UPDATE_COSTUMER_ID = "costumer_id";
    public static final String COSTUMER_NAME = "costumer_name";
    public static final String COSTUMER_ADDRESS = "costumer_address";
    public static final String COSTUMER_IMAGE = "costumer_image";
}
