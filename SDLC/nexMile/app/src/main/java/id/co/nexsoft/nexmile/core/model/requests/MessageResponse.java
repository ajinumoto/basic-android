package id.co.nexsoft.nexmile.core.model.requests;

import com.google.gson.annotations.SerializedName;

public class MessageResponse {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

}
