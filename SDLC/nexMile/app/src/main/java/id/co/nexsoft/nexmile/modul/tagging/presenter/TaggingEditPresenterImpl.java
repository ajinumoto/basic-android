package id.co.nexsoft.nexmile.modul.tagging.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.tagging.view.TaggingEditViewContract;

public class TaggingEditPresenterImpl implements TaggingEditPresenterContract {

    private TaggingEditViewContract view;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;


    public TaggingEditPresenterImpl(TaggingEditViewContract view, Context context) {
        apiClient = new APIClient();
        this.nexDataBase = NexDataBase.getInstance(context);
        this.view = view;
        this.context = context;
    }

    @Override
    public void saveTag(String imageUrl, String serialNumber, String contractNumber, String assetDate, int routePlanId, long apiId) {
        //save tag to local db
        Tagging tagging = new Tagging();
        tagging.setImageUrl(imageUrl);
        tagging.setSerialNumber(serialNumber);
        tagging.setContractNumber(contractNumber);
        tagging.setAssetDate(assetDate);
        tagging.setRoutePlanId(routePlanId);
        tagging.setStatus(0);
        nexDataBase.taggingDAO().insertTag(tagging);
    }

    @Override
    public void deleteTag(int id) {
        nexDataBase.taggingDAO().deleteTagById(id);
    }

    @Override
    public void updateTag(int id, String imageUrl, String serialNumber, String contractNumber, String assetDate, int routePlanId, long apiId) {
        //update tag to local db
        Tagging tagging = new Tagging();
        tagging.setId(id);
        tagging.setImageUrl(imageUrl);
        tagging.setSerialNumber(serialNumber);
        tagging.setContractNumber(contractNumber);
        tagging.setAssetDate(assetDate);
        tagging.setRoutePlanId(routePlanId);
        tagging.setStatus(0);
        tagging.setApiId(apiId);
        nexDataBase.taggingDAO().updateTag(tagging);
    }

    @Override
    public Tagging loadTagById(int tagId) {
        return nexDataBase.taggingDAO().loadTagById(tagId);
    }

    // Compress image file size
    @Override
    public void saveBitmapToFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = 1;

            // factor of downsizing the image
            FileInputStream inputStream = new FileInputStream(file);
            BitmapFactory.decodeStream(inputStream, null, options);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=1024;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(options.outWidth / scale >= REQUIRED_SIZE ||
                    options.outHeight / scale >= REQUIRED_SIZE) {
                scale *= 2;
            }

            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, options);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 80 , outputStream);

            Log.d("TAG", "saveBitmapToFile: " + Integer.parseInt(String.valueOf(file.length()/1024)));

        } catch (Exception e) {
            Log.e("TAG", "saveBitmapToFile: ", e);
        }
    }


}