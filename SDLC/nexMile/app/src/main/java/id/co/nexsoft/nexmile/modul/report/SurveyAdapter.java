package id.co.nexsoft.nexmile.modul.report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.report.presenter.SurveyAdapterPresenterContract;
import id.co.nexsoft.nexmile.modul.report.presenter.SurveyAdapterPresenterImpl;
import id.co.nexsoft.nexmile.modul.report.view.SurveyAdapterViewContract;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder> implements SurveyAdapterViewContract {

    private Context context;
    private List<Answer> answerList;
    private SurveyAdapterPresenterContract presenterContract;

    public SurveyAdapter(Context context, List<Answer> answerList) {
        this.context = context;
        this.answerList = answerList;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        presenterContract = new SurveyAdapterPresenterImpl(this, recyclerView.getContext());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_report, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Answer answer = answerList.get(position);
        //show green cloud status if data has been uploaded
        if (presenterContract.checkCloud(answer.getId())) {
            holder.ivReportStatus.setImageResource(R.drawable.ic_baseline_cloud_done_green_24);
        }
        holder.tvCostumer.setText(answer.getCostumerName());
        holder.tvSerialNumber.setText(answer.getAnswer());
    }

    @Override
    public int getItemCount() {
        return answerList.size();
    }

    @Override
    public void changeImage(int position) {
        notifyItemChanged(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCostumer, tvSerialNumber;
        private ImageView ivReportStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCostumer = itemView.findViewById(R.id.tvCostumer);
            tvSerialNumber = itemView.findViewById(R.id.tvSerialNumber);
            ivReportStatus = itemView.findViewById(R.id.ivReportStatus);
            ivReportStatus.setOnClickListener(view -> {

            });
        }
    }
}
