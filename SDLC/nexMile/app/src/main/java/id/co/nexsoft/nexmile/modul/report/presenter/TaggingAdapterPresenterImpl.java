package id.co.nexsoft.nexmile.modul.report.presenter;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.modul.report.TaggingAdapter;

public class TaggingAdapterPresenterImpl implements TaggingAdapterPresenterContract {

    private TaggingAdapter reportAdapter;

    private NexDataBase nexDataBase;

    public TaggingAdapterPresenterImpl(TaggingAdapter reportAdapter, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.reportAdapter = reportAdapter;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public boolean checkCloud(int id) {
        Tagging tagging = nexDataBase.taggingDAO().loadTagById(id);
        //check whether data has been successfully uploaded or not
        if (tagging.getStatus() == 201) {
            return true;
        }
        return false;
    }

}