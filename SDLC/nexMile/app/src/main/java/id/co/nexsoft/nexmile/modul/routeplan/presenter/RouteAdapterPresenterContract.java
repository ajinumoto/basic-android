package id.co.nexsoft.nexmile.modul.routeplan.presenter;

public interface RouteAdapterPresenterContract {
    void saveRoutePlan(int routeId, int position);

    boolean checkCloud(int routeId);

    boolean checkFilled(int routeId);
}
