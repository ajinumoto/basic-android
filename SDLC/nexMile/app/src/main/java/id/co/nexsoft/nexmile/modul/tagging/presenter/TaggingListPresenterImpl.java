package id.co.nexsoft.nexmile.modul.tagging.presenter;

import android.content.Context;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.repo.APIClient;
import id.co.nexsoft.nexmile.modul.tagging.view.TaggingListViewContract;

public class TaggingListPresenterImpl implements TaggingListPresenterContract {

    private TaggingListViewContract taggingListViewContract;
    private NexDataBase nexDataBase;
    private APIClient apiClient;
    private Context context;

    public TaggingListPresenterImpl(TaggingListViewContract taggingListViewContract, Context context) {
        apiClient = new APIClient();
        this.nexDataBase = NexDataBase.getInstance(context);
        this.taggingListViewContract = taggingListViewContract;
        this.context = context;
    }

    @Override
    public void getAllTags(int routePlanId) {
        taggingListViewContract.getAllTags(nexDataBase.taggingDAO().loadByRouteId(routePlanId));
    }

}