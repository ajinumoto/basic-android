package id.co.nexsoft.nexmile.modul.tagging.view;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.core.model.Tagging;

public interface TaggingListViewContract {
    void getAllTags(List<Tagging> taggingList);

    void getRoutePlan(List<RoutePlan> routePlanList);

}
