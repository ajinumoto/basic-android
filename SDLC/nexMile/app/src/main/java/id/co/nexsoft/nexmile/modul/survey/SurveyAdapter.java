package id.co.nexsoft.nexmile.modul.survey;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.Question;
import id.co.nexsoft.nexmile.modul.survey.presenter.SurveyAdapterPresenterContract;
import id.co.nexsoft.nexmile.modul.survey.presenter.SurveyAdapterPresenterImpl;
import id.co.nexsoft.nexmile.modul.survey.view.SurveyAdapterViewContract;


public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder> implements SurveyAdapterViewContract {

    private Context context;
    private List<Question> questionList;
    private HashMap<Integer, List> answerMap = new HashMap<Integer, List>();
    private SurveyAdapterPresenterContract presenterContract;

    public SurveyAdapter(Context context, List<Question> questionList) {
        this.context = context;
        this.questionList = questionList;
        presenterContract = new SurveyAdapterPresenterImpl(this, context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_question, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Question question = questionList.get(position);
        Log.d("TAG", "onBindViewHolder: " + question.getQuestion());
        holder.tvQuestion.setText(question.getQuestion());
        List<String> answer = new ArrayList<>();

        // Create question based on its tipe
        switch (question.getTypeId()) {
            case 1://Text
                holder.etQuestion.setVisibility(View.VISIBLE);
                holder.etQuestion.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (editable.length() > 0) {
                            answer.clear();
                            answer.add(editable.toString());
                            answerMap.put(question.getApiId(), answer);
                        } else {
                            answerMap.remove(question.getApiId());
                        }
                    }
                });
                break;
            case 2://Radio
                holder.rgQuestion.setVisibility(View.VISIBLE);
                for (String option : presenterContract.getAllOption(question.getApiId())) {
                    RadioButton rbOption = new RadioButton(holder.rgQuestion.getContext());
                    rbOption.setText(option);
                    rbOption.setOnCheckedChangeListener((compoundButton, checked) -> {
                        if (checked) {
                            answer.add(option);
                        } else {
                            answer.remove(option);
                        }

                        if (answer.size() > 0) {
                            answerMap.put(question.getApiId(), answer);
                        } else {
                            answerMap.remove(question.getApiId());
                        }
                    });
                    holder.rgQuestion.addView(rbOption);
                }
                break;
            case 3://Checkbox
                holder.llQuestion.setVisibility(View.VISIBLE);
                for (String option : presenterContract.getAllOption(question.getApiId())) {
                    CheckBox cbQuestion = new CheckBox(holder.llQuestion.getContext());
                    cbQuestion.setText(option);
                    cbQuestion.setOnCheckedChangeListener((compoundButton, checked) -> {
                        if (checked) {
                            answer.add(option);
                        } else {
                            answer.remove(option);
                        }

                        if (answer.size() > 0) {
                            answerMap.put(question.getApiId(), answer);
                        } else {
                            answerMap.remove(question.getApiId());
                        }
                    });
                    holder.llQuestion.addView(cbQuestion);
                }
                break;
            default:
        }
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    @Override
    public void saveTag(int routeId) {
        presenterContract.saveAnswer(answerMap, routeId);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestion;
        private EditText etQuestion;
        private RadioGroup rgQuestion;
        private LinearLayout llQuestion;

        public ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = itemView.findViewById(R.id.tvQuestion);
            etQuestion = itemView.findViewById(R.id.etQuestion);
            rgQuestion = itemView.findViewById(R.id.rgQuestion);
            llQuestion = itemView.findViewById(R.id.llCheckBox);
        }
    }
}
