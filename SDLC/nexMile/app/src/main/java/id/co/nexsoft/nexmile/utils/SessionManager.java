package id.co.nexsoft.nexmile.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;

import id.co.nexsoft.nexmile.R;


public class SessionManager {
    // Shared Preferences is the way in which one can store and retrieve small amounts of primitive data as key/value pairs to a file on the device storage such as String, int, float, Boolean that make up your preferences in an XML file inside the app on the device storage. Shared Preferences can be thought of as a dictionary or a key/value pair
    private SharedPreferences prefs;
    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ROLES = "user_roles";
    public static final String USER_USERNAME = "user_username";
    public static final String USER_IMAGE = "user_image";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_ID = "user_id";
    public static final String LAST_DATABASE_UPDATE = "database_update_date";

    public SessionManager(@NotNull Context context) {
        this.prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public void saveAccessToken(@NotNull String token) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACCESS_TOKEN, token)
                .apply();
    }

    public String fetchAccessToken() {
        return prefs.getString(ACCESS_TOKEN, null);
    }

    public void saveAccessUsername(@NotNull String username) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_USERNAME, username)
                .apply();
    }

    public String fetchAccessImage() {
        return prefs.getString(USER_IMAGE, null);
    }

    public void saveAccessImage(@NotNull String image) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_IMAGE, image)
                .apply();
    }

    public String fetchAccessUsername() {
        return prefs.getString(USER_USERNAME, null);
    }

    public void saveAccessEmail(@NotNull String email) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_EMAIL, email)
                .apply();
    }

    public String fetchAccessEmail() {
        return prefs.getString(USER_EMAIL, null);
    }

    public void saveAccessRoles(@NotNull String roles) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_ROLES, roles)
                .apply();
    }

    public int fetchAccessUserId() {
        return Integer.parseInt(prefs.getString(USER_ID, null));
    }

    public void saveAccessUserId(@NotNull String userId) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_ID, userId)
                .apply();
    }

    public String fetchAccessUpdateDate() {
        return prefs.getString(LAST_DATABASE_UPDATE, null);
    }

    public void saveAccessUpdateDate(@NotNull String date) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LAST_DATABASE_UPDATE, date)
                .apply();
    }

    public String fetchAccessRoles() {
        return prefs.getString(USER_ROLES, null);
    }

    public void deleteAccessToken() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear().apply();
    }


}
