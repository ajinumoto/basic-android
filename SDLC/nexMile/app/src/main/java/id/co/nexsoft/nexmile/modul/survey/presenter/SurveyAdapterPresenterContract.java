package id.co.nexsoft.nexmile.modul.survey.presenter;

import java.util.HashMap;
import java.util.List;

public interface SurveyAdapterPresenterContract {
    List<String> getAllOption(int questionId);

    void saveAnswer(HashMap<Integer, List> answerMap, int routeId);

    HashMap<Integer, List> loadAnswer(int routeId);
}
