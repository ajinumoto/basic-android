package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.RoutePlan;


@Dao
public interface RoutePlanDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RoutePlan routePlan);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(RoutePlan routePlan);

    @Delete()
    void deleteAll(List<RoutePlan> routePlans);

    // get all data
    @Query("Select * From route_plan")
    List<RoutePlan> getAll();

    // get data by id
    @Query("SELECT * FROM route_plan WHERE id = :id")
    RoutePlan loadById(int id);

    // get data by API id
    @Query("SELECT * FROM route_plan WHERE api_id = :apiId")
    RoutePlan loadByApiId(int apiId);

}
