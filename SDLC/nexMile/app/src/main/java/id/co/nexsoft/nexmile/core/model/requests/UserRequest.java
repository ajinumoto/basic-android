package id.co.nexsoft.nexmile.core.model.requests;

import com.google.gson.annotations.SerializedName;

public class UserRequest {
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;
    @SerializedName("id")
    private int id;

    public UserRequest() {
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }
}
