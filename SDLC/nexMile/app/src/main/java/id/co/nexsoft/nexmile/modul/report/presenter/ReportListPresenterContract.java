package id.co.nexsoft.nexmile.modul.report.presenter;

public interface ReportListPresenterContract {
    void getAllData();
    void saveAllData();
}
