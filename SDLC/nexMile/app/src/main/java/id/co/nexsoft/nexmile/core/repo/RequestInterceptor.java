package id.co.nexsoft.nexmile.core.repo;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.IOException;

import id.co.nexsoft.nexmile.utils.SessionManager;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {
    private SessionManager sessionManager;

    public RequestInterceptor(Context context) {
        this.sessionManager = new SessionManager(context);
    }


    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        Request.Builder requestBuilder = chain.request().newBuilder();
        if (sessionManager.fetchAccessToken() != null) {
            requestBuilder.addHeader("Authorization", "Bearer " + sessionManager.fetchAccessToken());
        }

        return chain.proceed(requestBuilder.build());
    }
}