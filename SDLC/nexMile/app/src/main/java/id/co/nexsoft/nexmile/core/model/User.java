package id.co.nexsoft.nexmile.core.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class User {
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("image")
    private String image;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("roles")
    private List<String> roles;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("status")
    private String status;

    public User() {
    }

    public String getStatus() {
        return status;
    }

    public String getUsername() {
        return username;
    }

    public String getImage() {
        return image;
    }

    public String getEmail() {
        return email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public List<String> getRoles() {
        return roles;
    }

    public int getUserId() {
        return userId;
    }
}
