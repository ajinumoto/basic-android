package id.co.nexsoft.nexmile.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import id.co.nexsoft.nexmile.core.model.requests.UserRequest;

@Entity(tableName = "route_plan")
public class RoutePlan {
    @SerializedName("retrofitId")
    @Expose(serialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("id")
    @ColumnInfo(name = "api_id")
    private int apiId;

    @SerializedName("date")
    @ColumnInfo(name = "date")
    private String date;

    @SerializedName("user")
    @Expose(serialize = false)
    @ColumnInfo(name = "user_id")
    private int userId;

    @SerializedName("userId")
    @Ignore
    private UserRequest user;

    @SerializedName("costumerId")
    @Expose(serialize = false)
    @Ignore
    private Costumer costumer;

    @SerializedName("retrofitCostumerId")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_id")
    private int costumerId;

    @SerializedName("costumerName")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_name")
    private String costumerName;

    @SerializedName("costumer_image")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_image")
    private String costumerImage;

    @SerializedName("costumer_address")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_address")
    private String costumerAddress;

    @SerializedName("completedDate")
    @ColumnInfo(name = "completed_date")
    private String completedDate;

    @SerializedName("status")
    @Expose(serialize = false)
    @ColumnInfo(name = "status")
    private int status;

    public RoutePlan() {
    }


    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public String getCostumerImage() {
        return costumerImage;
    }

    public void setCostumerImage(String costumerImage) {
        this.costumerImage = costumerImage;
    }

    public String getCostumerAddress() {
        return costumerAddress;
    }

    public void setCostumerAddress(String costumerAddress) {
        this.costumerAddress = costumerAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(int costumerId) {
        this.costumerId = costumerId;
    }

    public String getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(String completedDate) {
        this.completedDate = completedDate;
    }

    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UserRequest getUser() {
        return user;
    }

    public void setUser(UserRequest user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RoutePlan{" +
                "id=" + id +
                ", apiId=" + apiId +
                ", date='" + date + '\'' +
                ", userId=" + userId +
                ", user=" + user +
                ", costumer=" + costumer +
                ", costumerId=" + costumerId +
                ", costumerName='" + costumerName + '\'' +
                ", costumerImage='" + costumerImage + '\'' +
                ", costumerAddress='" + costumerAddress + '\'' +
                ", completedDate='" + completedDate + '\'' +
                ", status=" + status +
                '}';
    }
}