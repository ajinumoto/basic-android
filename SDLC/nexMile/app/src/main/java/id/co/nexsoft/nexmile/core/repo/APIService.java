package id.co.nexsoft.nexmile.core.repo;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.core.model.Option;
import id.co.nexsoft.nexmile.core.model.Question;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.core.model.Survey;
import id.co.nexsoft.nexmile.core.model.Tagging;
import id.co.nexsoft.nexmile.core.model.User;
import id.co.nexsoft.nexmile.core.model.requests.LoginRequest;
import id.co.nexsoft.nexmile.core.model.requests.MessageResponse;
import id.co.nexsoft.nexmile.core.model.requests.SignUpRequest;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    //Authentication
    @POST("/api/auth/signin")
    Call<User> login(@Body LoginRequest request);
    @POST("/api/auth/signup")
    Call<MessageResponse> signup(@Body SignUpRequest request);

    //Route Plan
    @GET("/api/routeplan/userdate")
    Call<List<RoutePlan>> getRoutePlanDate(@Query("date") String date, @Query("userId") int id);
    @GET("/api/tagging/routeplan")
    Call<List<Tagging>> getTagging(@Query("routePlanId") int routePlanId);
    @GET("/api/answer/routeplan")
    Call<List<Answer>> getAnswer(@Query("routePlanId") int routePlanId);

    //Tagging
    @POST("/api/taglist")
    Call<List<Tagging>> saveTaggingList(@Body List<Tagging> taggingList);

    //Survey
    @GET("/api/survey/date")
    Call<List<Survey>> getSurveyList(@Query("date") String date);
    @GET("/api/option")
    Call<List<Option>> getOptionList();
    @GET("/api/question")
    Call<List<Question>> getQuestionList();
    @POST("/api/answerlist")
    Call<List<Answer>> saveAnswerList(@Body List<Answer> answerList);

    //image
    @Multipart
    @POST("/api/files/upload")
    Call<ResponseBody> upload(
            @Part("pathName") RequestBody path,
            @Part MultipartBody.Part file
    );
}