package id.co.nexsoft.nexmile.modul.routeplan;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.nexsoft.nexmile.R;
import id.co.nexsoft.nexmile.core.model.RoutePlan;
import id.co.nexsoft.nexmile.modul.routeplan.presenter.RouteListPresenterContract;
import id.co.nexsoft.nexmile.modul.routeplan.presenter.RouteListPresenterImpl;
import id.co.nexsoft.nexmile.modul.routeplan.view.RouteListViewContract;

public class RouteListActivity extends AppCompatActivity implements RouteListViewContract {

    private RouteListPresenterContract routeListPresenterContract;

    private RecyclerView recyclerView;
    private TextView tvNoData;
    private RouteAdapterView routeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        routeListPresenterContract = new RouteListPresenterImpl(this, this);
        initViews();
        routeListPresenterContract.getRoutePlan();
    }

    @Override
    protected void onResume() {
        super.onResume();
        retrieveTasks();
    }

    public void retrieveTasks() {
        routeListPresenterContract.getRoutePlan();
    }

    private void initViews() {
        recyclerView = findViewById(R.id.recyclerViewRoute);
        tvNoData = findViewById(R.id.tvNoData);
    }

    @Override
    public void getRoutePlan(List<RoutePlan> routePlanList) {
        if (routePlanList.size()==0){
            tvNoData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.GONE);
        }
        routeAdapter = new RouteAdapterView(this, routePlanList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(routeAdapter);
        routeAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}