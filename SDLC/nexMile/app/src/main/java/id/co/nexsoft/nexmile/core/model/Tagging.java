package id.co.nexsoft.nexmile.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tagging")
public class Tagging {
    @SerializedName("retrofitId")
    @Expose(serialize = false)
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("id")
    @ColumnInfo(name = "api_id")
    private Long apiId;

    @SerializedName("imageUrl")
    @ColumnInfo(name = "image_url")
    private String imageUrl;

    @SerializedName("serialNumber")
    @ColumnInfo(name = "serial_number")
    private String serialNumber;

    @SerializedName("contractNumber")
    @ColumnInfo(name = "contract_number")
    private String contractNumber;

    @SerializedName("assetDate")
    @ColumnInfo(name = "asset_date")
    private String assetDate;

    @SerializedName("routePlanId")
    @ColumnInfo(name = "route_plan_id")
    private int routePlanId;

    @SerializedName("routePlan")
    @Ignore
    private RoutePlan routePlan;

    @SerializedName("status")
    @Expose(serialize = false)
    @ColumnInfo(name = "status")
    private int status;

    @SerializedName("costumerName")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_name")
    private String costumerName;

    public Tagging() {
    }

    public int getRoutePlanId() {
        return routePlanId;
    }

    public void setRoutePlanId(int routePlanId) {
        this.routePlanId = routePlanId;
    }

    public Long getApiId() {
        return apiId;
    }

    public void setApiId(Long apiId) {
        this.apiId = apiId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getAssetDate() {
        return assetDate;
    }

    public void setAssetDate(String assetDate) {
        this.assetDate = assetDate;
    }

    public RoutePlan getRoutePlan() {
        return routePlan;
    }

    public void setRoutePlan(RoutePlan routePlan) {
        this.routePlan = routePlan;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }
}
