package id.co.nexsoft.nexmile.modul.survey.presenter;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import id.co.nexsoft.nexmile.core.database.NexDataBase;
import id.co.nexsoft.nexmile.core.model.Answer;
import id.co.nexsoft.nexmile.modul.survey.SurveyAdapter;

public class SurveyAdapterPresenterImpl implements SurveyAdapterPresenterContract {

    private SurveyAdapter surveyAdapter;
    private NexDataBase nexDataBase;

    public SurveyAdapterPresenterImpl(SurveyAdapter surveyAdapter, Context context) {
        this.nexDataBase = NexDataBase.getInstance(context);
        this.surveyAdapter = surveyAdapter;
    }

    @Override
    public List<String> getAllOption(int questionId) {
        return nexDataBase.optionDAO().loadStringByQuestionId(questionId);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    //save using hasmap key value
    public void saveAnswer(HashMap<Integer, List> answerMap, int routeId) {
        answerMap.forEach((key, value) -> {
            //save answer to database
            Answer answer = new Answer();
            answer.setQuestionId(key);
            answer.setAnswer(value.toString().substring(1, value.toString().length() - 1));
            answer.setRoutePlanId(routeId);
            answer.setCostumerName(nexDataBase.routePlanDao().loadByApiId(routeId).getCostumerName());
            nexDataBase.answerDao().insert(answer);
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public HashMap<Integer, List> loadAnswer(int routeId) {
        HashMap<Integer, List> answerMap = new HashMap<Integer, List>();
        List<Answer> answerList = nexDataBase.answerDao().loadByRouteId(routeId);
        answerList.forEach(answer -> {
            List<String> stringList = Arrays.asList(answer.getAnswer().split(","));
            answerMap.put(answer.getQuestionId(), stringList);
        });
        return answerMap;
    }

}