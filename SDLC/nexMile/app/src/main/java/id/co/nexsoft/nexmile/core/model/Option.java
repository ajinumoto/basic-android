package id.co.nexsoft.nexmile.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "option")
public class Option {
    @SerializedName("retrofitId")
    @Expose(serialize = false)
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("id")
    @ColumnInfo(name = "api_id")
    private int apiId;

    @SerializedName("questionOption")
    @ColumnInfo(name = "question_option")
    private String questionOption;

    @SerializedName("questionId")
    @ColumnInfo(name = "question_id")
    private int questionId;

    public Option() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public String getQuestionOption() {
        return questionOption;
    }

    public void setQuestionOption(String questionOption) {
        this.questionOption = questionOption;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
