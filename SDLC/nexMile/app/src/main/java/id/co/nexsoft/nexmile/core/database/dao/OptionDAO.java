package id.co.nexsoft.nexmile.core.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Option;


@Dao
public interface OptionDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Option option);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Option option);

    @Delete()
    void deleteAll(List<Option> options);

    // gat all data
    @Query("Select * From Option")
    List<Option> getAll();

    // get data by id
    @Query("SELECT * FROM Option WHERE id = :id")
    Option loadById(int id);

    // get data from question id
    @Query("SELECT question_option FROM option WHERE question_id = :questionId")
    List<String> loadStringByQuestionId(int questionId);

}
