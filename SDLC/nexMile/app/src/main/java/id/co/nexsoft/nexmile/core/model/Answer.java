package id.co.nexsoft.nexmile.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "answer")
public class Answer {
    @SerializedName("retrofitId")
    @Expose(serialize = false)
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("id")
    @ColumnInfo(name = "api_id")
    private Long apiId;

    @SerializedName("routePlanId")
    @ColumnInfo(name = "route_plan_id")
    private int routePlanId;

    @SerializedName("routePlan")
    @Ignore
    private RoutePlan routePlan;

    @SerializedName("questionId")
    @ColumnInfo(name = "questionId")
    private int questionId;

    @SerializedName("answer")
    @ColumnInfo(name = "answer")
    private String answer;

    @SerializedName("status")
    @Expose(serialize = false)
    @ColumnInfo(name = "status")
    private int status;

    @SerializedName("costumerName")
    @Expose(serialize = false)
    @ColumnInfo(name = "costumer_name")
    private String costumerName;

    public Answer() {
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public Long getApiId() {
        return apiId;
    }

    public void setApiId(Long apiId) {
        this.apiId = apiId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoutePlanId() {
        return routePlanId;
    }

    public void setRoutePlanId(int routePlanId) {
        this.routePlanId = routePlanId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RoutePlan getRoutePlan() {
        return routePlan;
    }

    public void setRoutePlan(RoutePlan routePlan) {
        this.routePlan = routePlan;
    }
}
