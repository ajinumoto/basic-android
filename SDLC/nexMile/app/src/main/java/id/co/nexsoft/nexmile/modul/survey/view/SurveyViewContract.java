package id.co.nexsoft.nexmile.modul.survey.view;

import java.util.List;

import id.co.nexsoft.nexmile.core.model.Question;

public interface SurveyViewContract {
    void getAllQuestion(List<Question> questionList);
}
