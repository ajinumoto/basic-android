package id.co.nexsoft.nextmile;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.models.Tagging;
import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;
import id.co.nexsoft.nextmile.repository.TaggingRepository;
import id.co.nexsoft.nextmile.services.TaggingService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class TaggingTest {

	@InjectMocks
	TaggingService taggingService;
	@Mock
	TaggingService mTaggingService;

	@MockBean
	TaggingRepository mTaggingRepository;
	@MockBean
	RoutePlanRepository mRoutePlanRepository;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Tagging tagging = new Tagging();
		tagging.setRoutePlanId(id);
		tagging.setAssetDate(Date.valueOf("2022-05-05"));
		tagging.setContractNumber("1A2B3456");
		tagging.setImageUrl("imageUrl");
		tagging.setSerialNumber("FF233SDF");

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		assertEquals(HttpStatus.CREATED, taggingService.save(tagging).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Tagging tagging = new Tagging();
		tagging.setRoutePlanId(id);
		tagging.setAssetDate(Date.valueOf("2022-05-05"));
		tagging.setContractNumber("1A2B3456");
		tagging.setImageUrl("imageUrl");
		tagging.setSerialNumber("FF233SDF");

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mTaggingRepository.findById(id)).thenReturn(Optional.of(new Tagging()));
		assertEquals(HttpStatus.OK, taggingService.update(id, tagging).getStatusCode());
	}

	@Test
	void getAll() throws Exception {
		List<Tagging> taggingList = new ArrayList<Tagging>();
		taggingList.add(new Tagging());
		taggingList.add(new Tagging());
		taggingList.add(new Tagging());

		when(mTaggingRepository.findAll()).thenReturn(taggingList);
		assertEquals(new ResponseEntity<>(taggingList, HttpStatus.OK), taggingService.getAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Tagging tagging = new Tagging();
		tagging.setRoutePlanId(id);
		tagging.setAssetDate(Date.valueOf("2022-05-05"));
		tagging.setContractNumber("1A2B3456");
		tagging.setImageUrl("imageUrl");
		tagging.setSerialNumber("FF233SDF");

		when(mTaggingRepository.findById(id)).thenReturn(Optional.of(tagging));
		assertEquals(new ResponseEntity<>(tagging, HttpStatus.OK), taggingService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), taggingService.deleteById(id));
	}

	// Access test
	@Test
	@WithMockUser(username = "survey", roles = { "SURVEY" })
	void accessTest() throws Exception {
		Long id = 1L;
		User userId = new User("tagging", "tagging@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);
		Tagging tagging = new Tagging(id, routePlan, "imageUrl", "FF233SDF",
				"1A2B3456", Date.valueOf("2022-05-05"), null, id);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mTaggingRepository.findById(id)).thenReturn(Optional.of(tagging));
		mockMvc.perform(post("/api/tagging")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(tagging)))
				.andExpect(status().isForbidden());
	}

	// Integration Test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void integrationTest() throws Exception {
		Long id = 1L;
		User userId = new User("tagging", "tagging@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);
		Tagging tagging = new Tagging(id, routePlan, "imageUrl", "FF233SDF",
				"1A2B3456", Date.valueOf("2022-05-05"), null, id);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mTaggingRepository.findById(id)).thenReturn(Optional.of(tagging));
		mockMvc.perform(post("/api/tagging")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(tagging)))
				.andExpect(status().isCreated())
				.andDo(print())
				.andDo(result -> mockMvc
						.perform(get("/api/tagging/" + id))
						.andExpect(status().isOk())
						.andDo(print()));
	}
}