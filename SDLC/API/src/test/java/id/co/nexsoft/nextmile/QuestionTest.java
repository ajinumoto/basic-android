package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.QuestionType;
import id.co.nexsoft.nextmile.repository.QuestionRepository;
import id.co.nexsoft.nextmile.services.QuestionService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class QuestionTest {

	@InjectMocks
	QuestionService questionService;
	@Mock
	QuestionService mQuestionService;

	@MockBean
	QuestionRepository mQuestionRepository;

	@Autowired
	private MockMvc mockMvc;
	
	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);

		assertEquals(HttpStatus.CREATED, questionService.save(question).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);

		when(mQuestionRepository.findById(id)).thenReturn(Optional.of(new Question()));
		assertEquals(HttpStatus.OK, questionService.update(id, question).getStatusCode());
	}

	@Test
	void getAll() throws Exception {
		List<Question> questionList = new ArrayList<Question>();
		questionList.add(new Question());
		questionList.add(new Question());
		questionList.add(new Question());

		when(mQuestionRepository.findAll()).thenReturn(questionList);
		assertEquals(new ResponseEntity<>(questionList, HttpStatus.OK), questionService.getAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);

		when(mQuestionRepository.findById(id)).thenReturn(Optional.of(question));
		assertEquals(new ResponseEntity<>(question, HttpStatus.OK), questionService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), questionService.deleteById(id));
	}

	// Access test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void accessTest() throws Exception {
		Long id = 1L;
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);

		when(mQuestionRepository.findById(id)).thenReturn(Optional.of(question));
		mockMvc.perform(get("/api/question/" + id))
				.andExpect(status().isForbidden());
	}

	// Integration Test
	@Test
	@WithMockUser(username = "survey", roles = { "SURVEY" })
	void integrationTest() throws Exception {
		Long id = 1L;
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);

		when(mQuestionRepository.findById(id)).thenReturn(Optional.of(question));
		mockMvc.perform(get("/api/question/" + id))
				.andExpect(status().isOk());
	}
}