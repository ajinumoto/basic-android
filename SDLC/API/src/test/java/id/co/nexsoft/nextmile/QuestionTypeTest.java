package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import id.co.nexsoft.nextmile.models.QuestionType;
import id.co.nexsoft.nextmile.repository.QuestionTypeRepository;
import id.co.nexsoft.nextmile.services.QuestionTypeService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class QuestionTypeTest {

	@InjectMocks
	QuestionTypeService questiontypeService;
	@Mock
	QuestionTypeService mQuestionTypeService;

	@MockBean
	QuestionTypeRepository mQuestionTypeRepository;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;
		QuestionType questionType = new QuestionType(id,"Text");

		when(mQuestionTypeRepository.save(questionType)).thenReturn(questionType);
		assertEquals(questionType, questiontypeService.save(questionType));
	}

	@Test
	void getAll() throws Exception {
		List<QuestionType> questiontypeList = new ArrayList<QuestionType>();
		questiontypeList.add(new QuestionType());
		questiontypeList.add(new QuestionType());
		questiontypeList.add(new QuestionType());

		when(mQuestionTypeRepository.findAll()).thenReturn(questiontypeList);
		assertEquals(questiontypeList, questiontypeService.findAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;
		QuestionType questionType = new QuestionType(id,"Text");

		when(mQuestionTypeRepository.findById(id)).thenReturn(Optional.of(questionType));
		assertEquals(Optional.of(questionType), questiontypeService.findById(id));
	}
}