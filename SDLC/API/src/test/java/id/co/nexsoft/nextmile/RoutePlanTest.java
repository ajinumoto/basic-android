package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;
import id.co.nexsoft.nextmile.services.RoutePlanService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class RoutePlanTest {

	@InjectMocks
	RoutePlanService routeplanService;
	@Mock
	RoutePlanService mRoutePlanService;

	@MockBean
	RoutePlanRepository mRoutePlanRepository;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan(id, new User(), new Costumer(), null, null, null);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		assertEquals(HttpStatus.CREATED, routeplanService.save(routePlan).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan(id, new User(), new Costumer(), null, null, null);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mRoutePlanRepository.findById(id)).thenReturn(Optional.of(new RoutePlan()));
		assertEquals(HttpStatus.OK, routeplanService.update(id, routePlan).getStatusCode());
	}

	@Test
	void getAll() throws Exception {
		List<RoutePlan> routeplanList = new ArrayList<RoutePlan>();
		routeplanList.add(new RoutePlan());
		routeplanList.add(new RoutePlan());
		routeplanList.add(new RoutePlan());

		when(mRoutePlanRepository.findAll()).thenReturn(routeplanList);
		assertEquals(new ResponseEntity<>(routeplanList, HttpStatus.OK), routeplanService.findAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan(id, new User(), new Costumer(), null, null, null);

		when(mRoutePlanRepository.findById(id)).thenReturn(Optional.of(routePlan));
		assertEquals(new ResponseEntity<>(routePlan, HttpStatus.OK), routeplanService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), routeplanService.deleteById(id));
	}

	// Access test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void accessTest() throws Exception {
		Long id = 1L;
		User userId = new User("routeplan", "routeplan@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);

		when(mRoutePlanRepository.findById(id)).thenReturn(Optional.of(routePlan));
		mockMvc.perform(post("/api/routeplan")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(routePlan)))
				.andExpect(status().isForbidden());
	}

	// Integration Test
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	void integrationTest() throws Exception {
		Long id = 1L;
		User userId = new User("routeplan", "routeplan@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mRoutePlanRepository.findById(id)).thenReturn(Optional.of(routePlan));
		mockMvc.perform(post("/api/routeplan")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(routePlan)))
				.andExpect(status().isCreated())
				.andDo(print())
				.andDo(result -> mockMvc
						.perform(get("/api/routeplan/" + id))
						.andExpect(status().isOk())
						.andDo(print()));
	}
}