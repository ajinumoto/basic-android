package id.co.nexsoft.nextmile;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.QuestionType;
import id.co.nexsoft.nextmile.models.Survey;
import id.co.nexsoft.nextmile.repository.SurveyRepository;
import id.co.nexsoft.nextmile.services.SurveyService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class SurveyTest {

	@InjectMocks
	SurveyService surveyService;
	@Mock
	SurveyService mSurveyService;

	@MockBean
	SurveyRepository mSurveyRepository;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;
		Costumer costumer = new Costumer(id, "address", "name", "image", null);
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);
		Survey survey = new Survey(id, costumer, question, Date.valueOf("2022-05-05"), null);

		when(mSurveyRepository.save(survey)).thenReturn(survey);
		assertEquals(HttpStatus.CREATED, surveyService.save(survey).getStatusCode());
	}

	@Test
	void getAll() throws Exception {
		List<Survey> surveyList = new ArrayList<Survey>();
		surveyList.add(new Survey());
		surveyList.add(new Survey());
		surveyList.add(new Survey());

		when(mSurveyRepository.findAll()).thenReturn(surveyList);
		assertEquals(new ResponseEntity<>(surveyList, HttpStatus.OK), surveyService.getAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;
		Costumer costumer = new Costumer(id, "address", "name", "image", null);
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);
		Survey survey = new Survey(id, costumer, question, Date.valueOf("2022-05-05"), null);

		when(mSurveyRepository.findById(id)).thenReturn(Optional.of(survey));
		assertEquals(new ResponseEntity<>(survey, HttpStatus.OK), surveyService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), surveyService.deleteById(id));
	}

	// Access test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void accessTest() throws Exception {
		Long id = 1L;
		Costumer costumer = new Costumer(id, "address", "name", "image", null);
		Question question = new Question(id, new QuestionType(id, "text"), "How Are You?", null);
		Survey survey = new Survey(id, costumer, question, Date.valueOf("2022-05-05"), null);
		
		when(mSurveyRepository.findById(id)).thenReturn(Optional.of(survey));
		mockMvc.perform(get("/api/survey/"+id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(survey)))
				.andExpect(status().isForbidden());
	}
}