package id.co.nexsoft.nextmile;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import id.co.nexsoft.nextmile.models.Answer;
import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.models.Survey;
import id.co.nexsoft.nextmile.models.Tagging;
import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.payload.request.LoginRequest;
import id.co.nexsoft.nextmile.payload.request.SignupRequest;
import id.co.nexsoft.nextmile.payload.response.Data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTest {

    // Server-side Spring MVC testing
    @Autowired
    private MockMvc mockMvc;

    // Read or Write JSON
    @Autowired
    private ObjectMapper objectMapper;

    // Registration Testing
    @Test
    void registration() throws Exception {

        // Sign up form
        SignupRequest newUser = new SignupRequest();
        newUser.setUsername("userTest");
        newUser.setEmail("user@gmail.com");
        newUser.setPassword("Qwerty123");
        Set<String> role = new HashSet<String>();
        role.add("TAGGING");

        // Sign up in server
        mockMvc.perform(post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newUser)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Error: Username is already taken!"))
                .andDo(print());
    }

    // Login Testing
    @Test
    void login() throws Exception {

        // Login form
        LoginRequest user = new LoginRequest();
        user.setUsername("userTest");
        user.setPassword("Qwerty123");

        // Login test in server
        mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    // Account activation Testing
    @Test
    void activated() throws Exception {

        // Login form
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("12345678");

        // Send login form
        ResultActions resultActions = mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk())
                .andDo(print());

        // Save login response as token
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Data loginResponse = objectMapper.readValue(contentAsString, Data.class);
        String token = loginResponse.getAccessToken();

        Long id = 4L;

        // Get user info that want to be activated
        resultActions = mockMvc.perform(get("/api/user/{id}", id).header("authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andDo(print());

        result = resultActions.andReturn();
        contentAsString = result.getResponse().getContentAsString();
        User user = objectMapper.readValue(contentAsString, User.class);

        // Change user status to active
        user.setStatus("active");

        // Update user info to server
        mockMvc.perform(put("/api/user/{id}", id).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user)).header("authorization", "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("active"))
                .andDo(print());
    }

    // Tagging Test
    @Test
    void tagging() throws Exception {

        // Login form for Admin
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("12345678");

        // Save admin token from response
        ResultActions resultActions = mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk())
                .andDo(print());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Data loginResponse = objectMapper.readValue(contentAsString, Data.class);
        String adminToken = loginResponse.getAccessToken();

        // Set route plan for user
        User userId = new User();
        userId.setId(4L);
        Costumer costumerId = new Costumer();
        costumerId.setId(1L);

        String date = "2022-05-05";

        RoutePlan routePlan = new RoutePlan(1L, userId, costumerId, Date.valueOf(date), null, null);

        // Saving route plan data to server  
        mockMvc.perform(post("/api/routeplan").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(routePlan)).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());

        // Seles login
        loginRequest.setUsername("userTest");
        loginRequest.setPassword("Qwerty123");

        // Save tagging token from login response
        resultActions = mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk())
                .andDo(print());

        result = resultActions.andReturn();
        contentAsString = result.getResponse().getContentAsString();
        loginResponse = objectMapper.readValue(contentAsString, Data.class);
        String taggingToken = loginResponse.getAccessToken();

        // Get routeplan for certain user and date
        resultActions = mockMvc.perform(
                get("/api/routeplan/userdate").param("userId", String.valueOf(loginResponse.getUserId()))
                        .param("date", date).header("authorization", "Bearer " + taggingToken))
                .andExpect(status().isOk())
                .andDo(print());

        result = resultActions.andReturn();
        contentAsString = result.getResponse().getContentAsString();

        List<RoutePlan> routeList = new ArrayList<RoutePlan>();

        routeList = objectMapper.readValue(contentAsString, new TypeReference<List<RoutePlan>>() {
        });

        // Tagging user collect the tag data
        Tagging tag = new Tagging(1L, routeList.get(0), "imageUrl", "serialNumber123", "contractNumber123",
                Date.valueOf(date), null, routePlan.getId());

        // Save Tag data to server
        mockMvc.perform(post("/api/tagging").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(tag)).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());

        // Admin check whether the data appeared or not
        mockMvc.perform(
                get("/api/tagging/routeplan").param("routePlanId", "1").header("authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andDo(print());

        java.util.Date today = new java.util.Date();

        // Check route plan is complete or not
        resultActions = mockMvc.perform(
                get("/api/routeplan/{id}", 1).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.completedDate").value((new Date(today.getTime()).toString())))
                .andDo(print());
    }

// Tagging Test
    @Test
    void survey() throws Exception {

        // Login form for Admin
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("admin");
        loginRequest.setPassword("12345678");

        // Save admin token from response
        ResultActions resultActions = mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk())
                .andDo(print());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        Data loginResponse = objectMapper.readValue(contentAsString, Data.class);
        String adminToken = loginResponse.getAccessToken();

        // Set route plan for user
        User userId = new User();
        userId.setId(3L);
        Costumer costumerId = new Costumer();
        costumerId.setId(1L);

        String date = "2022-05-05";

        RoutePlan routePlan = new RoutePlan(2L, userId, costumerId, Date.valueOf(date), null, null);

        // Saving route plan data to server  
        mockMvc.perform(post("/api/routeplan").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(routePlan)).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(2))
                .andDo(print());

        // Set survey for user
        Question question = new Question();
        question.setId(1L);
        Survey survey = new Survey(1L, costumerId, question, Date.valueOf(date), null);

        // Save survey data to server  
        mockMvc.perform(post("/api/survey").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(survey)).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());

        // Seles login
        loginRequest.setUsername("survey");
        loginRequest.setPassword("12345678");

        // Save tagging token from login response
        resultActions = mockMvc.perform(post("/api/auth/signin").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isOk())
                .andDo(print());

        result = resultActions.andReturn();
        contentAsString = result.getResponse().getContentAsString();
        loginResponse = objectMapper.readValue(contentAsString, Data.class);
        String surveyToken = loginResponse.getAccessToken();

        // Get routeplan for certain user and date
        resultActions = mockMvc.perform(
                get("/api/routeplan/userdate").param("userId", String.valueOf(loginResponse.getUserId()))
                        .param("date", date).header("authorization", "Bearer " + surveyToken))
                .andExpect(status().isOk())
                .andDo(print());

        result = resultActions.andReturn();
        contentAsString = result.getResponse().getContentAsString();

        List<RoutePlan> routeList = new ArrayList<RoutePlan>();

        routeList = objectMapper.readValue(contentAsString, new TypeReference<List<RoutePlan>>() {
        });

        // Surveyor collect the survey data
        Answer answer = new Answer(1L, routePlan, question, "This is the Answer", null, question.getId(), routePlan.getId());

        // Save survey data to server
        mockMvc.perform(post("/api/answer").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)).header("authorization", "Bearer " + surveyToken))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());

        // Admin check whether the data appeared or not
        mockMvc.perform(
                get("/api/answer/{id}", 1).param("routePlanId", "1").header("authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andDo(print());

        java.util.Date today = new java.util.Date();

        // Check route plan is complete or not
        resultActions = mockMvc.perform(
                get("/api/routeplan/{id}", 1).header("authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.completedDate").value((new Date(today.getTime()).toString())))
                .andDo(print());
    }

}
