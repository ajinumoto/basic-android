package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.repository.CostumerRepository;
import id.co.nexsoft.nextmile.services.CostumerService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class CostumerTest {

	@InjectMocks
	CostumerService costumerService;
	@Mock
	CostumerService mCostumerService;

	@MockBean
	CostumerRepository mCostumerRepository;


	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;

		Costumer costumer = new Costumer();
		costumer.setId(id);
		costumer.setName("Ahmad Mujahidin");
		costumer.setAddress("Jalan Saleh Sungkar No. 2, Jakarta");
		costumer.setImage("Image URL");

		assertEquals(HttpStatus.CREATED, costumerService.save(costumer).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;

		Costumer costumer = new Costumer();
		costumer.setId(id);
		costumer.setName("Ahmad Mujahidin");
		costumer.setAddress("Jalan Saleh Sungkar No. 2, Jakarta");
		costumer.setImage("Image URL");

		when(mCostumerRepository.findById(id)).thenReturn(Optional.of(new Costumer()));
		assertEquals(HttpStatus.OK, costumerService.update(id, costumer).getStatusCode());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;

		Costumer costumer = new Costumer();
		costumer.setId(id);
		costumer.setName("Ahmad Mujahidin");
		costumer.setAddress("Jalan Saleh Sungkar No. 2, Jakarta");
		costumer.setImage("Image URL");

		when(mCostumerRepository.findById(id)).thenReturn(Optional.of(costumer));
		assertEquals(new ResponseEntity<>(costumer, HttpStatus.OK), costumerService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), costumerService.deleteById(id));
	}

	@Test
	void getAll() throws Exception {
		List<Costumer> costumerList = new ArrayList<Costumer>();
		costumerList.add(new Costumer());
		costumerList.add(new Costumer());
		costumerList.add(new Costumer());

		when(mCostumerRepository.findAll()).thenReturn(costumerList);
		assertEquals(new ResponseEntity<>(costumerList, HttpStatus.OK), costumerService.getAll());
	}

	// Access test
	@Test
	void accessTest() throws Exception {
		Long id = 1L;
		Costumer costumer = new Costumer(id, "address", "name", "image", null);

		when(mCostumerRepository.findById(id)).thenReturn(Optional.of(costumer));
		mockMvc.perform(post("/api/costumer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(costumer)))
				.andExpect(status().isUnauthorized());
	}

	// Integration Test
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	void integrationTest() throws Exception {
		Long id = 1L;
		Costumer costumer = new Costumer(id, "address", "name", "image", null);

		when(mCostumerRepository.findById(id)).thenReturn(Optional.of(costumer));
		mockMvc.perform(post("/api/costumer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(costumer)))
				.andExpect(status().isCreated())
				.andDo(print())
				.andDo(result -> mockMvc
						.perform(get("/api/costumer/" + id))
						.andExpect(status().isOk())
						.andDo(print()));
	}
}