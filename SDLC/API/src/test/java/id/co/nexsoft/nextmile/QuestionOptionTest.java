package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.QuestionOption;
import id.co.nexsoft.nextmile.repository.QuestionOptionRepository;
import id.co.nexsoft.nextmile.services.QuestionOptionService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class QuestionOptionTest {

	@InjectMocks
	QuestionOptionService questionoptionService;
	@Mock
	QuestionOptionService mQuestionOptionService;

	@MockBean
	QuestionOptionRepository mQuestionOptionRepository;

	@Autowired
	private MockMvc mockMvc;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;

		QuestionOption questionoption = new QuestionOption(id, new Question(), "Sangat Puas", null);

		assertEquals(HttpStatus.CREATED, questionoptionService.save(questionoption).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;

		QuestionOption questionoption = new QuestionOption(id, new Question(), "Sangat Puas", null);

		when(mQuestionOptionRepository.findById(id)).thenReturn(Optional.of(new QuestionOption()));
		assertEquals(HttpStatus.OK, questionoptionService.update(id, questionoption).getStatusCode());
	}

	@Test
	void getAll() throws Exception {
		List<QuestionOption> questionoptionList = new ArrayList<QuestionOption>();
		questionoptionList.add(new QuestionOption());
		questionoptionList.add(new QuestionOption());
		questionoptionList.add(new QuestionOption());

		when(mQuestionOptionRepository.findAll()).thenReturn(questionoptionList);
		assertEquals(new ResponseEntity<>(questionoptionList, HttpStatus.OK), questionoptionService.getAll());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;
		QuestionOption questionoption = new QuestionOption(id, new Question(), "Sangat Puas", null);

		when(mQuestionOptionRepository.findById(id)).thenReturn(Optional.of(questionoption));
		assertEquals(new ResponseEntity<>(questionoption, HttpStatus.OK), questionoptionService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), questionoptionService.deleteById(id));
	}

	// Access test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void accessTest() throws Exception {
		Long id = 1L;
		QuestionOption questionoption = new QuestionOption(id, new Question(), "Sangat Puas", null);

		when(mQuestionOptionRepository.findById(id)).thenReturn(Optional.of(questionoption));
		mockMvc.perform(get("/api/option/" + id))
				.andExpect(status().isForbidden());
	}

}