package id.co.nexsoft.nextmile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.models.Answer;
import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;
import id.co.nexsoft.nextmile.repository.AnswerRepository;
import id.co.nexsoft.nextmile.services.AnswerService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class AnswerTest {

	@InjectMocks
	AnswerService answerService;
	@Mock
	AnswerService mAnswerService;

	@MockBean
	AnswerRepository mAnswerRepository;
	@MockBean
	RoutePlanRepository mRoutePlanRepository;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	// Repository Test
	@Test
	void save() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Question question = new Question();
		question.setId(id);

		Answer answer = new Answer();
		answer.setRoutePlanId(id);
		answer.setRoutePlan(routePlan);
		answer.setQuestionId(id);
		answer.setQuestion(question);
		answer.setAnswer("Benar");

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		assertEquals(HttpStatus.CREATED, answerService.save(answer).getStatusCode());
	}

	@Test
	void update() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Question question = new Question();
		question.setId(id);

		Answer answer = new Answer();
		answer.setRoutePlanId(id);
		answer.setRoutePlan(routePlan);
		answer.setQuestionId(id);
		answer.setQuestion(question);
		answer.setAnswer("Benar");

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mAnswerRepository.findById(id)).thenReturn(Optional.of(new Answer()));
		assertEquals(HttpStatus.OK, answerService.update(id, answer).getStatusCode());
	}


	@Test
	void saveList() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Question question = new Question();
		question.setId(id);

		Answer answer = new Answer();
		answer.setRoutePlanId(id);
		answer.setRoutePlan(routePlan);
		answer.setQuestionId(id);
		answer.setQuestion(question);
		answer.setAnswer("Benar");

		List<Answer> answerList = new ArrayList<Answer>();

		answerList.add(answer);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		assertEquals(HttpStatus.CREATED, answerService.saveList(answerList).getStatusCode());
	}

	@Test
	void findById() throws Exception {
		Long id = 1L;

		RoutePlan routePlan = new RoutePlan();
		routePlan.setId(id);

		Question question = new Question();
		question.setId(id);

		Answer answer = new Answer();
		answer.setRoutePlanId(id);
		answer.setRoutePlan(routePlan);
		answer.setQuestionId(id);
		answer.setQuestion(question);
		answer.setAnswer("Benar");

		when(mAnswerRepository.findById(id)).thenReturn(Optional.of(answer));
		assertEquals(new ResponseEntity<>(answer, HttpStatus.OK), answerService.findById(id));
	}

	@Test
	void deleteById() throws Exception {
		Long id = 1L;

		assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), answerService.deleteById(id));
	}

	@Test
	void getAll() throws Exception {
		List<Answer> answerList = new ArrayList<Answer>();
		answerList.add(new Answer());
		answerList.add(new Answer());
		answerList.add(new Answer());

		when(mAnswerRepository.findAll()).thenReturn(answerList);
		assertEquals(new ResponseEntity<>(answerList, HttpStatus.OK), answerService.getAll());
	}

	// Access test
	@Test
	@WithMockUser(username = "tagging", roles = { "TAGGING" })
	void accessTest() throws Exception {
		Long id = 1L;
		User userId = new User("answer", "answer@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);
		Answer answer = new Answer(id, routePlan, new Question(), "Benar", null, id, id);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mAnswerRepository.findById(id)).thenReturn(Optional.of(answer));
		mockMvc.perform(post("/api/answer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(answer)))
				.andExpect(status().isForbidden());
	}

	// Integration Test
	@Test
	@WithMockUser(username = "survey", roles = { "SURVEY" })
	void integrationTest() throws Exception {
		Long id = 1L;
		User userId = new User("answer", "answer@gmail.com", "12345678");
		Costumer costumerId = new Costumer(id, "address", "name", "image", null);
		RoutePlan routePlan = new RoutePlan(id, userId, costumerId, null, null, null);
		Answer answer = new Answer(id, routePlan, new Question(), "Benar", null, id, id);

		when(mRoutePlanRepository.getById(id)).thenReturn(routePlan);
		when(mAnswerRepository.findById(id)).thenReturn(Optional.of(answer));
		mockMvc.perform(post("/api/answer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(answer)))
				.andExpect(status().isCreated())
				.andDo(print())
				.andDo(result -> mockMvc
						.perform(get("/api/answer/" + id))
						.andExpect(status().isOk())
						.andDo(print()));
	}
}