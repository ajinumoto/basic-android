package id.co.nexsoft.nextmile.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nextmile.models.Costumer;

@Repository
public interface CostumerRepository extends JpaRepository<Costumer, Long> {

    // Pagination for all data
    Page<Costumer> findAll(Pageable pageable);

    List<Costumer> findAll(Sort pageable);

    // Pagination for data based on costumer name
    Page<Costumer> findByNameContaining(String name, Pageable pageable);

    // Pagination for data based on costumer address
    Page<Costumer> findByAddressContaining(String address, Pageable pageable);
}
