package id.co.nexsoft.nextmile.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import id.co.nexsoft.nextmile.models.QuestionType;
import id.co.nexsoft.nextmile.repository.QuestionTypeRepository;

@Service
public class QuestionTypeService {
    
    @Autowired
    private QuestionTypeRepository questiontypeRepository;

    public QuestionType save(QuestionType questiontype) {
        return questiontypeRepository.save(questiontype);
    }

    public Iterable<QuestionType> findAll() {
        return questiontypeRepository.findAll();
    }

    public Optional<QuestionType> findById(Long id) {
        return questiontypeRepository.findById(id);
    }

    public void deleteById(Long id) {
        questiontypeRepository.deleteById(id);
    }

    public void deleteAll() {
        questiontypeRepository.deleteAll();
    }

    public Page<QuestionType> findAll(Pageable pageable){
        return questiontypeRepository.findAll(pageable);
    }
}
