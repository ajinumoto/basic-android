package id.co.nexsoft.nextmile.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.Survey;

//Survey table is the list of question for each costumer
@Repository
@Transactional
public interface SurveyRepository extends JpaRepository<Survey, Long> {

    // Find survey list from costumer id
    @Query(value = "SELECT * FROM survey WHERE costumer_id = ?1", nativeQuery = true)
    List<Survey> findByCostumerId(Long costumerId);

    // Find survey list from date
    @Query(value = "SELECT * FROM survey WHERE date = ?1", nativeQuery = true)
    List<Survey> findByDate(Date date);

    // Find survey list from costumer id and the date (for Mobile)
    @Query(value = "SELECT * FROM survey WHERE date = ?1 AND costumer_id = ?2", nativeQuery = true)
    List<Survey> findByDateAndCostumerId(Date date, Long costumerId);

    // Change the list of question on survey based on date and costumer id
    @Modifying
    @Query(value = "DELETE FROM survey WHERE date = ?1 AND costumer_id = ?2", nativeQuery = true)
    void deleteByDateAndCostumerId(Date date, Long costumerId);

    // Pagination
    Page<Survey> findAll(Pageable pageable);

    List<Survey> findAll(Sort pageable);
}
