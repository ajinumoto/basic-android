package id.co.nexsoft.nextmile.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

import id.co.nexsoft.nextmile.models.QuestionOption;
import id.co.nexsoft.nextmile.services.QuestionOptionService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class OptionController {

    @Autowired
    QuestionOptionService optionService;

    // Getting all answer
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping(path = "/option")
    public ResponseEntity<List<QuestionOption>> getAllOption() {
        return optionService.getAll();
    }

    // Getting answer from id
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping("/option/{id}")
    public ResponseEntity<QuestionOption> getOptionById(@PathVariable("id") long id) {
        return optionService.findById(id);
    }

    // Getting all option based on question id
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping("/option/byquestion")
    public ResponseEntity<List<QuestionOption>> getOptionByQuestionId(
            @RequestParam(name = "questionId", required = true) Long questionId) {
        return optionService.findByQuestionId(questionId);
    }

    // Create an option
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/option")
    public ResponseEntity<QuestionOption> createOption(@RequestBody QuestionOption option) {
        return optionService.save(option);
    }

    // Update an option
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/option/{id}")
    public ResponseEntity<QuestionOption> updateOption(@PathVariable("id") Long id,
            @RequestBody QuestionOption option) {
        return optionService.update(id, option);
    }

    // Remove an option
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/option/{id}")
    public ResponseEntity<HttpStatus> deleteOption(@PathVariable("id") Long id) {
        return optionService.deleteById(id);
    }

    // Option pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/option/page")
    public ResponseEntity<Map<String, Object>> getAllOptionPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return optionService.findAll(name, page, size, sort);
    }
}
