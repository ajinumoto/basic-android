package id.co.nexsoft.nextmile.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.services.RoutePlanService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class RoutePlanController {

    @Autowired
    RoutePlanService routePlanService;

    // Get all route plan
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/routeplan")
    public ResponseEntity<List<RoutePlan>> getAllRoutePlan() {
        return routePlanService.findAll();
    }

    // Get route plan by id
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/routeplan/{id}")
    public ResponseEntity<RoutePlan> getRoutePlanById(@PathVariable("id") long id) {
        return routePlanService.findById(id);
    }

    // Create a route plan
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/routeplan")
    public ResponseEntity<RoutePlan> createRoutePlan(@RequestBody RoutePlan routePlan) {
        return routePlanService.save(routePlan);
    }

    // update a route plan
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/routeplan/{id}")
    public ResponseEntity<RoutePlan> updateRoutePlan(@PathVariable("id") Long id, @RequestBody RoutePlan routePlan) {
        return routePlanService.update(id, routePlan);
    }

    // Get all route plan list based on user id
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/routeplan/user")
    public ResponseEntity<List<RoutePlan>> getRoutePlanByUserId(
            @RequestParam(name = "userId", required = true) Long userId) {
        return routePlanService.findByUserId(userId);
    }

    // Get all route plan list based on date
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/routeplan/date")
    public ResponseEntity<List<RoutePlan>> getRoutePlanByDate(@RequestParam(name = "date", required = true) Date date) {
        return routePlanService.findByDate(date);
    }

    // Get all route plan list based on user id and date
    @PreAuthorize("hasRole('SURVEY') or hasRole('TAGGING')")
    @GetMapping("/routeplan/userdate")
    public ResponseEntity<?> getRoutePlanByUserDate(@RequestParam(name = "userId", required = true) Long userId,
            @RequestParam(name = "date", required = true) Date date) {
        return routePlanService.findByUserIdAndDate(userId, date);
    }

    // Route plan pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/routeplan/page")
    public ResponseEntity<Map<String, Object>> getAllRoutePlanPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return routePlanService.findAll(name, page, size, sort);
    }
}
