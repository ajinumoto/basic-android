package id.co.nexsoft.nextmile.controllers;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nextmile.payload.request.LoginRequest;
import id.co.nexsoft.nextmile.payload.request.SignupRequest;
import id.co.nexsoft.nextmile.payload.response.Data;
import id.co.nexsoft.nextmile.payload.response.MessageResponse;
import id.co.nexsoft.nextmile.services.AuthService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

  @Autowired
  AuthService authService;

  // User Login
  @PostMapping("/signin")
  public ResponseEntity<Data> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    return authService.authenticateUser(loginRequest);
  }

  // Create new user account (Tagging/Survey)
  @PostMapping("/signup")
  public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) throws IOException {
    return authService.registerUser(signUpRequest);
  }
}
