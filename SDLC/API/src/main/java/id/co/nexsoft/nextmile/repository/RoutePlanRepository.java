package id.co.nexsoft.nextmile.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.RoutePlan;

@Repository
public interface RoutePlanRepository extends JpaRepository<RoutePlan, Long> {

    // Find route plan from user id
    @Query(value = "SELECT * FROM route_plan WHERE user_id = ?1", nativeQuery = true)
    List<RoutePlan> findByUserId(Long userId);

    // Find route plan from date
    @Query(value = "SELECT * FROM route_plan WHERE date = ?1", nativeQuery = true)
    List<RoutePlan> findByDate(Date date);

    // Find active route plan from date and user id (for Mobile app)
    @Query(value = "SELECT * FROM route_plan WHERE user_id = ?1 AND date = ?2", nativeQuery = true)
    List<RoutePlan> findByUserIdAndDate(Long userId, Date date);

   //Filtering (completed/not) route plan for the report
    @Query(value = "SELECT * FROM route_plan WHERE completed_date IS NOT NULL", nativeQuery = true)
    Page<RoutePlan> findCompletedDateIsNotNull(Pageable pageable);
    @Query(value = "SELECT * FROM route_plan WHERE completed_date IS NULL", nativeQuery = true)
    Page<RoutePlan> findCompletedDateIsNull(Pageable pageable);

    //Pagination for all data
    Page<RoutePlan> findAll(Pageable pageable);
    List<RoutePlan> findAll(Sort pageable);
}
