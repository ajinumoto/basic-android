package id.co.nexsoft.nextmile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.Tagging;

@Repository
public interface TaggingRepository extends JpaRepository<Tagging, Long> {

    // Get tagging data from route plan id (Mobile)
    @Query(value = "SELECT * FROM tagging WHERE route_plan_id = ?1", nativeQuery = true)
    List<Tagging> findByRoutePlanId(Long routePlanId);

    // Pagination
    Page<Tagging> findAll(Pageable pageable);
    List<Tagging> findAll(Sort pageable);
}
