package id.co.nexsoft.nextmile.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import id.co.nexsoft.nextmile.models.Survey;
import id.co.nexsoft.nextmile.payload.request.SurveyRequest;
import id.co.nexsoft.nextmile.services.SurveyService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class SurveyController {

    @Autowired
    SurveyService surveyService;

    // Create a survey
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/survey")
    public ResponseEntity<Survey> createRoutePlan(@RequestBody Survey survey) {
        return surveyService.save(survey);
    }

    // Get all survey
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/survey")
    public ResponseEntity<List<Survey>> getAllSurvey() {
        return surveyService.getAll();
    }

    // Get a survey by id
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/survey/{id}")
    public ResponseEntity<Survey> getSurveyById(@PathVariable("id") long id) {
        return surveyService.findById(id);
    }

    // Get a survey by costumer id
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/survey/costumer")
    public ResponseEntity<List<Survey>> getSurveyByCostumerId(
            @RequestParam(name = "costumerId", required = true) Long costumerId) {
        return surveyService.findByCostumerId(costumerId);
    }

    // Delete a survey by id
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/survey/{id}")
    public ResponseEntity<HttpStatus> deleteSurvey(@PathVariable("id") Long id) {
        return surveyService.deleteById(id);
    }

    // Update a survey by date and costumer id
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/survey/edit")
    public ResponseEntity<List<Survey>> editByDateAndCostumerId(@RequestBody SurveyRequest surveyEdit) {
        return surveyService.editByDateAndCostumerId(surveyEdit);
    }

    // Get all survey by the date
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping(path = "/survey/date")
    public ResponseEntity<List<Survey>> getSurveyByDate(@RequestParam(name = "date", required = true) Date date) {
        return surveyService.findByDate(date);
    }

    // Get all survey by the date
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/survey/find")
    public ResponseEntity<List<Survey>> getSurveyByDate(@RequestParam(name = "date", required = true) Date date,
            @RequestParam(name = "costumerId", required = true) Long costumerId) {
        return surveyService.findByDateAndCostumerId(date, costumerId);
    }

    // Survey pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/survey/page")
    public ResponseEntity<Map<String, Object>> getAllSurveyPage(
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return surveyService.findAll(name, page, size, sort);

    }
}
