package id.co.nexsoft.nextmile.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "users", uniqueConstraints = {
    @UniqueConstraint(columnNames = "username"),
    @UniqueConstraint(columnNames = "email")
})
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank
  @Size(max = 20)
  @Column(length = 20)
  private String username;

  @NotBlank
  @Email
  @Size(max = 50)
  @Column(length = 50)
  private String email;

  @NotBlank
  @Size(min = 8, max = 120)
  @Column(length = 120)
  @JsonIgnore
  private String password;

  @Pattern(regexp="^[a-zA-Z ]+$")
  @Size(min = 3, max = 50)  
  @Column(length = 50)  
  private String name;

  @Lob
  @Size(max = 1024)
  @Column(length = 1024)
  private String image;

  @Pattern(regexp="^[a-zA-Z ]+$")
  @Size(max = 10)  
  @Column(length = 10)  
  private String status = "not active";

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<Role> roles = new HashSet<>();

  public User() {
  }

  public User(String username, String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

  public User(String username, String email, String password, String name) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.name = name;
  }
}
