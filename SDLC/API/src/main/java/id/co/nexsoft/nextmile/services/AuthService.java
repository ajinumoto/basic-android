package id.co.nexsoft.nextmile.services;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.ERole;
import id.co.nexsoft.nextmile.models.Role;
import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.payload.request.LoginRequest;
import id.co.nexsoft.nextmile.payload.request.SignupRequest;
import id.co.nexsoft.nextmile.payload.response.Data;
import id.co.nexsoft.nextmile.payload.response.MessageResponse;
import id.co.nexsoft.nextmile.repository.RoleRepository;
import id.co.nexsoft.nextmile.repository.UserRepository;
import id.co.nexsoft.nextmile.security.jwt.JwtUtils;
import id.co.nexsoft.nextmile.security.services.UserDetailsImpl;

@Service
public class AuthService {

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @Autowired
  private JavaMailSender mailSender;

  public ResponseEntity<Data> authenticateUser(LoginRequest loginRequest) {

    // Generate token and user details from spring auth
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.toList());

    Data data = new Data();
    data.setUsername(userDetails.getUsername());
    data.setEmail(userDetails.getEmail());
    data.setImage(userDetails.getImage());
    data.setUserId(userDetails.getId());
    data.setStatus(userDetails.getStatus());
    data.setAccessToken(jwt);
    data.setTokenType("Bearer");
    data.setAccessRoles(roles);

    return ResponseEntity.ok(data);
  }

  public ResponseEntity<MessageResponse> registerUser(SignupRequest signUpRequest) throws IOException {

    // Validate username and email
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return new ResponseEntity<>(new MessageResponse("Error: Username is already taken!"), HttpStatus.BAD_REQUEST);
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return new ResponseEntity<>(new MessageResponse("Error: Email is already in use!"), HttpStatus.BAD_REQUEST);
    }

    // Create new user's account
    User user = new User(signUpRequest.getUsername(),
        signUpRequest.getEmail(),
        encoder.encode(signUpRequest.getPassword()));

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    // Check the role
    if (strRoles == null) {
      Role taggingRole = roleRepository.findByName(ERole.ROLE_TAGGING)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(taggingRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "survey":
            Role surveyRole = roleRepository.findByName(ERole.ROLE_SURVEY)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(surveyRole);

            break;
          case "admin":
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          default:
            Role taggingRole = roleRepository.findByName(ERole.ROLE_TAGGING)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(taggingRole);
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);

    // Send email to new user
    new Thread(() -> {
      // do your business here
      SimpleMailMessage message = new SimpleMailMessage();
      message.setFrom("sdlcspring@gmail.com");
      message.setTo(signUpRequest.getEmail());
      message.setSubject("[Nexmile] Registrasi Berhasil!");
      message.setText("Selamat, akun anda dengan username: " + signUpRequest.getUsername()
          + " telah berhasil dibuat! Untuk dapat menggunakan akun silahkan balas email ini dengan melampirkan Identitas diri dan foto profil yang akan Anda gunakan. Terima Kasih");
      mailSender.send(message);
    }).start();

    return new ResponseEntity<>(new MessageResponse("User registered successfully!"), HttpStatus.OK);
  }
}
