package id.co.nexsoft.nextmile.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

import id.co.nexsoft.nextmile.models.User;
import id.co.nexsoft.nextmile.services.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

  @Autowired
  UserService userService;

  // Get all users
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping(path = "/user")
  public ResponseEntity<List<User>> getAllUser() {
    return userService.getAll();
  }

  // Get all sales
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping(path = "/seles")
  public ResponseEntity<List<User>> getAllSales() {
    return userService.findSales();
  }

  // Get a user by id
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/user/{id}")
  public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
    return userService.findById(id);
  }

  // Update user info by id
  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/user/{id}")
  public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
    return userService.updateUser(id, user);
  }

  // Sales pagination from API
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/seles/page")
  public ResponseEntity<Map<String, Object>> getSelesPage(
      @RequestParam(required = false) String filter,
      @RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "3") int size,
      @RequestParam(defaultValue = "id,desc") String[] sort) {
    return userService.findSelesPage(filter, name, page, size, sort);
  }

  // User pagination from API (integer filter)
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/user/page")
  public ResponseEntity<Map<String, Object>> getAllPage(
      @RequestParam(required = false) String filter,
      @RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "3") int size,
      @RequestParam(defaultValue = "id,desc") String[] sort) {
    return userService.findAll(filter, name, page, size, sort);
  }

  // User pagination from API (String filter)
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/user/pages")
  public ResponseEntity<Map<String, Object>> getAllPageFilter(
      @RequestParam(defaultValue = "0", required = false) int filter,
      @RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "3") int size,
      @RequestParam(defaultValue = "id,desc") String[] sort) {
    return userService.findByNameContaining(filter, name, page, size, sort);
  }
}
