package id.co.nexsoft.nextmile.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import id.co.nexsoft.nextmile.payload.response.MessageResponse;
import id.co.nexsoft.nextmile.services.FilesStorageService;

@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/files")
public class FilesController {

  @Autowired
  FilesStorageService storageService;

  // Upload file to API local directory
  @PostMapping("/upload")
  public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file,
      @RequestParam(value = "pathName", required = false) String pathName) {
    return storageService.uploadFile(file, pathName);
  }

  // Get file from API local directory using url
  @GetMapping("/get/{pathname}/{filename:.+}")
  public ResponseEntity<Resource> getFile(@PathVariable String filename, @PathVariable String pathname) {
    return storageService.getFile(filename, pathname);
  }
}
