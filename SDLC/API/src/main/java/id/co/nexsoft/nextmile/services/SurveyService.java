package id.co.nexsoft.nextmile.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.Survey;
import id.co.nexsoft.nextmile.payload.request.SurveyRequest;
import id.co.nexsoft.nextmile.repository.SurveyRepository;

@Service
public class SurveyService {

    @Autowired
    private SurveyRepository surveyRepository;

    public ResponseEntity<Survey> save(Survey survey) {
        try {
            Survey newSurvey = surveyRepository.save(survey);
            return new ResponseEntity<>(newSurvey, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Survey>> getAll() {
        try {
            List<Survey> surveyList = new ArrayList<Survey>();

            surveyRepository.findAll().forEach(surveyList::add);

            return new ResponseEntity<>(surveyList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Survey> findById(Long id) {
        Optional<Survey> survey = surveyRepository.findById(id);

        if (survey.isPresent()) {
            return new ResponseEntity<>(survey.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            surveyRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Survey>> editByDateAndCostumerId(SurveyRequest surveyEdit) {
        try {
            surveyRepository.deleteByDateAndCostumerId(surveyEdit.getDate(), surveyEdit.getCostumerId());

            // Replace survey data
            surveyEdit.getQuestionIdList().forEach(question -> {
                Survey survey = new Survey();
                Costumer costumer = new Costumer();
                Question questionId = new Question();
                costumer.setId(surveyEdit.getCostumerId());

                survey.setDate(surveyEdit.getDate());
                survey.setCostumerId(costumer);

                questionId.setId(question);
                survey.setQuestionId(questionId);
                surveyRepository.save(survey);
            });

            List<Survey> surveyList = new ArrayList<Survey>();
            surveyRepository.findByDateAndCostumerId(surveyEdit.getDate(), surveyEdit.getCostumerId())
                    .forEach(surveyList::add);

            return new ResponseEntity<>(surveyList, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            surveyRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Survey>> findByDateAndCostumerId(Date date, Long costumerId) {
        List<Survey> surveyList = surveyRepository.findByDateAndCostumerId(date, costumerId);
        if (!surveyList.isEmpty()) {
            return new ResponseEntity<>(surveyList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<Survey>> findByCostumerId(Long costumerId) {
        List<Survey> surveyList = surveyRepository.findByCostumerId(costumerId);
        return new ResponseEntity<>(surveyList, HttpStatus.OK);
    }

    public ResponseEntity<List<Survey>> findByDate(Date date) {
        List<Survey> surveyList = surveyRepository.findByDate(date);

        if (!surveyList.isEmpty()) {
            return new ResponseEntity<>(surveyList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findAll(String name, int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Survey> surveyList = new ArrayList<Survey>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<Survey> pageTuts;
            pageTuts = surveyRepository.findAll(pagingSort);

            surveyList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", surveyList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
