package id.co.nexsoft.nextmile.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.services.QuestionService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class QuestionController {

    @Autowired
    QuestionService questionService;

    // Get all question
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping(path = "/question")
    public ResponseEntity<List<Question>> getAllQuestion(@RequestParam(required = false) Question question) {
        return questionService.getAll();
    }

    // Get a question from id
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping("/question/{id}")
    public ResponseEntity<Question> getQuestionById(@PathVariable("id") long id) {
        return questionService.findById(id);
    }

    // crate a question
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @PostMapping(path = "/question")
    public ResponseEntity<Question> createQuestion(@RequestBody Question question) {
        return questionService.save(question);
    }

    // Update a question
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @PutMapping("/question/{id}")
    public ResponseEntity<Question> updateQuestion(@PathVariable("id") Long id, @RequestBody Question question) {
        return questionService.update(id, question);
    }

    // Question pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/question/page")
    public ResponseEntity<Map<String, Object>> getAllQuestionPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return questionService.findAll(name, page, size, sort);
    }
}
