package id.co.nexsoft.nextmile.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import id.co.nexsoft.nextmile.models.Answer;
import id.co.nexsoft.nextmile.services.AnswerService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class AnswerController {

    @Autowired
    AnswerService answerService;

    // Getting all answer
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/answer")
    public ResponseEntity<List<Answer>> getAllAnswer() {
        return answerService.getAll();
    }

    // Getting answer from id
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping("/answer/{id}")
    public ResponseEntity<Answer> getAnswerById(@PathVariable("id") long id) {
        return answerService.findById(id);
    }

    // Create new answer
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @PostMapping(path = "/answer")
    public ResponseEntity<Answer> createAnswer(@RequestBody Answer answer) {
        return answerService.save(answer);
    }

    // Updating answer
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/answer/{id}")
    public ResponseEntity<Answer> updateAnswer(@PathVariable("id") Long id, @RequestBody Answer answer) {
        return answerService.update(id, answer);
    }

    // Getting Answer based on route plan
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @GetMapping("/answer/routeplan")
    public ResponseEntity<List<Answer>> getAnswerByRoutePlan(
            @RequestParam(name = "routePlanId", required = true) Long routePlanId) {
        return answerService.findByRoutePlanId(routePlanId);
    }

    // Save Answer from the list
    @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
    @PostMapping(path = "/answerlist")
    public ResponseEntity<List<Answer>> createAnswerList(@RequestBody List<Answer> answerList) {
        return answerService.saveList(answerList);
    }

    // Answer pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/answer/page")
    public ResponseEntity<Map<String, Object>> getAllAnswerPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return answerService.findAll(name, page, size, sort);
    }
}
