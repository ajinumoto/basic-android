package id.co.nexsoft.nextmile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.QuestionOption;

@Repository
public interface QuestionOptionRepository extends JpaRepository<QuestionOption, Long> {

    // Find all options for the question
    @Query(value = "SELECT * FROM question_option WHERE question_id = ?1", nativeQuery = true)
    List<QuestionOption> findByQuestionId(Long questionId);

    // Pagination
    Page<QuestionOption> findAll(Pageable pageable);
    List<QuestionOption> findAll(Sort pageable);
}
