package id.co.nexsoft.nextmile.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "costumer")
public class Costumer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 255)  
    @Column(length = 255)
    private String address;

    @Pattern(regexp="^[a-zA-Z ]+$")
    @Size(min = 3, max = 50)  
    @Column(length = 50)
    private String name;

    @Lob
    @Size(max = 1024)
    @Column(length = 1024)    
    private String image;
    private Date deleteDate;
}
