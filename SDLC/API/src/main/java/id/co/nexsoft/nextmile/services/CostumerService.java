package id.co.nexsoft.nextmile.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.repository.CostumerRepository;

@Service
public class CostumerService {

    @Autowired
    private CostumerRepository costumerRepository;

    public ResponseEntity<Costumer> update(Long id, Costumer costumer) {
        Optional<Costumer> webData = costumerRepository.findById(id);

        if (webData.isPresent()) {
            return new ResponseEntity<>(costumerRepository.save(costumer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Costumer> save(Costumer costumer) {
        try {
            costumerRepository.save(costumer);
            return new ResponseEntity<>(costumer, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Costumer>> getAll() {
        try {
            List<Costumer> costumerList = new ArrayList<Costumer>();

            costumerRepository.findAll().forEach(costumerList::add);

            return new ResponseEntity<>(costumerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Costumer> findById(Long id) {

        Optional<Costumer> costumer = costumerRepository.findById(id);

        if (costumer.isPresent()) {
            return new ResponseEntity<>(costumer.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            costumerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            costumerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findByFilter(String filter, String name, int page, int size,
            String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Costumer> costumers = new ArrayList<Costumer>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<Costumer> pageTuts;

            // Filtering case
            if (name == null || filter == null) {
                pageTuts = costumerRepository.findAll(pagingSort);
            } else {
                switch (filter) {
                    case "name":
                        pageTuts = costumerRepository.findByNameContaining(name, pagingSort);
                        break;
                    case "address":
                        pageTuts = costumerRepository.findByAddressContaining(name, pagingSort);
                        break;
                    default:
                        pageTuts = costumerRepository.findAll(pagingSort);
                        break;
                }
            }

            costumers = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", costumers);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
