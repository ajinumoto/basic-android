package id.co.nexsoft.nextmile.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import id.co.nexsoft.nextmile.models.Answer;
import id.co.nexsoft.nextmile.models.Question;
import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.repository.AnswerRepository;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;

@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private RoutePlanRepository routePlanRepository;

    public ResponseEntity<Answer> update(Long id, Answer answer) {
        Optional<Answer> webData = answerRepository.findById(id);

        if (webData.isPresent()) {
            return new ResponseEntity<>(answerRepository.save(answer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Answer> save(Answer answer) {
        try {
            RoutePlan routePlan = new RoutePlan();
            java.util.Date date = new java.util.Date();
            Question question = new Question();

            // If answer is received, it will also change the completed date on route plan.
            routePlan.setId(answer.getRoutePlanId());
            question.setId(answer.getQuestionId());
            answer.setRoutePlan(routePlan);
            answer.setQuestion(question);

            routePlan = routePlanRepository.getById(answer.getRoutePlan().getId());
            routePlan.setCompletedDate(new Date(date.getTime()));

            routePlanRepository.save(routePlan);
            answerRepository.save(answer);
            return new ResponseEntity<>(answer, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Answer>> saveList(List<Answer> answerList) {
        try {
            answerList.forEach(this::save);
            return new ResponseEntity<>(answerList, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Answer>> getAll() {
        try {
            List<Answer> answerList = new ArrayList<Answer>();

            answerRepository.findAll().forEach(answerList::add);

            return new ResponseEntity<>(answerList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Answer> findById(Long id) {

        Optional<Answer> answer = answerRepository.findById(id);

        if (answer.isPresent()) {
            return new ResponseEntity<>(answer.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            answerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            answerRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Answer>> findByRoutePlanId(Long routePlanId) {
        List<Answer> answerList = answerRepository.findByRoutePlanId(routePlanId);
        return new ResponseEntity<>(answerList, HttpStatus.OK);
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findAll(String name, int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Answer> answers = new ArrayList<Answer>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<Answer> pageTuts;
            pageTuts = answerRepository.findAll(pagingSort);

            answers = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", answers);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
