package id.co.nexsoft.nextmile.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import id.co.nexsoft.nextmile.models.Tagging;
import id.co.nexsoft.nextmile.services.TaggingService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class TaggingController {

    @Autowired
    TaggingService taggingService;

    // Get all tags
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/tagging")
    public ResponseEntity<List<Tagging>> getAllTagging() {
        return taggingService.getAll();
    }

    // Get a tag from id
    @PreAuthorize("hasRole('ADMIN') or hasRole('TAGGING')")
    @GetMapping("/tagging/{id}")
    public ResponseEntity<Tagging> getTaggingById(@PathVariable("id") long id) {
        return taggingService.findById(id);
    }

    // Save a tag
    @PreAuthorize("hasRole('ADMIN') or hasRole('TAGGING')")
    @PostMapping(path = "/tagging")
    public ResponseEntity<Tagging> createTagging(@RequestBody Tagging tagging) {
        return taggingService.save(tagging);
    }

    // Delete a tag
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/tagging/{id}")
    public ResponseEntity<HttpStatus> deleteTagging(@PathVariable("id") Long id) {
        return taggingService.deleteById(id);
    }

    // Get tag list based on route plan id
    @PreAuthorize("hasRole('ADMIN') or hasRole('TAGGING')")
    @GetMapping("/tagging/routeplan")
    public ResponseEntity<List<Tagging>> getTaggingByRoutePlan(
            @RequestParam(name = "routePlanId", required = true) Long routePlanId) {
        return taggingService.findByRoutePlanId(routePlanId);
    }

    // Save tag list
    @PreAuthorize("hasRole('ADMIN') or hasRole('TAGGING')")
    @PostMapping(path = "/taglist")
    public ResponseEntity<List<Tagging>> createTaggingList(@RequestBody List<Tagging> taggingList) {
        return taggingService.saveList(taggingList);
    }

    // Tag pagination from API
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/tagging/page")
    public ResponseEntity<Map<String, Object>> getAllTaggingPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {
        return taggingService.findAll(name, page, size, sort);
    }

    // Update a tag
    @PreAuthorize("hasRole('TAGGING') or hasRole('ADMIN')")
    @PutMapping("/tagging/{id}")
    public ResponseEntity<Tagging> updateTagging(@PathVariable("id") Long id, @RequestBody Tagging tagging) {
        return taggingService.update(id, tagging);
    }
}
