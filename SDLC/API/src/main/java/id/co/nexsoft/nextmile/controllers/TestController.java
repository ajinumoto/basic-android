package id.co.nexsoft.nextmile.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

  // Access test for public
  @GetMapping("/all")
  public String allAccess() {
    return "Public Content.";
  }

  // Access test for admin role
  @GetMapping("/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public String adminAccess() {
    return "Admin Board.";
  }

  // Access test for survey role
  @GetMapping("/survey")
  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY')")
  public String surveyAccess() {
    return "Survey Board.";
  }

  // Access test for tagging role
  @GetMapping("/tagging")
  @PreAuthorize("hasRole('ADMIN') or hasRole('TAGGING')")
  public String taggingAccess() {
    return "Tagging Board.";
  }
}
