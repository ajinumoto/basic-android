package id.co.nexsoft.nextmile.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;

@Service
public class RoutePlanService {

    @Autowired
    private RoutePlanRepository routePlanRepository;

    public ResponseEntity<RoutePlan> update(Long id, RoutePlan routePlan) {
        Optional<RoutePlan> webData = routePlanRepository.findById(id);

        if (webData.isPresent()) {
            return new ResponseEntity<>(routePlanRepository.save(routePlan), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<RoutePlan> save(RoutePlan routePlan) {
        try {
            RoutePlan newRoutePlan = routePlanRepository.save(routePlan);
            return new ResponseEntity<>(newRoutePlan, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<RoutePlan>> findAll() {
        try {
            List<RoutePlan> routePlanList = new ArrayList<RoutePlan>();

            routePlanRepository.findAll().forEach(routePlanList::add);

            return new ResponseEntity<>(routePlanList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<RoutePlan> findById(Long id) {
        Optional<RoutePlan> newRoutePlan = routePlanRepository.findById(id);

        if (newRoutePlan.isPresent()) {
            return new ResponseEntity<>(newRoutePlan.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            routePlanRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            routePlanRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<RoutePlan>> findByDate(Date date) {
        List<RoutePlan> routePlan = routePlanRepository.findByDate(date);

        if (!routePlan.isEmpty()) {
            return new ResponseEntity<>(routePlan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<RoutePlan>> findByUserId(Long userId) {
        List<RoutePlan> routePlan = routePlanRepository.findByUserId(userId);

        if (!routePlan.isEmpty()) {
            return new ResponseEntity<>(routePlan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<RoutePlan>> findByUserIdAndDate(Long userId, Date date) {

        List<RoutePlan> routePlan = routePlanRepository.findByUserIdAndDate(userId, date);

        if (!routePlan.isEmpty()) {
            return new ResponseEntity<>(routePlan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findAll(String name, int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<RoutePlan> routePlanList = new ArrayList<RoutePlan>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<RoutePlan> pageTuts;
            if (name != null) {
                switch (name) {
                    case "0":
                        pageTuts = routePlanRepository.findAll(pagingSort);
                        break;
                    case "1":
                        pageTuts = routePlanRepository.findCompletedDateIsNotNull(pagingSort);
                        break;
                    case "2":
                        pageTuts = routePlanRepository.findCompletedDateIsNull(pagingSort);
                        break;
                    default:
                        pageTuts = routePlanRepository.findAll(pagingSort);
                        break;
                }
            } else {
                pageTuts = routePlanRepository.findAll(pagingSort);

            }

            routePlanList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", routePlanList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
