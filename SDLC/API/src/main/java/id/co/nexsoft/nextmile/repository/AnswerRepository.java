package id.co.nexsoft.nextmile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import id.co.nexsoft.nextmile.models.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    //Find All Answer from route plan id
    @Query(value = "SELECT * FROM answer WHERE route_plan_id = ?1", nativeQuery = true)
	List<Answer> findByRoutePlanId(Long routePlanId);  

    //Pagination
    Page<Answer> findAll(Pageable pageable);
    List<Answer> findAll(Sort pageable);
}