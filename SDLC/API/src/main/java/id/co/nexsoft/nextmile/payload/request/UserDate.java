package id.co.nexsoft.nextmile.payload.request;

import java.sql.Date;

import id.co.nexsoft.nextmile.models.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDate {
    private User user;
    private Date date;
}
