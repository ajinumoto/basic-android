package id.co.nexsoft.nextmile.models;

public enum ERole {
  ROLE_SURVEY,
  ROLE_TAGGING,
  ROLE_ADMIN
}
