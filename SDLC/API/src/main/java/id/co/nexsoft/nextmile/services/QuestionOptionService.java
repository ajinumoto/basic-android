package id.co.nexsoft.nextmile.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.QuestionOption;
import id.co.nexsoft.nextmile.repository.QuestionOptionRepository;

@Service
public class QuestionOptionService {
    
    @Autowired
    private QuestionOptionRepository questionOptionRepository;

    public ResponseEntity<QuestionOption> update(Long id, QuestionOption questionOption) {
        Optional<QuestionOption> webData = questionOptionRepository.findById(id);

        if (webData.isPresent()) {
            return new ResponseEntity<>(questionOptionRepository.save(questionOption), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<QuestionOption> save(QuestionOption option) {
        try {
            QuestionOption newOption = questionOptionRepository.save(option);
            return new ResponseEntity<>(newOption, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<QuestionOption>> getAll() {
        try {
            List<QuestionOption> optionList = new ArrayList<QuestionOption>();
            questionOptionRepository.findAll().forEach(optionList::add);
            return new ResponseEntity<>(optionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<QuestionOption> findById(Long id) {
        Optional<QuestionOption> option = questionOptionRepository.findById(id);

        if (option.isPresent()) {
            return new ResponseEntity<>(option.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            questionOptionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            questionOptionRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<QuestionOption>> findByQuestionId(Long questionId){
        List<QuestionOption> option = questionOptionRepository.findByQuestionId(questionId);
        if (!option.isEmpty()) {
            return new ResponseEntity<>(option, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(option, HttpStatus.OK);
        }
    } 
    
    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findAll(String name, int page, int size, String[] sort){
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<QuestionOption> options = new ArrayList<QuestionOption>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<QuestionOption> pageTuts;
            pageTuts = questionOptionRepository.findAll(pagingSort);

            options = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", options);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
