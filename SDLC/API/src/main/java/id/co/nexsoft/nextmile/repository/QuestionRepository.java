package id.co.nexsoft.nextmile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    //Pagination
    Page<Question> findAll(Pageable pageable);
    List<Question> findAll(Sort pageable);
}
