package id.co.nexsoft.nextmile.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tagging")
public class Tagging {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "route_plan_id", referencedColumnName = "id", nullable = false)
    private RoutePlan routePlan;

    @Lob
    @NotBlank
    @Size(max = 1024)
    @Column(length = 1024)
    private String imageUrl;

    @NotBlank
    @Size(min = 3, max = 20)  
    @Column(length = 20)
    private String serialNumber;

    @NotBlank
    @Size(min = 3, max = 20)  
    @Column(length = 20)
    private String contractNumber;

    private Date assetDate;
    private Date deleteDate;

    // Data Json
    @Transient
    private Long routePlanId;

}
