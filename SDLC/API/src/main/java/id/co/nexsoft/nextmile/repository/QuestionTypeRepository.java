package id.co.nexsoft.nextmile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import id.co.nexsoft.nextmile.models.QuestionType;

@Repository
public interface QuestionTypeRepository extends JpaRepository<QuestionType, Long> {
    //Pagination
    Page<QuestionType> findAll(Pageable pageable);
    List<QuestionType> findAll(Sort pageable);
}
