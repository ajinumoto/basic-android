package id.co.nexsoft.nextmile.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.RoutePlan;
import id.co.nexsoft.nextmile.models.Tagging;
import id.co.nexsoft.nextmile.repository.RoutePlanRepository;
import id.co.nexsoft.nextmile.repository.TaggingRepository;

@Service
public class TaggingService {

    @Autowired
    private TaggingRepository taggingRepository;
    @Autowired
    private RoutePlanRepository routePlanRepository;

    public ResponseEntity<Tagging> update(Long id, Tagging tagging) {
        Optional<Tagging> webData = taggingRepository.findById(id);

        if (webData.isPresent()) {
            return new ResponseEntity<>(taggingRepository.save(tagging), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Tagging> save(Tagging tagging) {
        try {
            RoutePlan routePlan = new RoutePlan();
            java.util.Date date = new java.util.Date();
            // If tagging is received, it will also change the completed date on route plan.
            routePlan.setId(tagging.getRoutePlanId());
            tagging.setRoutePlan(routePlan);
            routePlan = routePlanRepository.getById(tagging.getRoutePlan().getId());

            if (routePlan!=null) {
                routePlan.setCompletedDate(new Date(date.getTime()));
                routePlanRepository.save(routePlan);
                taggingRepository.save(tagging);
                return new ResponseEntity<>(tagging, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.err.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Tagging>> saveList(List<Tagging> taggingList) {
        try {
            taggingList.forEach(tag -> {
                save(tag);
            });
            return new ResponseEntity<>(taggingList, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Tagging>> getAll() {
        try {
            List<Tagging> taggingList = new ArrayList<Tagging>();

            taggingRepository.findAll().forEach(taggingList::add);

            return new ResponseEntity<>(taggingList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Tagging> findById(Long id) {
        Optional<Tagging> tagging = taggingRepository.findById(id);

        if (tagging.isPresent()) {
            return new ResponseEntity<>(tagging.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteById(Long id) {
        try {
            taggingRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<HttpStatus> deleteAll() {
        try {
            taggingRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<Tagging>> findByRoutePlanId(Long routePlanId) {
        List<Tagging> taggingList = taggingRepository.findByRoutePlanId(routePlanId);

        if (!taggingList.isEmpty()) {
            return new ResponseEntity<>(taggingList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

    public ResponseEntity<Map<String, Object>> findAll(String name, int page, int size, String[] sort) {
        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Tagging> taggingList = new ArrayList<Tagging>();
            Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

            Page<Tagging> pageTuts;
            pageTuts = taggingRepository.findAll(pagingSort);

            taggingList = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("items", taggingList);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
