package id.co.nexsoft.nextmile;

import javax.annotation.Resource;

// import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import id.co.nexsoft.nextmile.services.FilesStorageService;

@SpringBootApplication
// public class NexmileApplication implements CommandLineRunner {
public class NexmileApplication {
	
	//For saving to local disk
	@Resource
	FilesStorageService storageService;

	public static void main(String[] args) {
		SpringApplication.run(NexmileApplication.class, args);
	}

}
