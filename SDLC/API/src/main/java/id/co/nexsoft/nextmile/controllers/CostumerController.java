package id.co.nexsoft.nextmile.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;
import java.util.List;

import id.co.nexsoft.nextmile.models.Costumer;
import id.co.nexsoft.nextmile.services.CostumerService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class CostumerController {

  @Autowired
  CostumerService costumerService;

  // Getting all costumer
  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @GetMapping(path = "/costumer")
  public ResponseEntity<List<Costumer>> getAllCostumer() {
    return costumerService.getAll();
  }

  // Getting costumer from id
  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @GetMapping("/costumer/{id}")
  public ResponseEntity<Costumer> getCostumerById(@PathVariable("id") long id) {
    return costumerService.findById(id);
  }

  // Create new costumer
  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping(path = "/costumer")
  public ResponseEntity<Costumer> createCostumer(@RequestBody Costumer costumer) {
    return costumerService.save(costumer);
  }

  // Updating costumer
  @PreAuthorize("hasRole('ADMIN')")
  @PutMapping("/costumer/{id}")
  public ResponseEntity<Costumer> updateCostumer(@PathVariable("id") Long id, @RequestBody Costumer costumer) {
    return costumerService.update(id, costumer);
  }

  // Costumer pagination from API
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/costumer/page")
  public ResponseEntity<Map<String, Object>> getAllPage(
      @RequestParam(required = false) String filter,
      @RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "3") int size,
      @RequestParam(defaultValue = "id,desc") String[] sort) {

    return costumerService.findByFilter(filter, name, page, size, sort);
  }
}
