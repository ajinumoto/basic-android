package id.co.nexsoft.nextmile.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import id.co.nexsoft.nextmile.controllers.FilesController;
import id.co.nexsoft.nextmile.models.FileInfo;
import id.co.nexsoft.nextmile.payload.response.MessageResponse;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

  private final Path root = Paths.get("uploads");

  // Initiated master directory
  @Override
  public void init() {
    try {
      Files.createDirectory(root);
    } catch (IOException e) {
      throw new RuntimeException("Could not initialize folder for upload!");
    }
  }

  // Save file to local directory
  @Override
  public void save(MultipartFile file, String pathName) {

    if (Files.notExists(root.resolve(pathName))) {
      try {
        Files.createDirectory(root.resolve(pathName));
      } catch (Exception e) {
        throw new RuntimeException("Could not create the directory.");
      }
    }

    try {
      Files.copy(file.getInputStream(),
          this.root.resolve(pathName).resolve(Objects.requireNonNull(file.getOriginalFilename())));
    } catch (Exception e) {
      replace(file, pathName);
      throw new RuntimeException("Could not store the file.");
    }
  }

  // Load file from local directory (file format)
  @Override
  public Resource load(String filename, String pathName) {
    try {
      Path file = root.resolve(pathName).resolve(filename);
      Resource resource = new UrlResource(file.toUri());

      if (resource.exists() || resource.isReadable()) {
        return resource;
      } else {
        throw new RuntimeException("Could not read the file!");
      }
    } catch (MalformedURLException e) {
      throw new RuntimeException("Error: " + e.getMessage());
    }
  }

  // Replace file with the new one
  @Override
  public void replace(MultipartFile file, String pathName) {
    try {
      delete(file.getOriginalFilename(), pathName);
      Files.copy(file.getInputStream(),
          this.root.resolve(pathName).resolve(Objects.requireNonNull(file.getOriginalFilename())));
    } catch (Exception e) {
      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
    }
  }

  // Delete file in local disk
  @Override
  public void delete(String filename, String pathName) {
    try {
      FileSystemUtils.deleteRecursively(root.resolve(pathName).resolve(filename));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // remove all file on master directory
  @Override
  public void deleteAll() {
    FileSystemUtils.deleteRecursively(root.toFile());
  }

  @Override
  public Stream<Path> loadAll() {
    try {
      return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
    } catch (IOException e) {
      throw new RuntimeException("Could not load the files!");
    }
  }

  // upload file from http to local disk
  @Override
  public ResponseEntity<MessageResponse> uploadFile(MultipartFile file, String pathName) {
    String message = "";
    try {
      save(file, pathName);
      message = "Uploaded the file successfully: " + file.getOriginalFilename();
      return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message));
    } catch (Exception e) {
      message = "Could not upload the file: " + file.getOriginalFilename() + "! " + e.toString();
      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(message));
    }
  }

  // Show list of all file
  public ResponseEntity<List<FileInfo>> getListFiles() {
    List<FileInfo> fileInfos = loadAll().map(path -> {
      String filename = path.getFileName().toString();
      String url = MvcUriComponentsBuilder
          .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();

      return new FileInfo(filename, url);
    }).collect(Collectors.toList());

    return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
  }

  // Load file from local directory (http format)
  @Override
  public ResponseEntity<Resource> getFile(String filename, String pathname) {
    Resource file = load(filename, pathname);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
  }

}
