package id.co.nexsoft.nextmile.services;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import id.co.nexsoft.nextmile.payload.response.MessageResponse;

public interface FilesStorageService {
  public void init();

  public void save(MultipartFile file, String pathName);

  public ResponseEntity<MessageResponse> uploadFile(MultipartFile file, String pathName);

  public Resource load(String filename, String pathName);

  public ResponseEntity<Resource> getFile(String filename, String pathName);

  public void replace(MultipartFile file, String pathName);

  public void delete(String filename, String pathName);

  public void deleteAll();

  public Stream<Path> loadAll();
}
