package id.co.nexsoft.nextmile.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "question_option")
public class QuestionOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private Long id;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, 
    property="id")
    @JsonIdentityReference(alwaysAsId=true) 
    @ManyToOne(optional = false)
    @JoinColumn(name = "question_id", referencedColumnName = "id", nullable = false)
    private Question questionId;

    @NotBlank
    @Size(min = 2, max = 20)
    @Column(length = 20)
    private String questionOption;
    private Date deleteDate;
}
