package id.co.nexsoft.nextmile.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Sort;

import id.co.nexsoft.nextmile.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  // Find by username
  Optional<User> findByUsername(String username);

  // Check the username
  Boolean existsByUsername(String username);

  // Check the email
  Boolean existsByEmail(String email);

  // Find only TAGGING and SURVEY user (avoid admin data)
  @Query(value = "SELECT u.* FROM users as u left join user_roles as ur on u.id=ur.user_id where ur.role_id!=1", nativeQuery = true)
  List<User> findSeles();

  // Pagination for sales
  @Query(value = "SELECT * FROM users as u left join user_roles as ur on u.id=ur.user_id where ur.role_id!=1", nativeQuery = true)
  Page<User> findSelesPage(Pageable pageable);

  // Pagination all data
  Page<User> findAll(Pageable pageable);

  List<User> findAll(Sort pageable);

  // Filtering seles by name
  Page<User> findByNameContaining(String name, Pageable pageable);

}
