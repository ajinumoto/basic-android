package id.co.nexsoft.nextmile.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nextmile.models.ERole;
import id.co.nexsoft.nextmile.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  //Find role from name
  Optional<Role> findByName(ERole name);
}
