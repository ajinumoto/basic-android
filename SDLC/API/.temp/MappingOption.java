package id.co.nexsoft.nextmile.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
// @Entity
// @Table(name = "mapping_option")
public class MappingOption {
    // @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private Long id;

    // @ManyToOne(optional = false)
    // @JoinColumn(name = "question_id", referencedColumnName = "id", nullable = false)
    private Question questionId;

    // @ManyToOne(optional = false)
    // @JoinColumn(name = "question_option_id", referencedColumnName = "id", nullable = false)
    private QuestionOption questionOptionId;

    private Date deleteDate;
}
