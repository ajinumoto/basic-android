package id.co.nexsoft.nextmile.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.QuestionOption;
import id.co.nexsoft.nextmile.repository.QuestionOptionRepository;

@Service
public class QuestionOptionService {
    
    @Autowired
    private QuestionOptionRepository questionOptionRepository;

    public QuestionOption save(QuestionOption option) {
        return questionOptionRepository.save(option);
    }

    public Iterable<QuestionOption> findAll() {
        return questionOptionRepository.findAll();
    }

    public Optional<QuestionOption> findById(Long id) {
        return questionOptionRepository.findById(id);
    }

    public void deleteById(Long id) {
        questionOptionRepository.deleteById(id);
    }

    public void deleteAll() {
        questionOptionRepository.deleteAll();
    }

    
}
