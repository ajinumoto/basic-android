package id.co.nexsoft.nextmile.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.nextmile.models.MappingOption;
import id.co.nexsoft.nextmile.repository.MappingOptionRepository;

@Service
public class MappingOptionService {
    
    @Autowired
    private MappingOptionRepository mappingoptionRepository;

    public MappingOption save(MappingOption mappingoption) {
        return mappingoptionRepository.save(mappingoption);
    }

    public Iterable<MappingOption> findAll() {
        return mappingoptionRepository.findAll();
    }

    public Optional<MappingOption> findById(Long id) {
        return mappingoptionRepository.findById(id);
    }

    public void deleteById(Long id) {
        mappingoptionRepository.deleteById(id);
    }

    public void deleteAll() {
        mappingoptionRepository.deleteAll();
    }

    
}
