package id.co.nexsoft.nextmile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nextmile.models.MappingOption;

@Repository
public interface MappingOptionRepository extends JpaRepository<MappingOption, Long> {
    
}