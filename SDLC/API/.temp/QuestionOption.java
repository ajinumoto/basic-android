package id.co.nexsoft.nextmile.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
// @Entity
// @Table(name = "question_option")
public class QuestionOption {
    // @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private Long id;

    private String name;

    private Date deleteDate;
}
