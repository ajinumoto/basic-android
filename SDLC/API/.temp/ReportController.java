package id.co.nexsoft.nextmile.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import java.util.Map;
import java.util.HashMap;

import id.co.nexsoft.nextmile.models.Report;
import id.co.nexsoft.nextmile.services.ReportService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ReportController {

  // Report
  @Autowired
  private ReportService reportService;

  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @GetMapping(path = "/report")
  public ResponseEntity<List<Report>> getAllReport(@RequestParam(required = false) Report report) {
    try {
      List<Report> reportes = new ArrayList<Report>();

      reportService.findAll().forEach(reportes::add);

      return new ResponseEntity<>(reportes, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @GetMapping("/report/{id}")
  public ResponseEntity<Report> getReportById(@PathVariable("id") long id) {
    Optional<Report> report = reportService.findById(id);

    if (report.isPresent()) {
      return new ResponseEntity<>(report.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @PostMapping(path = "/report")
  public ResponseEntity<Report> createReport(@RequestBody Report report) {
    try {
      Report newReport = reportService.save(report);
      return new ResponseEntity<>(newReport, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PreAuthorize("hasRole('ADMIN') or hasRole('SURVEY') or hasRole('TAGGING')")
  @PutMapping("/report/{id}")
  public ResponseEntity<Report> updateReport(@PathVariable("id") Long id, @RequestBody Report report) {
    Optional<Report> webData = reportService.findById(id);

    if (webData.isPresent()) {
      return new ResponseEntity<>(reportService.save(report), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping("/report/{id}")
  public ResponseEntity<HttpStatus> deleteReport(@PathVariable("id") Long id) {
    try {
      reportService.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping("/report")
  public ResponseEntity<HttpStatus> deleteAllReport() {
    try {
      reportService.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  private Sort.Direction getSortDirection(String direction) {
    if (direction.equals("asc")) {
      return Sort.Direction.ASC;
    } else if (direction.equals("desc")) {
      return Sort.Direction.DESC;
    }

    return Sort.Direction.ASC;
  }

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/report/page")
  public ResponseEntity<Map<String, Object>> getAllReportPage(
      @RequestParam(required = false) String name,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "3") int size,
      @RequestParam(defaultValue = "id,desc") String[] sort) {

    try {
      List<Order> orders = new ArrayList<Order>();

      if (sort[0].contains(",")) {
        // will sort more than 2 fields
        // sortOrder="field, direction"
        for (String sortOrder : sort) {
          String[] _sort = sortOrder.split(",");
          orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
        }
      } else {
        // sort=[field, direction]
        orders.add(new Order(getSortDirection(sort[1]), sort[0]));
      }

      List<Report> reports = new ArrayList<Report>();
      Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));

      Page<Report> pageTuts;
      if (name == null)
        pageTuts = reportService.findAll(pagingSort);
      else
        pageTuts = reportService.findByNameContaining(name, pagingSort);

      reports = pageTuts.getContent();

      Map<String, Object> response = new HashMap<>();
      response.put("items", reports);
      response.put("currentPage", pageTuts.getNumber());
      response.put("totalItems", pageTuts.getTotalElements());
      response.put("totalPages", pageTuts.getTotalPages());

      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
